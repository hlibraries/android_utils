package ru.hnau.androidutilsapp

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.utils.ThemeManager


data class AppTheme(
        val background: ColorGetter,
        val foreground: ColorGetter
) {

    companion object {

        val DEFAULT = AppTheme(
                background = ColorGetter.WHITE,
                foreground = ColorGetter.DKGRAY
        )

        val BLUE = AppTheme(
                background = ColorManager.BLUE,
                foreground = ColorGetter.LTGRAY
        )

        val RED = AppTheme(
                background = ColorManager.RED,
                foreground = ColorGetter.LTGRAY
        )

        var current = DEFAULT
            set(value) {
                if (field != value) {
                    field = value
                    ThemeManager.onThemeChanged()
                }
            }

    }

}