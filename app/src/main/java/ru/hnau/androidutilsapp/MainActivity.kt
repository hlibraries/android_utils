package ru.hnau.androidutilsapp

import android.os.Bundle
import ru.hnau.androidutils.ui.TransparentStatusBarActivity
import ru.hnau.androidutils.ui.utils.permissions.PermissionsManager
import ru.hnau.androidutils.ui.view.layer.manager.LayerManager
import ru.hnau.androidutilsapp.layer.InitialLayer

class MainActivity : TransparentStatusBarActivity() {

    private val layerManager: LayerManager by lazy { LayerManager(this@MainActivity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(layerManager)
        layerManager.showLayer(InitialLayer(this))
    }

    override fun getIsStatusBarIconsLight() =
            AppTheme.current == AppTheme.DEFAULT

    override fun onBackPressed() {
        if (layerManager.handleGoBack()) {
            return
        }
        super.onBackPressed()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionsManager.onRequestResult(requestCode, permissions, grantResults)
    }


}
