package ru.hnau.androidutilsapp.layer

import android.content.Context
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.BottomSheetView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogView
import ru.hnau.androidutils.ui.view.utils.postDelayed
import ru.hnau.androidutils.utils.shortToast
import ru.hnau.androidutilsapp.DialogManager
import ru.hnau.androidutilsapp.layer.animations.AnimationsLayer
import ru.hnau.androidutilsapp.layer.base.ListLayer
import ru.hnau.jutils.TimeValue


class InitialLayer(context: Context) : ListLayer(
        context = context,
        title = "AndroidUtils".toGetter(),
        items = listOf<Pair<String, (ListLayer) -> Unit>>(
                "Theme" to { listLayer -> listLayer.layerManagerConnector.showLayer(ThemeLayer(context)) },
                "Scale" to { listLayer -> listLayer.layerManagerConnector.showLayer(ScaleLayer(context)) },
                "Dialog" to { listLayer -> DialogManager.showDialog(listLayer.layerManagerConnector, dialogViewConfigurator = dialogViewConfigurator) },
                "Bottom Sheet" to { listLayer -> DialogManager.showBottomSheet(listLayer.layerManagerConnector, bottomSheetViewConfigurator = bottomSheetViewConfigurator) },
                "Suspend getter" to { listLayer -> listLayer.layerManagerConnector.showLayer(SuspendGetterLayer(context)) },
                "Animations" to { listLayer -> listLayer.layerManagerConnector.showLayer(AnimationsLayer(context)) },
                "Presenter" to { listLayer -> listLayer.layerManagerConnector.showLayer(PresenterLayer(context)) },
                "Scroll" to { listLayer -> listLayer.layerManagerConnector.showLayer(ScrollLayer(context)) }
        ).map { (title, layerCreator) ->
            ListLayer.Item(
                    title = title.toGetter(),
                    onClick = layerCreator
            )
        },
        addGoBackButton = false
) {

    companion object {

        private val dialogViewConfigurator: MaterialDialogView.() -> Unit = {
            title("Заголовок".toGetter())
            text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua".toGetter())
            button("Pause".toGetter()) {
                shortToast("Pause before close".toGetter())
                postDelayed(TimeValue.SECOND) {
                    close()
                }
            }
            closeButton("Закрыть".toGetter())
            closeButton("Темы".toGetter()) {
                layerManagerConnector.showLayer(ThemeLayer(context))
            }
            goBackHandler = {
                shortToast("Закрытие диалога заблокировано".toGetter())
                true
            }
        }

        private val bottomSheetViewConfigurator: BottomSheetView.() -> Unit = {
            title("Заголовок".toGetter())
            text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua".toGetter())
            item("Pause".toGetter()) {
                shortToast("Pause before close".toGetter())
                postDelayed(TimeValue.SECOND) {
                    close()
                }
            }
            closeItem("Закрыть".toGetter())
            closeItem("Темы".toGetter()) {
                layerManagerConnector.showLayer(ThemeLayer(context))
            }
        }

    }

}