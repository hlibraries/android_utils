package ru.hnau.androidutilsapp.layer.animations

import android.content.Context
import ru.hnau.androidutils.animations.*
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.context_getters.dp_px.dp32
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.view.addLinearSeparator
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.androidutilsapp.view.addAppButton
import ru.hnau.androidutilsapp.layer.base.BaseLayer
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.StateProducerSimple
import ru.hnau.jutils.producer.extensions.toFloat
import ru.hnau.jutils.producer.invert


class AnimationsLayer(
        context: Context
) : BaseLayer(
        context = context,
        title = "Animations".toGetter(),
        addGoBackButton = true
) {

    private val stateProducer =
            StateProducerSimple<Boolean>(false)

    private val animationPositionProducer =
            stateProducer
                    .toFloat()
                    .smooth(TimeValue.SECOND)
                    .interpolateAccelerateDecelerate()

    override fun afterCreate() {
        super.afterCreate()

        addLinearSeparator()
        addView(AnimationPositionView(context, animationPositionProducer).apply {
            setPadding(dp16, dp32)
        })
        addLinearSeparator()
        addAppButton("Change".toGetter(), stateProducer::invert)
    }

}