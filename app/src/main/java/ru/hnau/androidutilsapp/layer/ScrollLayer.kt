package ru.hnau.androidutilsapp.layer

import android.content.Context
import android.graphics.Color
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.dp_px.*
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.buttons.addMainActionButton
import ru.hnau.androidutils.ui.view.buttons.main_action.MainActionButtonInfo
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.list.base.*
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.ui.view.utils.apply.addFrameLayout
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyFrameParams
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.androidutils.ui.view.utils.scroll.recycle_view.createOnRecyclerViewScrolledProducer
import ru.hnau.androidutils.ui.view.utils.scroll.recycle_view.createRecycleViewIsScrolledToTopProducer
import ru.hnau.androidutils.ui.view.utils.scroll.view.createOnScrolledProducer
import ru.hnau.androidutils.utils.longToast
import ru.hnau.androidutils.utils.shortToast
import ru.hnau.androidutilsapp.ColorManager
import ru.hnau.androidutilsapp.R
import ru.hnau.androidutilsapp.layer.base.BaseLayer
import ru.hnau.jutils.producer.extensions.not
import ru.hnau.jutils.producer.extensions.toProducer


class ScrollLayer(
        context: Context
) : BaseLayer(
        context = context,
        title = "Is scrolled".toGetter(),
        addGoBackButton = true
) {

    override fun afterCreate() {
        super.afterCreate()

        addFrameLayout {

            applyLinearParams {
                setStretchedHeight()
                setMatchParentWidth()
            }

            val listView = BaseList<Int>(
                    context = context,
                    itemsProducer = (0..100).toList().toProducer(),
                    viewWrappersCreator = {
                        object : Label(
                                context = context,
                                gravity = HGravity.START_CENTER_VERTICAL,
                                textColor = ColorGetter.DKGRAY,
                                textSize = dp16,
                                minLines = 1,
                                maxLines = 1
                        ), BaseListViewWrapper<Int> {

                            override val view = this

                            init {
                                setPadding(dp16, dp16)
                            }

                            override fun setContent(content: Int, position: Int) {
                                text = content.toString().toGetter()
                            }

                        }
                    }
            )

            addView(listView)

            val listOnScrolled =
                    listView.createOnRecyclerViewScrolledProducer()

            val listIsScrolledToTop =
                    listView.createRecycleViewIsScrolledToTopProducer(listOnScrolled)

            addMainActionButton(
                    icon = DrawableGetter(R.drawable.ic_cirlce_icon_button_icon),
                    onClick = { shortToast("Click".toGetter()) },
                    title = "Button title".toGetter(),
                    needShowTitle = listIsScrolledToTop.not(),
                    info = MainActionButtonInfo(
                            rippleDrawInfo = ColorManager.FG_RIPPLE_DRAW_INFO,
                            titleLabelInfo = LabelInfo(textColor = ColorManager.BACKGROUND)
                    )
            ) {
                applyFrameParams {
                    setEndBottomGravity()
                    setMargins(dp8)
                }
            }

        }

    }

}