package ru.hnau.androidutilsapp.layer.animations

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen


class AnimationPositionView(
        context: Context,
        animationPosition: Producer<Float>
) : View(
        context
) {

    companion object {

        private val CIRCLE_RADIUS = dp16
        private val CIRCLE_COLOR = ColorGetter.MAGENTA

    }

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private var position = 0f
        set(value) {
            field = value
            invalidate()
        }

    private val circlePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = CIRCLE_COLOR.get(context)
    }

    init {
        animationPosition.observeWhen(isVisibleToUserProducer) { position = it }
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        val circleRadius = CIRCLE_RADIUS.getPx(context)
        val x = paddingLeft + circleRadius + (width - circleRadius * 2 - horizontalPaddingSum) * position
        val y = paddingTop + (height - verticalPaddingSum) / 2f
        canvas.drawCircle(x, y, circleRadius, circlePaint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
                getMaxMeasurement(widthMeasureSpec, 0),
                getDefaultMeasurement(heightMeasureSpec, CIRCLE_RADIUS.getPxInt(context) * 2 + verticalPaddingSum)
        )
    }

}