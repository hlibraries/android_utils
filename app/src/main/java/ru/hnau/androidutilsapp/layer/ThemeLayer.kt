package ru.hnau.androidutilsapp.layer

import android.content.Context
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutilsapp.layer.base.ListLayer
import ru.hnau.androidutilsapp.AppTheme


class ThemeLayer(context: Context) : ListLayer(
        context = context,
        title = "Theme".toGetter(),
        items = listOf(
                "стандартную" to AppTheme.DEFAULT,
                "синюю" to AppTheme.BLUE,
                "красную" to AppTheme.RED
        ).map { (title, theme) ->
            ListLayer.Item(
                    title = "Включить $title".toGetter(),
                    onClick = { AppTheme.current = theme }
            )
        }
)