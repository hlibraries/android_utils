package ru.hnau.androidutilsapp.layer

import android.content.Context
import android.graphics.Color
import android.view.View
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.context_getters.dp_px.dp32
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.view.utils.apply.addFrameLayout
import ru.hnau.androidutils.ui.view.utils.apply.addHorizontalLayout
import ru.hnau.androidutils.ui.view.view_presenter.*
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.utils.RANDOM
import ru.hnau.androidutilsapp.view.addAppButton
import ru.hnau.androidutilsapp.layer.base.BaseLayer
import ru.hnau.jutils.producer.StateProducerSimple


class PresenterLayer(
        context: Context
) : BaseLayer(
        context = context,
        title = "Presenter".toGetter(),
        addGoBackButton = true
) {

    private val viewsProducer = StateProducerSimple<PresentingViewInfo>()

    private var magenta = true

    private fun getNextView() = object : View(context) {

        private val size = RANDOM.nextInt(dpToPxInt(128)) + dpToPxInt(128)

        init {
            setBackgroundColor(if (magenta) Color.MAGENTA else Color.YELLOW)
            magenta = !magenta
        }

        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            setMeasuredDimension(size, size)
        }

    }

    private fun updateView(view: View?) {
        viewsProducer.updateState(
                PresentingViewInfo(
                        view = view
                )
        )
    }


    override fun afterCreate() {
        super.afterCreate()

        addFrameLayout {

            setLinearParams(MATCH_PARENT, 0, 1f)

            addPresenter(
                    presentingViewProducer = viewsProducer
            ) {
                setBackgroundColor(Color.CYAN)
                setPadding(dp32, dp16)
                setFrameParams(WRAP_CONTENT, WRAP_CONTENT) {
                    setFrameParamsCenterGravity()
                }
            }
        }

        addHorizontalLayout {

            addAppButton("Null".toGetter(), { updateView(null) }) {
                setLinearParams(0, WRAP_CONTENT, 1f)
            }

            addAppButton("View".toGetter(), { updateView(getNextView()) }) {
                setLinearParams(0, WRAP_CONTENT, 1f)
            }

        }

    }

}