package ru.hnau.androidutilsapp.layer

import android.app.Activity
import android.content.Context
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.context_getters.dp_px.dp24
import ru.hnau.androidutils.context_getters.dp_px.dp64
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.permissions.PermissionsManager
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.utils.permissions.OnPermissionDeniedException
import ru.hnau.androidutils.ui.utils.permissions.OnPermissionDeniedForeverException
import ru.hnau.androidutils.ui.view.buttons.circle.icon.addCircleIconButton
import ru.hnau.androidutils.ui.view.utils.apply.addFrameLayout
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.addLabel
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyFrameParams
import ru.hnau.androidutils.ui.view.waiter.loader.SuspendLoader
import ru.hnau.androidutils.ui.view.waiter.material.MaterialWaiterView
import ru.hnau.androidutilsapp.ColorManager
import ru.hnau.androidutilsapp.DialogManager
import ru.hnau.androidutilsapp.R
import ru.hnau.androidutilsapp.view.addAppButton
import ru.hnau.androidutilsapp.layer.base.BaseLayer
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.coroutines.deferred.complete
import ru.hnau.jutils.coroutines.deferred.deferred
import ru.hnau.jutils.coroutines.delay
import ru.hnau.jutils.getter.Getter
import ru.hnau.jutils.getter.SuspendGetter
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.producer.CachingProducer


class SuspendGetterLayer(
        context: Context
) : BaseLayer(
        context = context,
        title = "Suspend getter".toGetter(),
        addGoBackButton = true
) {

    companion object {

        private const val PERMISSION = android.Manifest.permission.WRITE_EXTERNAL_STORAGE

        private suspend fun showExplanation(layer: SuspendGetterLayer) {
            deferred<Unit> {
                DialogManager.showDialog(layer.layerManagerConnector) {
                    title("Разрешение на запись".toGetter())
                    text("Это разрешение нам очень необходимо".toGetter())
                    closeButton("Понятно".toGetter())
                    addOnClosedListener {
                        complete()
                    }
                }
            }.await()
        }

        private suspend fun requestPermission(layer: SuspendGetterLayer) =
                PermissionsManager.requestPermission(layer.context as Activity, PERMISSION) {
                    showExplanation(layer)
                }

        private val VALUE_PRODUCER = Getter { layer: SuspendGetterLayer ->

            CachingProducer.create(TimeValue.HOUR) {
                SuspendGetter.simple {
                    requestPermission(layer)
                    delay(TimeValue.SECOND)
                    "Result"
                } as GetterAsync<Unit, String>
            }

        }

    }

    override fun afterCreate() {
        super.afterCreate()

        addFrameLayout {

            setLinearParams(MATCH_PARENT, MATCH_PARENT)

            addView(
                    SuspendLoader.create(
                            context = context,
                            producer = VALUE_PRODUCER.get(this@SuspendGetterLayer),
                            waiterViewGenerator = { MaterialWaiterView(context, it) },
                            contentViewGenerator = this@SuspendGetterLayer::createContentView,
                            errorViewGenerator = this@SuspendGetterLayer::createErrorView
                    )
                            .applyFrameParams { setMatchParentSize() }
            )

            addCircleIconButton(
                    icon = DrawableGetter(R.drawable.ic_cirlce_icon_button_icon),
                    onClick = { VALUE_PRODUCER.get(this@SuspendGetterLayer).invalidate() },
                    rippleDrawInfo = ColorManager.FG_RIPPLE_DRAW_INFO
            ) {
                applyFrameParams { setEndBottomGravity() }
            }

        }


    }

    private fun createContentView(data: String) = Label(
            context = context,
            initialText = data.toGetter(),
            gravity = HGravity.CENTER,
            textSize = dp24,
            textColor = ColorGetter.BLACK
    )

    private fun createErrorView(th: Throwable) =
            LinearLayout(context).apply {

                val (title, buttonText, buttonOnClick) = when (th) {
                    is OnPermissionDeniedException -> Triple(
                            "Разрешение не дано".toGetter(),
                            "Спросить меня ещё раз".toGetter(),
                            { VALUE_PRODUCER.get(this@SuspendGetterLayer).invalidate() }
                    )
                    is OnPermissionDeniedForeverException -> Triple(
                            "Разрешение не дано, Перейдите в настройки и дайте разрешение вручную".toGetter(),
                            "Перейти в настройки".toGetter(),
                            { th.openSettingsToGivePermission(context) }
                    )
                    else -> Triple(
                            "Ошибка получения данных".toGetter(),
                            "Обновить".toGetter(),
                            { VALUE_PRODUCER.get(this@SuspendGetterLayer).invalidate() }
                    )
                }

                setCenterForegroundGravity()
                orientation = VERTICAL
                setPadding(dp64)

                addLabel(
                        text = title,
                        gravity = HGravity.CENTER,
                        textSize = dp16,
                        textColor = ColorManager.FOREGROUND
                )

                addAppButton(
                        text = buttonText,
                        onClick = buttonOnClick
                )

            }

}