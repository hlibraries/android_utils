package ru.hnau.androidutilsapp.layer.base

import android.content.Context
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.header.addHeader
import ru.hnau.androidutils.ui.view.header.addHeaderTitle
import ru.hnau.androidutils.ui.view.header.back.button.addHeaderBackButton
import ru.hnau.androidutils.ui.view.layer.layer.Layer
import ru.hnau.androidutils.ui.view.layer.layer.ManagerConnector
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setBackgroundColor
import ru.hnau.androidutils.ui.view.utils.setLinearParams
import ru.hnau.androidutilsapp.ColorManager


abstract class BaseLayer(
        context: Context,
        private val title: StringGetter,
        private val addGoBackButton: Boolean = true
) : LinearLayout(context), Layer {

    override val view = this

    @ManagerConnector
    lateinit var layerManagerConnector: LayerManagerConnector

    override fun afterCreate() {
        super.afterCreate()

        orientation = VERTICAL
        setBackgroundColor(ColorManager.BACKGROUND)

        addHeader(
                headerBackgroundColor = ColorManager.FOREGROUND
        ) {

            setLinearParams(MATCH_PARENT, WRAP_CONTENT)

            if (addGoBackButton) {
                addHeaderBackButton(
                        onClick = { layerManagerConnector.handleGoBack() },
                        color = ColorManager.BACKGROUND,
                        rippleDrawInfo = ColorManager.FG_RIPPLE_DRAW_INFO
                )
            }
            addHeaderTitle(
                    text = title,
                    textColor = ColorManager.BACKGROUND
            )
        }
    }

}