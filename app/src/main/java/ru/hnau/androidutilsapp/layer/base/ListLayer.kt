package ru.hnau.androidutilsapp.layer.base

import android.content.Context
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.list.base.addListView
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.setLinearParams
import ru.hnau.androidutilsapp.view.AppButton
import ru.hnau.jutils.producer.DataProducer


abstract class ListLayer(
        context: Context,
        title: StringGetter,
        private val items: List<Item>,
        addGoBackButton: Boolean = true
) : BaseLayer(
        context = context,
        title = title,
        addGoBackButton = addGoBackButton
) {

    data class Item(
            val title: StringGetter,
            val onClick: (ListLayer) -> Unit
    )

    private inner class ItemViewWrapper(context: Context) : FrameLayout(context),
            BaseListViewWrapper<Item> {

        override val view = this

        private var item: Item? = null

        private val label = AppButton(
                context = context,
                onClick = { item?.onClick?.invoke(this@ListLayer) }
        )

        init {
            addView(label)
        }

        override fun setContent(content: Item, position: Int) {
            item = content
            label.text = content.title
        }

    }

    override fun afterCreate() {
        super.afterCreate()
        addListView(
                itemsProducer = DataProducer(items),
                viewWrappersCreator = { ItemViewWrapper(context) }
        ) {
            setLinearParams(MATCH_PARENT, 0, 1f)
        }
    }

}