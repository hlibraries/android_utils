package ru.hnau.androidutilsapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.dp_px.*
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.ui.bounds_producer.ViewBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RoundSidesRectCanvasShape
import ru.hnau.androidutils.ui.drawer.Insets
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler
import ru.hnau.androidutilsapp.ColorManager


@SuppressLint("ViewConstructor")
class AppButton(
        context: Context,
        onClick: () -> Unit,
        initialText: StringGetter = StringGetter.EMPTY
) : Label(
        context = context,
        initialText = initialText,
        textColor = ColorManager.BACKGROUND,
        minLines = 1,
        maxLines = 1,
        textSize = dp(20),
        gravity = HGravity.CENTER
) {

    private val boundsProducer = ViewBoundsProducer(
            view = this,
            usePaddings = false
    ).applyInsents(context, Insets(dp16, dp8))

    private val canvasShape = RoundSidesRectCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClick = onClick
    )

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            animatingViewIsVisibleToUser = isVisibleToUserProducer,
            touchHandler = touchHandler,
            canvasShape = canvasShape,
            rippleDrawInfo = ColorManager.FG_RIPPLE_DRAW_INFO
    )

    init {
        setPadding(dp48, dp24)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

    override fun draw(canvas: Canvas) {
        rippleDrawer.draw(canvas)
        super.draw(canvas)
    }

}

fun <G : ViewGroup> G.addAppButton(
        text: StringGetter = StringGetter(),
        onClick: (() -> Unit),
        viewConfigurator: (AppButton.() -> Unit)? = null
) =
        addChild(
                AppButton(context, onClick, text),
                viewConfigurator
        )