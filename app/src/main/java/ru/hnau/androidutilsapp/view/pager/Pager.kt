package ru.hnau.remote_teaching_android.ui.pager

import android.annotation.SuppressLint
import android.content.Context
import android.support.design.widget.TabLayout
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutilsapp.ColorManager
import ru.hnau.jutils.helpers.VariableConnector


@SuppressLint("ViewConstructor")
open class Pager(
    context: Context,
    private val pages: List<PagerPage>
) : LinearLayout(
    context
) {

    private val adapter = object : PagerAdapter() {

        override fun isViewFromObject(view: View, any: Any) =
            view == any

        override fun getCount() =
            pages.size

        override fun instantiateItem(container: ViewGroup, position: Int) =
            pages[position].viewCreator.invoke().apply(container::addView)

        override fun destroyItem(container: ViewGroup, position: Int, any: Any) =
            container.removeView(any as View)

        override fun getPageTitle(position: Int) =
            pages[position].title.get(context)

    }

    private val pager = ViewPager(context).apply {
        setLinearParams(MATCH_PARENT, 0, 1f)
        adapter = this@Pager.adapter
    }

    private val tabLayout = TabLayout(context).apply {
        setLinearParams(MATCH_PARENT, WRAP_CONTENT)
        setupWithViewPager(pager)
        setBackgroundColor(ColorManager.BLUE)
        setTabTextColors(
            ColorManager.BACKGROUND.mapWithAlpha(0.5f).get(context),
            ColorManager.BACKGROUND.get(context)
        )
        setSelectedTabIndicatorColor(ColorManager.BACKGROUND.get(context))
        setSelectedTabIndicatorHeight(dpToPxInt(4))
        setTabRippleColorResource(android.R.color.white)
    }

    init {
        orientation = VERTICAL
        addView(tabLayout)
        addView(pager)
    }

}