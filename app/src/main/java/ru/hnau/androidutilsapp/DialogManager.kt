package ru.hnau.androidutilsapp

import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.layer.preset.dialog.DialogLayer
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogViewInfo
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.BottomSheetView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.BottomSheetViewInfo
import ru.hnau.androidutils.ui.view.layer.transaction.TransactionInfo


object DialogManager {

    fun showDialog(
            layerManager: LayerManagerConnector,
            materialDialogViewInfo: MaterialDialogViewInfo = MaterialDialogViewInfo.DEFAULT,
            dialogViewConfigurator: MaterialDialogView.() -> Unit
    ) {
        val dialogBuilder = MaterialDialogView.create(
                info = materialDialogViewInfo,
                dialogViewConfigurator = dialogViewConfigurator
        )
        layerManager.showLayer(
                layer = DialogLayer.create(
                        context = layerManager.viewContext,
                        dialogViewBuilder = dialogBuilder
                ),
                transactionInfo = TransactionInfo(
                        emersionSide = Side.BOTTOM
                )
        )
    }

    fun showBottomSheet(
            layerManager: LayerManagerConnector,
            bottomSheetViewInfo: BottomSheetViewInfo = BottomSheetViewInfo.DEFAULT,
            bottomSheetViewConfigurator: BottomSheetView.() -> Unit
    ) {
        val dialogBuilder = BottomSheetView.create(
                info = bottomSheetViewInfo,
                dialogViewConfigurator = bottomSheetViewConfigurator
        )
        layerManager.showLayer(
                layer = DialogLayer.create(
                        context = layerManager.viewContext,
                        dialogViewBuilder = dialogBuilder
                ),
                transactionInfo = TransactionInfo(
                        emersionSide = Side.BOTTOM
                )
        )
    }

}