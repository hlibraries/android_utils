package ru.hnau.androidutils.animations

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.AlwaysProducer

@Deprecated("Use FloatSmoother")
open class StatesAnimator(
        initialPosition: Int = 0,
        interStatesAnimatingTime: TimeValue = AnimationsUtils.DEFAULT_ANIMATION_TIME,
        private val onTic: ((Float) -> Unit)? = null
) : AlwaysProducer<Float>() {

    private val interStatesAnimatingTime = interStatesAnimatingTime.milliseconds.toDouble()

    var position: Float = initialPosition.toFloat()
        private set(value) {
            field = value
            onPositionChanged(value)
        }

    override val value: Float
        get() = position

    private var target: Int = initialPosition

    private var animating = false
        set(value) = synchronized(this) {
            if (field != value) {
                field = value
                if (value) {
                    AnimationMetronome.attach(this::onAnimationTic)
                } else {
                    AnimationMetronome.detach(this::onAnimationTic)
                }
            }
        }

    private val animationTicMoveAmount: Float
        get() = (AnimationMetronome.ticTime.toDouble() / interStatesAnimatingTime).toFloat()

    private fun onPositionChanged(position: Float) {
        call(position)
        onTic?.invoke(position)
    }

    fun addNewTarget(target: Int, animate: Boolean) = synchronized(this) {
        if (this.target == target) {
            return@synchronized
        }
        this.target = target
        if (!animate) {
            this.position = this.target.toFloat()
            animating = false
            return@synchronized
        }
        animating = true
    }

    fun animateTo(target: Int) = addNewTarget(target, true)

    fun switchTo(target: Int) = addNewTarget(target, false)

    private fun onAnimationTic(param: Unit): Unit = synchronized(this) {
        val moveAmount = animationTicMoveAmount
        if (position < target) {
            val position = this.position + moveAmount
            if (position >= target) {
                this.position = target.toFloat()
                animating = false
            }
            this.position = position
        } else {
            val position = this.position - moveAmount
            if (position <= target) {
                this.position = target.toFloat()
                animating = false
            }
            this.position = position
        }
    }

    override fun onLastDetached() {
        super.onLastDetached()
        synchronized(this) {
            animating = false
            position = target.toFloat()
        }
    }


}