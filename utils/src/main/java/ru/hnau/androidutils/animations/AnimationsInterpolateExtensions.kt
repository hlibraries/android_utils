package ru.hnau.androidutils.animations

import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.view.animation.BounceInterpolator
import android.view.animation.Interpolator
import android.view.animation.OvershootInterpolator
import ru.hnau.jutils.producer.Producer


val ACCELERATE_DECELERATE_INTERPOLATOR = FastOutSlowInInterpolator()
val ACCELERATE_INTERPOLATOR = FastOutLinearInInterpolator()
val DECELERATE_INTERPOLATOR = LinearOutSlowInInterpolator()
val OVERSHOOT_INTERPOLATOR = OvershootInterpolator()
val BOUNCE_INTERPOLATOR = BounceInterpolator()

fun Producer<Float>.interpolate(interpolator: Interpolator) =
        map { interpolator.getInterpolation(it) }

fun Producer<Float>.interpolateAccelerateDecelerate() =
        interpolate(ACCELERATE_DECELERATE_INTERPOLATOR)

fun Producer<Float>.interpolateAccelerate() =
        interpolate(ACCELERATE_INTERPOLATOR)

fun Producer<Float>.interpolateDecelerate() =
        interpolate(DECELERATE_INTERPOLATOR)

fun Producer<Float>.interpolateOvershoot() =
        interpolate(OVERSHOOT_INTERPOLATOR)

fun Producer<Float>.interpolateBounce() =
        interpolate(BOUNCE_INTERPOLATOR)


val Interpolator.inverted: Interpolator
    get() = Interpolator { 1f - this.getInterpolation(1f - it) }