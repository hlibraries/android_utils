package ru.hnau.androidutils.animations

import ru.hnau.jutils.TimeValue

object AnimationsUtils {

    val DEFAULT_ANIMATION_TIME = TimeValue.SECOND * 0.3

}