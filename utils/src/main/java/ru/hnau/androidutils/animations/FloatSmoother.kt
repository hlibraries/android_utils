package ru.hnau.androidutils.animations

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.StateProducer
import ru.hnau.jutils.producer.StateProducerSimple
import ru.hnau.jutils.producer.extensions.callIfFalse
import ru.hnau.jutils.producer.extensions.observeWhen
import ru.hnau.jutils.takeIfPositive


class FloatSmoother(
        private val targetProducer: Producer<Float>,
        interStatesAnimatingMilliseconds: Double = INTER_STATES_ANIMATING_MILLISECONDS_DEFAULT
) : StateProducer<Float>() {

    companion object {

        val INTER_STATES_ANIMATING_MILLISECONDS_DEFAULT = AnimationsUtils.DEFAULT_ANIMATION_TIME.milliseconds.toDouble()

    }

    constructor(
            targetProducer: Producer<Float>,
            interStatesAnimatingTime: TimeValue
    ) : this(
            targetProducer = targetProducer,
            interStatesAnimatingMilliseconds = interStatesAnimatingTime.milliseconds.toDouble()
    )

    private val ticOffset = (AnimationMetronome.ticTime.toDouble() /
            (interStatesAnimatingMilliseconds.takeIfPositive()
                    ?: throw IllegalArgumentException("interStatesAnimatingMilliseconds must be positive"))).toFloat()

    private val isAnimatingProducer =
            StateProducerSimple<Boolean>()

    private val isObservingProducer =
            StateProducerSimple<Boolean>()

    private var targetInitialized = false
    private var target = 0f

    private var animatingPositive = false

    private var currentState: Float
        set(value) {
            update(value)
        }
        get() = state ?: 0f

    init {

        AnimationMetronome
                .observeWhen(isAnimatingProducer)
                .attach { onAnimationTic() }

        isObservingProducer
                .callIfFalse()
                .attach { finish() }

        targetProducer
                .observeWhen(isObservingProducer)
                .attach(this::onTargetChanged)
    }

    private fun onTargetChanged(target: Float) {

        this.target = target

        if (!targetInitialized) {
            targetInitialized = true
            finish()
            return
        }

        animatingPositive = target > currentState
        startAnimation()
    }

    private fun onAnimationTic() {

        val offset = if (animatingPositive) ticOffset else -ticOffset
        val newState = currentState + offset

        val stop = if (animatingPositive) newState >= target else newState <= target
        if (stop) {
            currentState = target
            stopAnimation()
            return
        }

        currentState = newState
    }

    private fun startAnimation() =
            isAnimatingProducer.updateStateIfChanged(true)

    private fun stopAnimation() =
            isAnimatingProducer.updateStateIfChanged(false)

    fun finish() {
        currentState = target
        stopAnimation()
    }

    override fun onIsObservingChanged(isObserving: Boolean) {
        super.onIsObservingChanged(isObserving)
        isObservingProducer.updateState(isObserving)
    }

}

fun Producer<Float>.smooth(
        interStatesAnimatingMilliseconds: Double = FloatSmoother.INTER_STATES_ANIMATING_MILLISECONDS_DEFAULT
) = FloatSmoother(
        targetProducer = this,
        interStatesAnimatingMilliseconds = interStatesAnimatingMilliseconds
)

fun Producer<Float>.smooth(
        interStatesAnimatingTime: TimeValue
) = FloatSmoother(
        targetProducer = this,
        interStatesAnimatingTime = interStatesAnimatingTime
)