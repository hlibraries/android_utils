package ru.hnau.androidutils.animations

import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.view.animation.Interpolator
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.AlwaysProducer
import ru.hnau.jutils.toInt


@Deprecated("Use FloatSmoother")
open class TwoStatesAnimator(
        initPositive: Boolean = false,
        switchingTime: TimeValue = AnimationsUtils.DEFAULT_ANIMATION_TIME,
        private val interpolator: Interpolator = FastOutSlowInInterpolator(),
        private val onTic: ((Float) -> Unit)? = null
) : AlwaysProducer<Float>() {

    var position: Float = initPositive.toInt().toFloat()
        private set(value) {
            field = value
            onPositionChanged(value)
        }

    override val value: Float
        get() = position

    private val statesAnimator = StatesAnimator(
            initialPosition = initPositive.toInt(),
            interStatesAnimatingTime = switchingTime
    )

    init {
        onTic?.let {
            statesAnimator.attach(this::onStatesPositionChanged)
        }
    }

    private fun onPositionChanged(position: Float) {
        call(position)
        onTic?.invoke(position)
    }

    fun addNewTarget(targetIsPositive: Boolean, animate: Boolean) =
            statesAnimator.addNewTarget(targetIsPositive.toInt(), animate)

    fun animateTo(targetIsPositive: Boolean) = addNewTarget(targetIsPositive, true)

    fun switchTo(targetIsPositive: Boolean) = addNewTarget(targetIsPositive, true)

    fun toPositive(animate: Boolean) = addNewTarget(true, animate)

    fun toNegative(animate: Boolean) = addNewTarget(false, animate)

    fun animateToPositive() = animateTo(true)

    fun animateToNegative() = animateTo(false)

    fun switchToPositive() = switchTo(true)

    fun switchToNegative() = switchTo(false)

    private fun onStatesPositionChanged(statesPosition: Float) {
        val normalizedStatesPosition = statesPosition.coerceIn(0f, 1f)
        position = interpolator.getInterpolation(normalizedStatesPosition)
    }

    override fun onFirstAttached() {
        super.onFirstAttached()
        if (onTic == null) {
            statesAnimator.attach(this::onStatesPositionChanged)
        }
    }

    override fun onLastDetached() {
        super.onLastDetached()
        if (onTic == null) {
            statesAnimator.detach(this::onStatesPositionChanged)
        }
    }


}