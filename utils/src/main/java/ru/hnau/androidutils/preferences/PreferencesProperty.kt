package ru.hnau.androidutils.preferences

import android.content.SharedPreferences
import ru.hnau.jutils.getter.MutableGetter
import ru.hnau.jutils.getter.base.get
import ru.hnau.jutils.producer.AlwaysProducer
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


abstract class PreferencesProperty<T : Any>(
        private val preferences: SharedPreferences,
        private val key: String,
        protected val defaultValue: T,
        private val mutableType: Boolean
) : AlwaysProducer<T>(), ReadWriteProperty<Any, T> {

    private val valueGetter =
            MutableGetter.simple { readValue(preferences, key) ?: defaultValue }

    override val value: T
        get() = valueGetter.get()

    override operator fun getValue(thisRef: Any, property: KProperty<*>) = valueGetter.get()

    override operator fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        synchronized(this) {
            if (!mutableType && value == valueGetter.get()) {
                return
            }
            valueGetter.setValue(value)
            val editor = preferences.edit()
            writeValue(editor, key, value)
            editor.apply()
            return@synchronized
        }
        call(value)
    }

    abstract fun readValue(container: SharedPreferences, key: String): T?

    abstract fun writeValue(editor: SharedPreferences.Editor, key: String, value: T)

}