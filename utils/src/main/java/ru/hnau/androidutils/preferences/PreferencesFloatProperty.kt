package ru.hnau.androidutils.preferences

import android.content.SharedPreferences


class PreferencesFloatProperty(
        preferences: SharedPreferences,
        key: String,
        defaultValue: Float = 0f
) : PreferencesProperty<Float>(
        preferences = preferences,
        key = key,
        defaultValue = defaultValue,
        mutableType = false
) {

    override fun readValue(container: SharedPreferences, key: String) =
            container.getFloat(key, defaultValue)

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: Float) {
        editor.putFloat(key, value)
    }
}