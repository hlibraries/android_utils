package ru.hnau.androidutils.preferences

import android.content.SharedPreferences
import java.lang.reflect.Type


abstract class PreferencesPackToStringProperty<T : Any>(
        preferences: SharedPreferences,
        key: String,
        defaultValue: T
) : PreferencesProperty<T>(
        preferences = preferences,
        key = key,
        defaultValue = defaultValue,
        mutableType = false
) {

    companion object {

        fun <T : Any> create(
                preferences: SharedPreferences,
                key: String,
                defaultValue: T,
                valueToStringConverter: (value: T) -> String,
                stringToValueConverter: (string: String) -> T
        ) = object : PreferencesPackToStringProperty<T>(
                preferences = preferences,
                key = key,
                defaultValue = defaultValue
        ) {
            override fun valueToString(value: T) = valueToStringConverter(value)
            override fun stringToValue(string: String) = stringToValueConverter(string)
        }

    }

    protected abstract fun valueToString(value: T): String

    protected abstract fun stringToValue(string: String): T

    override fun readValue(container: SharedPreferences, key: String) =
            container.getString(key, null)?.let(this::stringToValue) ?: defaultValue

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: T) {
        editor.putString(key, valueToString(value))
    }

}