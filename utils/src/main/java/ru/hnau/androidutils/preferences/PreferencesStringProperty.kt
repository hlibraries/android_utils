package ru.hnau.androidutils.preferences

import android.content.SharedPreferences


class PreferencesStringProperty(
        preferences: SharedPreferences,
        key: String,
        defaultValue: String = ""
) : PreferencesProperty<String>(
        preferences = preferences,
        key = key,
        defaultValue = defaultValue,
        mutableType = false
) {

    override fun readValue(container: SharedPreferences, key: String) =
            container.getString(key, defaultValue)!!

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: String) {
        editor.putString(key, value)
    }
}