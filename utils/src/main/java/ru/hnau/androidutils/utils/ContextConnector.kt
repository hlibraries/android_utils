package ru.hnau.androidutils.utils

import android.annotation.SuppressLint
import android.content.Context


@SuppressLint("StaticFieldLeak")
object ContextConnector {

    var customContextProducer: (() -> Context?)? = null

    private var appContext: Context? = null

    val context: Context
        get() = customContextProducer?.invoke() ?: appContext!!

    fun init(appContext: Context) {
        ContextConnector.appContext = appContext
    }

}