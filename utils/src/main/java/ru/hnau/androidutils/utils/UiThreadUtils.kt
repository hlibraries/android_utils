package ru.hnau.androidutils.utils

import android.os.Handler
import android.os.Looper
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.handle
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.producer.Producer
import java.lang.IllegalStateException

val MAIN_LOOPER = Looper.getMainLooper()!!

fun runUi(action: () -> Unit) {
    runIfUi(action) ?: postMain(action)
}

val isInMainThread: Boolean
    get() = Looper.myLooper() == MAIN_LOOPER

fun throwIfNotInMainThread() {
    isInMainThread.ifFalse {
        throw IllegalStateException("Not in main thread")
    }
}

inline fun <T : Any> runIfUi(action: () -> T) =
        isInMainThread.handle(
                onTrue = { action.invoke() },
                onFalse = { null }
        )

fun <T> finishUi(action: () -> T) = Finisher<T> { onFinished ->
    runUi { onFinished.invoke(action.invoke()) }
}

fun <T> Finisher<T>.mapUi() =
        Finisher<T> { onFinished ->
            await { data ->
                runUi { onFinished.invoke(data) }
            }
        }

fun <T : Any> Producer<T>.mapUi() =
        mapAsync<T> { data, onConverted ->
            runUi { onConverted.invoke(data) }
        }

fun Handler.postDelayed(pause: TimeValue, action: () -> Unit) =
        postDelayed(action, pause.milliseconds)


private val MAIN_THREAD_HANDLER = Handler(MAIN_LOOPER)

fun postMain(action: () -> Unit) =
        MAIN_THREAD_HANDLER.post(action)

fun postDelayerMain(pause: TimeValue, action: () -> Unit) =
        MAIN_THREAD_HANDLER.postDelayed(pause, action)