package ru.hnau.androidutils.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import ru.hnau.jutils.helpers.weak.Weak


object KeyboardManager {

    private fun getInputMethodManager(context: Context) =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    private var lastView: Weak<View>? = null

    fun showAndRequestFocus(view: View?) {
        if (view == null) {
            return
        }
        lastView = Weak(view)
        if (view.requestFocus()) {
            getInputMethodManager(view.context)
                    .showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    fun hide() {
        val lastView = this.lastView?.value ?: return
        getInputMethodManager(lastView.context)
                .hideSoftInputFromWindow(lastView.windowToken, 0)
    }
}