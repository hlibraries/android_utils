package ru.hnau.androidutils.utils

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import ru.hnau.jutils.tryOrElse
import ru.hnau.jutils.tryOrNull


class ReconfigurableBitmapContainer {

    companion object {

        private val CONFIGURATIONS_PIXEL_SIZE = hashMapOf(
                Bitmap.Config.ALPHA_8 to 1,
                Bitmap.Config.RGB_565 to 2,
                Bitmap.Config.ARGB_4444 to 2,
                Bitmap.Config.ARGB_8888 to 4
        )

        private fun reconfigureOrCreateNew(
                bitmap: Bitmap?,
                newWidth: Int,
                newHeight: Int,
                newConfig: Bitmap.Config
        ): Bitmap? {
            val reconfiguredBitmap = tryReconfigureBitmap(bitmap, newWidth, newHeight, newConfig)
            if (reconfiguredBitmap != null) {
                if (reconfiguredBitmap.isMutable) {
                    reconfiguredBitmap.eraseColor(Color.TRANSPARENT)
                }
                return reconfiguredBitmap
            }
            bitmap?.recycle()
            return tryOrNull { Bitmap.createBitmap(newWidth, newHeight, newConfig) }
        }

        private fun tryReconfigureBitmap(
                bitmap: Bitmap?,
                newWidth: Int,
                newHeight: Int,
                newConfig: Bitmap.Config
        ): Bitmap? {

            if (
                    Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT ||
                    bitmap == null ||
                    bitmap.isRecycled
            ) {
                return null
            }

            if (
                    bitmap.width == newWidth &&
                    bitmap.height == newHeight &&
                    bitmap.config == newConfig
            ) {
                return bitmap
            }

            if (!bitmap.isMutable) {
                return null
            }

            val newConfigPixelSizeInBytes = CONFIGURATIONS_PIXEL_SIZE[newConfig] ?: return null
            val newBitmapBytesCount = newWidth * newHeight * newConfigPixelSizeInBytes
            val allocatedBytes = bitmap.allocationByteCount
            if (allocatedBytes < newBitmapBytesCount) {
                return null
            }
            
            return tryOrElse(
                    throwsAction = {
                        bitmap.apply { reconfigure(newWidth, newHeight, newConfig) }
                    },
                    onThrow = {
                        bitmap.apply { recycle() }
                        null
                    }
            )
        }

    }

    private var bitmap: Bitmap? = null

    fun getBitmap(width: Int, height: Int, config: Bitmap.Config) =
            reconfigureOrCreateNew(bitmap, width, height, config)

    fun clear() {
        bitmap?.recycle()
        bitmap = null
    }

}