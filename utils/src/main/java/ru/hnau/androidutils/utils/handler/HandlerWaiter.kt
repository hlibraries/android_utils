package ru.hnau.androidutils.utils.handler

import android.os.Handler
import ru.hnau.jutils.TimeValue


class HandlerWaiter(
        private val handler: Handler = Handler(),
        block: () -> Unit
) {

    private val blockExecutor = Runnable {
        synchronized(this) {
            block.invoke()
            started = false
        }
    }

    var started = false
        private set

    fun start(time: TimeValue) = synchronized(this) {
        cancel()
        handler.postDelayed(blockExecutor, time.milliseconds)
        started = true
    }

    fun cancel() = synchronized(this) {
        handler.removeCallbacks(blockExecutor)
        started = false
    }

}