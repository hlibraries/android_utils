package ru.hnau.androidutils.coroutines

import android.view.View
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import ru.hnau.jutils.coroutines.JobExecutor
import ru.hnau.jutils.coroutines.executor.InterruptableExecutor
import ru.hnau.jutils.producer.Producer


class UIJob(
        isVisibleToUserProducer: Producer<Boolean>,
        errorsHandler: (Throwable) -> Unit = DEFAULT_ERRORS_HANDLER
) : JobExecutor(
        isActiveProducer = isVisibleToUserProducer,
        errorsHandler = errorsHandler
) {

    override fun createCoroutineContext(job: Job) =
            job + Dispatchers.Main

}

fun View.createUIJob(
        isVisibleToUserProducer: Producer<Boolean>,
        errorsHandler: (Throwable) -> Unit = JobExecutor.DEFAULT_ERRORS_HANDLER
) = UIJob(
        isVisibleToUserProducer = isVisibleToUserProducer,
        errorsHandler = errorsHandler
)