package ru.hnau.androidutils.ui.drawables.layout_drawable.view

import android.content.Context
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutType
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity

fun LayoutDrawableView.Companion.create(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        layoutType: LayoutType,
        gravity: HGravity = HGravity.CENTER
) =
        LayoutDrawableView(context, content, layoutType, gravity)

fun LayoutDrawableView.Companion.createStretched(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.Stretched, gravity)

fun LayoutDrawableView.Companion.createIndependent(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.Independent, gravity)

fun LayoutDrawableView.Companion.createInside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.Inside, gravity)

fun LayoutDrawableView.Companion.createOutside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.Outside, gravity)

fun LayoutDrawableView.Companion.createLargerOrOutside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.LargerOrOutside, gravity)

fun LayoutDrawableView.Companion.createLargerOrInside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.LargerOrInside, gravity)

fun LayoutDrawableView.Companion.createSmallerOrOutside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.SmallerOrOutside, gravity)

fun LayoutDrawableView.Companion.createSmallerOrInside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.SmallerOrInside, gravity)