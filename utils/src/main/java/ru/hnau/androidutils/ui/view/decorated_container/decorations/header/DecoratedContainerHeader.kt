package ru.hnau.androidutils.ui.view.decorated_container.decorations.header

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.decorated_container.DecoratedContainerDecoration
import ru.hnau.androidutils.ui.view.shadow_line.addTopShadowLine
import ru.hnau.androidutils.ui.view.utils.apply.*
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyFrameParams
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.toProducer


abstract class DecoratedContainerHeader(
        context: Context,
        info: DecoratedContainerHeaderInfo = DecoratedContainerHeaderInfo.DEFAULT,
        title: Producer<StringGetter> = DEFAULT_TITLE,
        goBackAvailable: Producer<Boolean> = DEFAULT_GO_BACK_AVAILABLE,
        options: Producer<Collection<DecoratedContainerHeaderOption>> = DEFAULT_OPTIONS,
        contentIsScrolledToUp: Producer<Boolean> = DEFAULT_CONTENT_IS_SCROLLED_TO_UP
) : LinearLayout(
        context
), DecoratedContainerDecoration {

    companion object {

        val DEFAULT_TITLE = StringGetter.EMPTY.toProducer()
        val DEFAULT_GO_BACK_AVAILABLE = true.toProducer()
        val DEFAULT_OPTIONS = emptyList<DecoratedContainerHeaderOption>().toProducer() as Producer<Collection<DecoratedContainerHeaderOption>>
        val DEFAULT_CONTENT_IS_SCROLLED_TO_UP = false.toProducer()

    }

    private val content = DecoratedContainerHeaderContent(
            context = context,
            info = info,
            title = title,
            goBackAvailable = goBackAvailable,
            options = options
    )

    override val view = this
    override val side = Side.TOP
    override val preferredSize = content.preferredHeight

    init {
        applyVerticalOrientation()
        applyFrameParams {
            setMatchParentWidth()
            setTopGravity()
        }
        addChild(content)
        addTopShadowLine(
                isVisibleProducer = contentIsScrolledToUp,
                info = info.shadow
        )
    }

}