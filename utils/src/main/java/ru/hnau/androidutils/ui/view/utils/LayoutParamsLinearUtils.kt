package ru.hnau.androidutils.ui.view.utils

import android.view.Gravity
import android.widget.LinearLayout


@Deprecated("Use LinearLayout.LayoutParams.setGravity(gravity: HGravity)")
fun LinearLayout.LayoutParams.setLinearParamsCenterGravity() {
    gravity = Gravity.CENTER
}

@Deprecated("Use LinearLayout.LayoutParams.setGravity(gravity: HGravity)")
fun LinearLayout.LayoutParams.setLinearParamsStartTopGravity() {
    gravity = Gravity.START or Gravity.TOP
}

@Deprecated("Use LinearLayout.LayoutParams.setGravity(gravity: HGravity)")
fun LinearLayout.LayoutParams.setLinearParamsEndTopGravity() {
    gravity = Gravity.END or Gravity.TOP
}

@Deprecated("Use LinearLayout.LayoutParams.setGravity(gravity: HGravity)")
fun LinearLayout.LayoutParams.setLinearParamsEndBottomGravity() {
    gravity = Gravity.END or Gravity.BOTTOM
}

@Deprecated("Use LinearLayout.LayoutParams.setGravity(gravity: HGravity)")
fun LinearLayout.LayoutParams.setLinearParamsStartBottomGravity() {
    gravity = Gravity.START or Gravity.BOTTOM
}

@Deprecated("Use LinearLayout.LayoutParams.setGravity(gravity: HGravity)")
fun LinearLayout.LayoutParams.setLinearParamsTopCenterHorizontalGravity() {
    gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
}

@Deprecated("Use LinearLayout.LayoutParams.setGravity(gravity: HGravity)")
fun LinearLayout.LayoutParams.setLinearParamsBottomCenterHorizontalGravity() {
    gravity = Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM
}

@Deprecated("Use LinearLayout.LayoutParams.setGravity(gravity: HGravity)")
fun LinearLayout.LayoutParams.setLinearParamsStartCenterVerticalGravity() {
    gravity = Gravity.START or Gravity.CENTER_VERTICAL
}

@Deprecated("Use LinearLayout.LayoutParams.setGravity(gravity: HGravity)")
fun LinearLayout.LayoutParams.setLinearParamsEndCenterVerticalGravity() {
    gravity = Gravity.END or Gravity.CENTER_VERTICAL
}