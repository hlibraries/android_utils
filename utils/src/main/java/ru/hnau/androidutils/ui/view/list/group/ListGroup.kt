package ru.hnau.androidutils.ui.view.list.group


data class ListGroup<G, T>(
        val group: G,
        val items: List<T>
)