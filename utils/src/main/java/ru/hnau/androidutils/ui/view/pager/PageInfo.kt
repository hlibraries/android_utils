package ru.hnau.androidutils.ui.view.pager

import ru.hnau.androidutils.utils.savable.Savable


data class PageInfo<S : Savable>(
        val pageClass: Class<out Page<S>>,
        val state: S
) : PageInfoContainer<S> {

    override val pageInfo: PageInfo<S>
        get() = this

}