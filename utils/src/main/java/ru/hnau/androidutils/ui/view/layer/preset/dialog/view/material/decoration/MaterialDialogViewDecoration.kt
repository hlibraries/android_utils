package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.decoration

import android.content.Context
import android.graphics.*
import android.view.View
import ru.hnau.androidutils.ui.bounds_producer.BoundsProducer
import ru.hnau.androidutils.ui.bounds_producer.ViewBasedBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.PathCanvasShape
import ru.hnau.androidutils.ui.canvas_shape.RoundCornersRectCanvasShape
import ru.hnau.androidutils.ui.drawer.shadow.drawer.ShadowDrawer
import ru.hnau.androidutils.ui.utils.types_utils.initAsRoundCornerRect
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogViewDecoration
import ru.hnau.androidutils.ui.view.utils.makeAtMostMeasureSpec
import kotlin.math.min


class MaterialDialogViewDecoration(
        private val context: Context,
        private val info: MaterialDialogViewDecorationInfo
) : DialogViewDecoration {

    private val paddingX = info.paddingHorizontal.getPxInt(context)
    private val paddingY = info.paddingVertical.getPxInt(context)

    private val panelPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = info.background.get(context)
    }

    private val boundsProducer =
            MaterialDialogViewDecorationBoundsProducer()

    private val canvasShape = RoundCornersRectCanvasShape(
            context = context,
            cornersRadius = info.borderRadius,
            boundsProducer = boundsProducer
    )

    private val shadowDrawer = ShadowDrawer(
            context = context,
            shadowInfo = info.shadow,
            canvasShape = canvasShape
    )

    override fun draw(canvas: Canvas) {
        shadowDrawer.draw(canvas)
        canvasShape.draw(canvas, panelPaint)
    }

    override fun layoutDialogView(maxWidth: Int, maxHeight: Int, dialogView: View) {

        val width = min(dialogView.measuredWidth, maxWidth - paddingX * 2)
        val height = min(dialogView.measuredHeight, maxHeight - paddingY * 2)
        val left = (maxWidth - width) / 2
        val top = (maxHeight - height) / 2

        dialogView.layout(left, top, left + width, top + height)

        boundsProducer.onDialogLayout(dialogView)

    }

    override fun measureDialogView(maxWidth: Int, maxHeight: Int, dialogView: View) {
        dialogView.measure(
                makeAtMostMeasureSpec(maxWidth - paddingX * 2),
                makeAtMostMeasureSpec(maxHeight - paddingY * 2)
        )
    }

}