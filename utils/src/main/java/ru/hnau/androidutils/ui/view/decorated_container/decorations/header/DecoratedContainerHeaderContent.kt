package ru.hnau.androidutils.ui.view.decorated_container.decorations.header

import android.app.Activity
import android.content.Context
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp0
import ru.hnau.androidutils.ui.utils.ScreenManager
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.clickable.ClickableLayoutDrawableView
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.apply.applyCenterGravity
import ru.hnau.androidutils.ui.view.utils.apply.applyHorizontalOrientation
import ru.hnau.androidutils.ui.view.utils.apply.applyTopPadding
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.view_presenter.*
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen


class DecoratedContainerHeaderContent(
        context: Context,
        private val info: DecoratedContainerHeaderInfo = DecoratedContainerHeaderInfo.DEFAULT,
        title: Producer<StringGetter>,
        goBackAvailable: Producer<Boolean>,
        options: Producer<Collection<DecoratedContainerHeaderOption>>
) : LinearLayout(
        context
) {

    private val preferredContnetHeight =
            info.height

    private val statusBarHeightIfUnderStatusBar =
            info.underStatusBar.handle(
                    onTrue = { ScreenManager.statusBarHeight },
                    onFalse = { dp0 }
            )

    val preferredHeight: DpPxGetter
        get() = preferredContnetHeight + statusBarHeightIfUnderStatusBar

    private val presenterPreferredSizeSizeInterpolator =
            SizeInterpolator.create(
                    context = context,
                    size = preferredContnetHeight
            )

    private val buttonPresentersInfo =
            PresenterViewInfo(
                    horizontalSizeInterpolator = presenterPreferredSizeSizeInterpolator,
                    verticalSizeInterpolator = presenterPreferredSizeSizeInterpolator
            )

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val goBackButtonPresenter = run {

        val goBackButtonPresentingInfo: PresentingViewInfo by lazy {

            val goBackButton = createButton(info.backButtonDrawable) {
                (context as Activity).onBackPressed()
            }

            PresentingViewInfo(
                    view = goBackButton,
                    properties = PresentingViewProperties(
                            gravity = HGravity.END_CENTER_VERTICAL,
                            fromSide = Side.START,
                            toSide = Side.START
                    )
            )
        }

        PresenterView(
                context = context,
                presenterViewInfo = buttonPresentersInfo,
                presentingViewProducer = goBackAvailable
                        .observeWhen(isVisibleToUserProducer)
                        .map { canGoBack ->
                            canGoBack.handle(
                                    forTrue = goBackButtonPresentingInfo,
                                    forFalse = PresentingViewInfo.EMPTY
                            )
                        }
        ).applyLinearParams {
            setHeight(preferredContnetHeight)
        }
    }

    private val titlePresenter = PresenterView(
            context = context,
            presenterViewInfo = PresenterViewInfo(
                    horizontalSizeInterpolator = SizeInterpolator.MAX,
                    verticalSizeInterpolator = presenterPreferredSizeSizeInterpolator
            ),
            presentingViewProducer = title
                    .observeWhen(isVisibleToUserProducer)
                    .map { title ->
                        PresentingViewInfo(
                                view = Label(
                                        context = context,
                                        info = info.titleLabelInfo,
                                        initialText = title
                                ),
                                properties = PresentingViewProperties(
                                        gravity = HGravity.CENTER,
                                        fromSide = Side.BOTTOM,
                                        toSide = Side.BOTTOM
                                )
                        )
                    }
    )
            .applyLinearParams {
                setStretchedWidth()
                setHeight(preferredContnetHeight)
            }

    private val optionsButtonPresenter = run {

        val optionsButtonPresentingInfo: PresentingViewInfo by lazy {

            val optionsButton = createButton(info.optionsButtonDrawable) {
                //TODO show options
            }

            PresentingViewInfo(
                    view = optionsButton,
                    properties = PresentingViewProperties(
                            gravity = HGravity.START_CENTER_VERTICAL,
                            fromSide = Side.END,
                            toSide = Side.END
                    )
            )
        }

        PresenterView(
                context = context,
                presenterViewInfo = buttonPresentersInfo,
                presentingViewProducer = options
                        .map { it.isNotEmpty() }
                        .observeWhen(isVisibleToUserProducer)
                        .map { optionsExists ->
                            optionsExists.handle(
                                    forTrue = optionsButtonPresentingInfo,
                                    forFalse = PresentingViewInfo.EMPTY
                            )
                        }
        ).applyLinearParams {
            setHeight(preferredContnetHeight)
        }
    }

    init {
        applyTopPadding(statusBarHeightIfUnderStatusBar)
        applyHorizontalOrientation()
        applyCenterGravity()
        addChild(goBackButtonPresenter)
        addChild(titlePresenter)
        addChild(optionsButtonPresenter)
    }

    private fun createButton(
            drawable: DrawableGetter,
            onClick: () -> Unit
    ) = object : ClickableLayoutDrawableView(
            context = context,
            onClick = onClick,
            rippleDrawInfo = info.buttonRippleDrawInfo,
            initialContent = drawable
    ) {

        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            val width = getDefaultMeasurement(widthMeasureSpec, preferredContnetHeight.getPxInt(context))
            val height = getDefaultMeasurement(heightMeasureSpec, preferredContnetHeight.getPxInt(context))
            setMeasuredDimension(width, height)
        }

    }

}