package ru.hnau.androidutils.ui.utils

import android.util.Log

val <T : Any> T.logginTag: String
    get() = javaClass.simpleName

fun <T : Any> T.logV(message: String, throwable: Throwable? = null) =
        Log.v(logginTag, message, throwable)

fun <T : Any> T.logI(message: String, throwable: Throwable? = null) =
        Log.i(logginTag, message, throwable)

fun <T : Any> T.logD(message: String, throwable: Throwable? = null) =
        Log.d(logginTag, message, throwable)

fun <T : Any> T.logW(message: String, throwable: Throwable? = null) =
        Log.w(logginTag, message, throwable)

fun <T : Any> T.logE(message: String, throwable: Throwable? = null) =
        Log.e(logginTag, message, throwable)

fun <T : Any> T.logWTF(message: String, throwable: Throwable? = null) =
        Log.wtf(logginTag, message, throwable)