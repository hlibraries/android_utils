package ru.hnau.androidutils.ui.bounds_producer

import android.content.Context
import android.graphics.RectF
import ru.hnau.androidutils.ui.drawer.Insets
import ru.hnau.androidutils.ui.view.utils.isLTR
import ru.hnau.androidutils.utils.ContextConnector.init
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.producer.AlwaysProducer


open class BoundsProducer : AlwaysProducer<RectF>() {

    private val bounds = RectF()

    override val value: RectF
        get() = bounds

    private val tempBounds = RectF()

    protected fun editBounds(editor: RectF.() -> Unit) {
        tempBounds.set(bounds)
        editor.invoke(bounds)
        (tempBounds == bounds).ifTrue { return }
        call(bounds)
    }


    fun map(
            converter: (fromBounds: RectF, toBounds: RectF) -> Unit
    ) = BoundsProducer().also { result ->
        attach { innerBounds ->
            result.editBounds { converter.invoke(innerBounds, this) }
        }
    }

    fun applyInsents(
            context: Context,
            insets: Insets
    ) = map { fromBounds, toBounds ->
        toBounds.set(
                fromBounds.left + insets.left.getPxInt(context),
                fromBounds.top + insets.top.getPxInt(context),
                fromBounds.right - insets.right.getPxInt(context),
                fromBounds.bottom - insets.bottom.getPxInt(context)
        )
    }


}