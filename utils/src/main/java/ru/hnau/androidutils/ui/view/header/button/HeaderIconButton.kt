package ru.hnau.androidutils.ui.view.header.button

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.horizontalPaddingSum
import ru.hnau.androidutils.ui.view.utils.verticalPaddingSum


@SuppressLint("ViewConstructor")
class HeaderIconButton(
        context: Context,
        onClick: () -> Unit,
        icon: DrawableGetter,
        rippleDrawInfo: RippleDrawInfo
) : HeaderButton(
        context,
        onClick,
        rippleDrawInfo
) {

    private val icon = icon.get(context)

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        icon.draw(canvas)
    }

    override fun layout(l: Int, t: Int, r: Int, b: Int) {
        super.layout(l, t, r, b)
        icon.setBounds(paddingLeft, paddingTop, width - horizontalPaddingSum, height - verticalPaddingSum)
    }

}

fun <G: ViewGroup> G.addHeaderIconButton(
        onClick: () -> Unit,
        icon: DrawableGetter,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (HeaderIconButton.() -> Unit)? = null
) =
        addChild(
                HeaderIconButton(
                        context,
                        onClick,
                        icon,
                        rippleDrawInfo
                ),
                viewConfigurator
        )