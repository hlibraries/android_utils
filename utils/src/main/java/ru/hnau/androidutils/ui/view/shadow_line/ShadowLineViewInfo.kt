package ru.hnau.androidutils.ui.view.shadow_line

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.toProducer


data class ShadowLineViewInfo(
        val maxAlpha: Int = 64,
        val color: ColorGetter = ColorGetter.BLACK,
        val preferredSize: DpPxGetter = dp8
) {


    companion object {

        val DEFAULT = ShadowLineViewInfo()

    }

}