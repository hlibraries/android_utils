package ru.hnau.androidutils.ui.view.list.base

import android.support.v7.widget.helper.ItemTouchHelper


enum class SwipeOrDragDirection(
        val flag: Int
) {

    LEFT(ItemTouchHelper.LEFT),
    UP(ItemTouchHelper.UP),
    RIGHT(ItemTouchHelper.RIGHT),
    DOWN(ItemTouchHelper.DOWN),
    START(ItemTouchHelper.START),
    END(ItemTouchHelper.END);

    companion object {

        fun findByFlag(flag: Int) = values().find { it.flag == flag }

    }

}

fun Set<SwipeOrDragDirection>.combineFlags() =
        fold(0) { acc, direction ->
            acc or direction.flag
        }