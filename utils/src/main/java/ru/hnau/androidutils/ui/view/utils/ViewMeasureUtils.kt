package ru.hnau.androidutils.ui.view.utils

import android.graphics.Point
import android.view.View
import kotlin.math.min


fun View.getDefaultMeasurement(measureSpec: Int, size: Int): Int {
    var result = size
    val specMode = getMeasuredSpecMode(measureSpec)
    val specSize = getMeasuredSpecSize(measureSpec)

    when (specMode) {
        View.MeasureSpec.AT_MOST -> result = min(size, specSize)
        View.MeasureSpec.UNSPECIFIED -> result = size
        View.MeasureSpec.EXACTLY -> result = specSize
    }
    return result
}

fun View.getMaxMeasurement(measureSpec: Int, sizeForUnspecified: Int) =
        getMaxMeasurementOrNullIfUnspecified(measureSpec) ?: sizeForUnspecified

fun View.getMaxMeasurementOrNullIfUnspecified(measureSpec: Int): Int? {
    val specMode = getMeasuredSpecMode(measureSpec)
    return if (specMode == View.MeasureSpec.UNSPECIFIED) {
        null
    } else getMeasuredSpecSize(measureSpec)
}

fun View.makeMeasureSpec(size: Int, mode: Int) =
        View.MeasureSpec.makeMeasureSpec(size, mode)

fun View.getMeasuredSpecSize(measureSpec: Int) =
        View.MeasureSpec.getSize(measureSpec)

fun View.getMeasuredSpecMode(measureSpec: Int) =
        View.MeasureSpec.getMode(measureSpec)

fun View.getMeasuredSize(result: Point) =
        result.set(measuredWidth, measuredHeight)

fun View.resizeMeasureSpec(measureSpec: Int, delta: Int): Int {
    val mode = getMeasuredSpecMode(measureSpec)
    if (getMeasuredSpecMode(measureSpec) == View.MeasureSpec.UNSPECIFIED) {
        return measureSpec
    }
    val size = getMeasuredSpecSize(measureSpec) + delta
    return makeMeasureSpec(size, mode)
}