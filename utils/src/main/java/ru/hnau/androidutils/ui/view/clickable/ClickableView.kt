package ru.hnau.androidutils.ui.view.clickable

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RectCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


@SuppressLint("ViewConstructor")
open class ClickableView(
        context: Context,
        private val onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : View(
        context
) {

    private val boundsProducer =
            createBoundsProducer(false)

    private val canvasShape = RectCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClick = this::onClick
    )

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            animatingViewIsVisibleToUser = isVisibleToUserProducer,
            touchHandler = touchHandler,
            canvasShape = canvasShape,
            rippleDrawInfo = rippleDrawInfo
    )

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

    protected open fun onClick() {
        onClick?.invoke()
    }

    override fun draw(canvas: Canvas) {
        rippleDrawer.draw(canvas)
        super.draw(canvas)
    }

}

fun <G: ViewGroup> G.addClickableView(
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableView.() -> Unit)? = null
) =
        addChild(
                ClickableView(
                        context,
                        onClick,
                        rippleDrawInfo
                ),
                viewConfigurator
        )