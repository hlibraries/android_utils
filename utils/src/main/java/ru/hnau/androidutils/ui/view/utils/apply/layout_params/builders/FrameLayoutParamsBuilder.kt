package ru.hnau.androidutils.ui.view.utils.apply.layout_params.builders

import android.view.View
import android.widget.FrameLayout


class FrameLayoutParamsBuilder(
        view: View
) : MarginGravityLayoutParamsBuilder(
        view
) {

    override fun build() =
            FrameLayout.LayoutParams(width, height).apply {
                applyMargins()
                gravity = resolveGravity()
            }

}