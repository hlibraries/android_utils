package ru.hnau.androidutils.ui.view.decorated_container.decorations.tabs

import android.content.Context
import android.widget.LinearLayout
import ru.hnau.androidutils.animations.smooth
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.jutils.producer.ActualProducerSimple
import ru.hnau.jutils.producer.extensions.observeWhen


class TabsView<T : Any>(
        context: Context,
        private val items: List<T>,
        initialPosition: Int = 0,
        info: TabsViewInfo = TabsViewInfo()
) : LinearLayout(
        context
) {

    private val currentPosition =
            ActualProducerSimple(initialPosition)

    val currentItemProducer =
            currentPosition.map(items::get)

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val currentPositionSmoothed =
            currentPosition
                    .map(Int::toFloat)
                    .smooth(info.animatingTime)
                    .observeWhen(isVisibleToUserProducer)

    init {
        items.forEach {item ->

        }
    }


}