package ru.hnau.androidutils.ui.view.list.base

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView


enum class BaseListOrientation(
        val vertical: Boolean,
        val inverse: Boolean
) {

    HORIZONTAL(
            vertical = false,
            inverse = false
    ),

    VERTICAL(
            vertical = true,
            inverse = false
    ),

    HORIZONTAL_INVERSE(
            vertical = false,
            inverse = true
    ),

    VERTICAL_INVERSE(
            vertical = true,
            inverse = true
    );

    fun createLayoutManager(context: Context): LinearLayoutManager {
        val orientation = if (vertical) RecyclerView.VERTICAL else RecyclerView.HORIZONTAL
        return LinearLayoutManager(context, orientation, inverse)
    }

}