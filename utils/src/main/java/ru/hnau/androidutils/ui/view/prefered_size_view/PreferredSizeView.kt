package ru.hnau.androidutils.ui.view.prefered_size_view

import android.content.Context
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement


class PreferredSizeView(
        context: Context,
        private val measuredSizeResolver: PreferredSizeView.(widthMeasureSpec: Int, heightMeasureSpec: Int) -> Unit
) : View(
        context
), MeasuredDemensionSetter {


    companion object {

        fun fixed(
                context: Context,
                preferredWidth: DpPxGetter,
                preferredHeight: DpPxGetter
        ) = PreferredSizeView(context) { widthMeasureSpec: Int, heightMeasureSpec: Int ->
            val width = getDefaultMeasurement(widthMeasureSpec, preferredWidth.getPxInt(context))
            val height = getDefaultMeasurement(heightMeasureSpec, preferredHeight.getPxInt(context))
            setMeasuredSize(width, height)
        }

    }

    override fun setMeasuredSize(width: Int, height: Int) =
            setMeasuredDimension(width, height)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) =
            measuredSizeResolver.invoke(this, widthMeasureSpec, heightMeasureSpec)

}

fun <G : ViewGroup> G.addPreferredSizeView(
        measuredSizeResolver: PreferredSizeView.(widthMeasureSpec: Int, heightMeasureSpec: Int) -> Unit,
        viewConfigurator: (PreferredSizeView.() -> Unit)? = null
) =
        addChild(
                PreferredSizeView(context, measuredSizeResolver),
                viewConfigurator
        )

fun <G : ViewGroup> G.addFixedPreferredSizeView(
        preferredWidth: DpPxGetter,
        preferredHeight: DpPxGetter,
        viewConfigurator: (PreferredSizeView.() -> Unit)? = null
) =
        addChild(
                PreferredSizeView.fixed(context, preferredWidth, preferredHeight),
                viewConfigurator
        )