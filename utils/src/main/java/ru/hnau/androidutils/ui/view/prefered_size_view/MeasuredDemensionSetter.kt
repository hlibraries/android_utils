package ru.hnau.androidutils.ui.view.prefered_size_view


interface MeasuredDemensionSetter {

    fun setMeasuredSize(
            width: Int,
            height: Int
    )

}