package ru.hnau.androidutils.ui.view.view_presenter

import android.view.View
import android.view.animation.Interpolator
import ru.hnau.androidutils.animations.AnimationsUtils
import ru.hnau.androidutils.animations.DECELERATE_INTERPOLATOR
import ru.hnau.androidutils.animations.AnimationsUtils.DEFAULT_ANIMATION_TIME
import ru.hnau.androidutils.animations.inverted
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.Producer


data class PresentingViewInfo(
        val view: View?,
        val properties: PresentingViewProperties = PresentingViewProperties()
) {

    companion object {

        val EMPTY = PresentingViewInfo(null)

    }

}

fun View?.toPresentingInfo(
        properties: PresentingViewProperties = PresentingViewProperties()
) = PresentingViewInfo(
        view = this,
        properties = properties
)

fun Producer<View?>.mapToPresentingViewInfo(
        properties: PresentingViewProperties = PresentingViewProperties()
) = map { view ->
    view.toPresentingInfo(properties)
}