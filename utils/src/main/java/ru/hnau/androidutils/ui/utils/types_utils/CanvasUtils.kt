package ru.hnau.androidutils.ui.utils.types_utils

import android.graphics.*
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


fun Canvas.doInState(action: Canvas.() -> Unit) {
    save()
    action.invoke(this)
    restore()
}

fun Canvas.doInState(
        transformation: Canvas.() -> Unit,
        action: Canvas.() -> Unit
) =
        doInState {
            transformation.invoke(this)
            action.invoke(this)
        }

fun Canvas.drawInRect(
        rect: RectF,
        size: PointF = PointF(),
        action: Canvas.(size: PointF) -> Unit
) =
        doInState {
            clipRect(rect)
            translate(-rect.left, -rect.top)
            rect.getSize(size)
            action.invoke(this, size)
        }

fun Canvas.drawInRect(
        rect: Rect,
        size: Point = Point(),
        action: Canvas.(size: Point) -> Unit
) =
        doInState {
            clipRect(rect)
            translate(-rect.left.toFloat(), -rect.top.toFloat())
            rect.getSize(size)
            action.invoke(this, size)
        }

fun Canvas.drawInRect(
        rect: RectF,
        size: PointF,
        gravity: HGravity,
        drawRect: RectF = RectF(),
        rtl: Boolean,
        action: Canvas.(drawRect: RectF) -> Unit
) =
        doInState {
            gravity.apply(size, rect, rtl, drawRect)
            clipRect(drawRect)
            action.invoke(this, drawRect)
        }

fun Canvas.drawInRect(
        rect: Rect,
        size: Point,
        gravity: HGravity,
        drawRect: Rect = Rect(),
        rtl: Boolean,
        action: Canvas.(drawRect: Rect) -> Unit
) =
        doInState {
            gravity.apply(size, rect, rtl, drawRect)
            clipRect(drawRect)
            action.invoke(this, drawRect)
        }

fun Canvas.drawText(
        text: String,
        rect: RectF,
        gravity: HGravity,
        paint: Paint,
        textSizeMemSaveValue: PointF = PointF(),
        drawRectMemSaveValue: RectF = RectF(),
        rtl: Boolean
) {
    textSizeMemSaveValue.set(
            paint.measureText(text),
            paint.fontMetrics.descent - paint.fontMetrics.ascent
    )
    drawInRect(rect, textSizeMemSaveValue, gravity, drawRectMemSaveValue, rtl) { rectToDraw ->
        drawText(text, rectToDraw.left, rectToDraw.bottom, paint)
    }
}

fun Canvas.drawText(
        text: String,
        rect: Rect,
        gravity: HGravity,
        paint: Paint,
        textSizeMemSaveValue: Point = Point(),
        drawRectMemSaveValue: Rect = Rect(),
        rtl: Boolean
) {
    textSizeMemSaveValue.set(
            paint.measureText(text).toInt(),
            paint.fontMetricsInt.descent - paint.fontMetricsInt.ascent
    )
    drawInRect(rect, textSizeMemSaveValue, gravity, drawRectMemSaveValue, rtl) { rectToDraw ->
        drawText(text, rectToDraw.left.toFloat(), rectToDraw.bottom.toFloat(), paint)
    }
}