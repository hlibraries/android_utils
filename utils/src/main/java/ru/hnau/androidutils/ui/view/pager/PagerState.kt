package ru.hnau.androidutils.ui.view.pager

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import java.util.*


class PagerState(
        val superState: Parcelable,
        val stack: Stack<PageWrapper>
) : Parcelable {

    constructor(
            parcel: Parcel
    ) : this(
            superState = parcel.readParcelable(PagerState::class.java.classLoader),
            stack = Stack<PageWrapper>().apply {
                val count = parcel.readInt()
                repeat(count) { pageNum ->
                    val pageBundle = parcel.readBundle(javaClass.classLoader)
                    val pageWrapper = PageWrapper.createFromBundle(pageBundle, pageNum.toString())
                    push(pageWrapper)
                }
            }
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(superState, flags)
        parcel.writeInt(stack.size)
        stack.forEachIndexed { i, pageWrapper ->
            val pageBundle = Bundle()
            pageWrapper.saveToBundle(pageBundle, i.toString())
            parcel.writeBundle(pageBundle)
        }
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<PagerState> {
        override fun createFromParcel(parcel: Parcel) = PagerState(parcel)
        override fun newArray(size: Int): Array<PagerState?> = arrayOfNulls(size)
    }

}