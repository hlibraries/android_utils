package ru.hnau.androidutils.ui.drawables

import android.graphics.Canvas
import android.graphics.drawable.Drawable
import ru.hnau.androidutils.ui.utils.types_utils.doInState


abstract class HDrawable : Drawable() {

    private var offsetX = 0f
    private var offsetY = 0f
    private var boundsWidth = 0f
    private var boundsHeight = 0f

    val width: Float
        get() = bounds.width().toFloat()

    val height: Float
        get() = bounds.height().toFloat()

    override fun draw(canvas: Canvas) = canvas.doInState {
        translate(offsetX, offsetY)
        draw(this, boundsWidth, boundsHeight)
    }

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
        super.setBounds(left, top, right, bottom)
        offsetX = left.toFloat()
        offsetY = top.toFloat()
        boundsWidth = (right - left).toFloat()
        boundsHeight = (bottom - top).toFloat()
    }

    protected abstract fun draw(canvas: Canvas, width: Float, height: Float)

    final override fun getOpacity() = getDrawableOpacity().value

    protected abstract fun getDrawableOpacity(): DrawableOpacity

}