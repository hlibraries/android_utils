package ru.hnau.androidutils.ui.drawables.layout_drawable

import android.graphics.Point
import android.graphics.Rect
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.jutils.takeIfPositive


abstract class LayoutType {

    object Stretched : LayoutType() {

        override fun calculatePosition(
                scene: Rect, size: Point,
                gravity: HGravity, rtl: Boolean,
                result: Rect
        ) =
                result.set(scene)

    }

    object Independent : LayoutType() {

        override fun calculatePosition(
                scene: Rect, size: Point,
                gravity: HGravity, rtl: Boolean,
                result: Rect
        ) =
                gravity.apply(size, scene, rtl, result)

    }

    abstract class ScaleLayoutType : LayoutType() {

        private val memSavePoint = Point()

        override fun calculatePosition(
                scene: Rect, size: Point,
                gravity: HGravity, rtl: Boolean,
                result: Rect
        ) {

            val sceneWidth = scene.width().takeIfPositive()
            val sceneHeight = scene.height().takeIfPositive()
            val width = size.x.takeIfPositive()
            val height = size.y.takeIfPositive()
            if (sceneWidth == null || sceneHeight == null || width == null || height == null) {
                result.set(scene.left, scene.top, scene.left, scene.top)
                return
            }

            val sceneAspectRatio = sceneWidth.toFloat() / sceneHeight.toFloat()
            val aspectRatio = width.toFloat() / height.toFloat()

            calculateSize(sceneWidth, sceneHeight, sceneAspectRatio, aspectRatio, memSavePoint)
            gravity.apply(memSavePoint, scene, rtl, result)
        }

        protected abstract fun calculateSize(
                sceneWidth: Int, sceneHeight: Int,
                sceneAspectRatio: Float, aspectRatio: Float,
                result: Point
        )

    }

    object Inside : ScaleLayoutType() {

        override fun calculateSize(
                sceneWidth: Int, sceneHeight: Int,
                sceneAspectRatio: Float, aspectRatio: Float,
                result: Point
        ) {

            if (aspectRatio > sceneAspectRatio) {
                result.set(sceneWidth, (sceneWidth / aspectRatio).toInt())
            } else {
                result.set((sceneHeight * aspectRatio).toInt(), sceneHeight)
            }

        }

    }

    object Outside : ScaleLayoutType() {

        override fun calculateSize(
                sceneWidth: Int, sceneHeight: Int,
                sceneAspectRatio: Float, aspectRatio: Float,
                result: Point
        ) {

            if (aspectRatio < sceneAspectRatio) {
                result.set(sceneWidth, (sceneWidth / aspectRatio).toInt())
            } else {
                result.set((sceneHeight * aspectRatio).toInt(), sceneHeight)
            }

        }

    }

    abstract class LayoutTypeChooser : LayoutType() {

        override fun calculatePosition(
                scene: Rect, size: Point,
                gravity: HGravity, rtl: Boolean,
                result: Rect
        ) =
                chooseType(scene, size, gravity)
                        .calculatePosition(scene, size, gravity, rtl, result)

        protected abstract fun chooseType(scene: Rect, size: Point, gravity: HGravity): LayoutType

    }

    object SmallerOrInside : LayoutTypeChooser() {

        override fun chooseType(scene: Rect, size: Point, gravity: HGravity) =
                if (size.x < scene.width() && size.y < scene.height()) Independent else Inside

    }

    object SmallerOrOutside : LayoutTypeChooser() {

        override fun chooseType(scene: Rect, size: Point, gravity: HGravity) =
                if (size.x < scene.width() || size.y < scene.height()) Independent else Outside

    }

    object LargerOrInside : LayoutTypeChooser() {

        override fun chooseType(scene: Rect, size: Point, gravity: HGravity) =
                if (size.x > scene.width() || size.y > scene.height()) Independent else Inside

    }

    object LargerOrOutside : LayoutTypeChooser() {

        override fun chooseType(scene: Rect, size: Point, gravity: HGravity) =
                if (size.x > scene.width() && size.y > scene.height()) Independent else Outside

    }

    companion object {

        val DEFAULT: LayoutType
            get() = Independent

    }

    abstract fun calculatePosition(
            scene: Rect,
            size: Point,
            gravity: HGravity,
            rtl: Boolean,
            result: Rect
    )

}