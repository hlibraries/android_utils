package ru.hnau.androidutils.ui.view.layer.manager

import android.content.Context
import ru.hnau.androidutils.go_back_handler.GoBackHandler
import ru.hnau.androidutils.ui.view.layer.layer.Layer
import ru.hnau.androidutils.ui.view.layer.transaction.TransactionInfo


interface LayerManagerConnector : GoBackHandler {

    val viewContext: Context

    val isEmpty: Boolean

    val canGoBack: Boolean

    val defaultTransactionInfo: TransactionInfo

    fun showLayer(
            layer: Layer,
            clearStack: Boolean = false,
            transactionInfo: TransactionInfo = defaultTransactionInfo
    )

    fun goBack(): Boolean

}