package ru.hnau.androidutils.ui.view.view_presenter

import android.graphics.Point
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.ui.view.utils.isLTR
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.handle


class PresentingViewWrapper(
        parent: ViewGroup,
        val info: PresentingViewInfo
) {

    private val fromSide = info.properties.fromSide.getDirection(!isLTR)
    private val toSide = info.properties.toSide.getDirection(!isLTR)

    val measuredSize = Point()

    val animatingTime: TimeValue
        get() = info.properties.animatingTime

    private val layoutBounds = Rect()

    val view: View?
        get() = info.view

    init {
        view?.let(parent::addView)
    }

    fun removeFromParent(parent: ViewGroup) {
        view?.let(parent::removeView)
    }

    fun measure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        view?.measure(widthMeasureSpec, heightMeasureSpec)
        measuredSize.set(
                view?.measuredWidth ?: 0,
                view?.measuredHeight ?: 0
        )
    }

    fun layout(presenterBounds: Rect, isShowing: Boolean, animationPercentage: Float) {

        val view = view ?: return

        val interpolator = if (isShowing) info.properties.showInterpolator else info.properties.hideInterpolator
        val animationPercentageInterpolated = interpolator.getInterpolation(animationPercentage)

        val side = if (isShowing) fromSide else toSide
        val offsetPercentageRaw = if (isShowing) 1 - animationPercentageInterpolated else animationPercentageInterpolated
        val offsetPercentage = offsetPercentageRaw * info.properties.scrollFactor
        val offsetX = (side.x * offsetPercentage * presenterBounds.width()).toInt()
        val offsetY = (side.y * offsetPercentage * presenterBounds.height()).toInt()

        info.properties.gravity.apply(measuredSize, presenterBounds, !isLTR, layoutBounds)
        view.layout(
                layoutBounds.left + offsetX,
                layoutBounds.top + offsetY,
                layoutBounds.right + offsetX,
                layoutBounds.bottom + offsetY
        )
        view.alpha = (info.properties.scrollFactor >= 1).handle(
                forTrue = 1f,
                forFalse = 1 - offsetPercentageRaw
        )
    }

}

val PresentingViewWrapper?.measuredWidth: Int
    get() = this?.measuredSize?.x ?: 0

val PresentingViewWrapper?.measuredHeight: Int
    get() = this?.measuredSize?.y ?: 0