package ru.hnau.androidutils.ui.view.utils

import android.support.v4.text.TextUtilsCompat
import android.support.v4.view.ViewCompat
import java.util.*


val isLTR: Boolean
    get() {
        val layoutDirection = TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault())
        return layoutDirection == ViewCompat.LAYOUT_DIRECTION_LTR
    }

fun <R> handleLayoutDirection(ifLTR: () -> R, ifRTL: () -> R) =
        if (isLTR) ifLTR.invoke() else ifRTL.invoke()

fun <R> handleLayoutDirection(forLTR: R, forRTL: R) =
        if (isLTR) forLTR else forRTL