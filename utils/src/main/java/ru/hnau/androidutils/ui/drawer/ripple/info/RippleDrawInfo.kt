package ru.hnau.androidutils.ui.drawer.ripple.info

import ru.hnau.androidutils.context_getters.ColorGetter


data class RippleDrawInfo(
        val rippleInfo: RippleInfo = RippleInfo(),
        val color: ColorGetter = DEFAULT_COLOR,
        val backgroundColor: ColorGetter = DEFAULT_BACKGROUND_COLOR,
        val rippleAlpha: Float = DEFAULT_RIPPLE_ALPHA
) {

    companion object {

        val DEFAULT_COLOR = ColorGetter.BLACK
        val DEFAULT_BACKGROUND_COLOR = ColorGetter.WHITE
        const val DEFAULT_RIPPLE_ALPHA = 0.25f

    }

}