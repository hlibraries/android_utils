package ru.hnau.androidutils.ui.utils.h_gravity

import android.graphics.Point
import android.graphics.PointF
import android.graphics.Rect
import android.graphics.RectF
import android.os.Build
import android.view.Gravity
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.utils.handleLayoutDirection


data class HGravity(
        val horizontalValue: HGravityOrientationValue? = null,
        val verticalValue: HGravityOrientationValue? = null
) {

    companion object {

        private val START = HGravity(horizontalValue = HGravityOrientationValue.START)
        private val CENTER_HORIZONTAL = HGravity(horizontalValue = HGravityOrientationValue.CENTER)
        private val END = HGravity(horizontalValue = HGravityOrientationValue.END)

        private val TOP = HGravity(verticalValue = HGravityOrientationValue.START)
        private val CENTER_VERTICAL = HGravity(verticalValue = HGravityOrientationValue.CENTER)
        private val BOTTOM = HGravity(verticalValue = HGravityOrientationValue.END)

        val CENTER = CENTER_HORIZONTAL + CENTER_VERTICAL

        val START_CENTER_VERTICAL = START + CENTER_VERTICAL
        val END_CENTER_VERTICAL = END + CENTER_VERTICAL

        val TOP_CENTER_HORIZONTAL = TOP + CENTER_HORIZONTAL
        val BOTTOM_CENTER_HORIZONTAL = BOTTOM + CENTER_HORIZONTAL

        val START_TOP = START + TOP
        val START_BOTTOM = START + BOTTOM
        val END_TOP = END + TOP
        val END_BOTTOM = END + BOTTOM

        fun calcChildBegin(
                orientationValue: HGravityOrientationValue?,
                childSize: Int,
                parentBegin: Int,
                parentEnd: Int,
                invert: Boolean
        ): Int {
            val valueRaw = orientationValue ?: HGravityOrientationValue.CENTER
            val value = if (invert) valueRaw.opposite else valueRaw
            val parentSize = parentEnd - parentBegin
            return parentBegin + when (value) {
                HGravityOrientationValue.START -> 0
                HGravityOrientationValue.CENTER -> (parentSize - childSize) / 2
                HGravityOrientationValue.END -> parentSize - childSize
            }
        }

        fun calcChildBegin(
                orientationValue: HGravityOrientationValue?,
                childSize: Float,
                parentBegin: Float,
                parentEnd: Float,
                invert: Boolean
        ): Float {
            val valueRaw = orientationValue ?: HGravityOrientationValue.CENTER
            val value = if (invert) valueRaw.opposite else valueRaw
            val parentSize = parentEnd - parentBegin
            return parentBegin + when (value) {
                HGravityOrientationValue.START -> 0f
                HGravityOrientationValue.CENTER -> (parentSize - childSize) / 2
                HGravityOrientationValue.END -> parentSize - childSize
            }
        }

    }

    fun resolveAndroidGravity() = getHorizontalGravity() or
            when (verticalValue) {
                HGravityOrientationValue.START -> Gravity.TOP
                HGravityOrientationValue.END -> Gravity.BOTTOM
                HGravityOrientationValue.CENTER,
                null -> Gravity.CENTER_VERTICAL
            }

    private fun getHorizontalGravity(): Int {
        val horizontalValue = this.horizontalValue
        if (horizontalValue == null || horizontalValue == HGravityOrientationValue.CENTER) {
            return Gravity.CENTER_HORIZONTAL
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return if (horizontalValue == HGravityOrientationValue.START) {
                Gravity.START
            } else {
                Gravity.END
            }
        }
        return if (horizontalValue == HGravityOrientationValue.START) {
            handleLayoutDirection(Gravity.LEFT, Gravity.RIGHT)
        } else {
            handleLayoutDirection(Gravity.RIGHT, Gravity.LEFT)
        }
    }

    operator fun plus(other: HGravity) = HGravity(
            horizontalValue = other.horizontalValue ?: this.horizontalValue,
            verticalValue = other.verticalValue ?: this.verticalValue
    )

    fun apply(size: Point, parentRect: Rect, rtl: Boolean, childRect: Rect) {
        val left = calcChildBegin(horizontalValue, size.x, parentRect.left, parentRect.right, rtl)
        val top = calcChildBegin(verticalValue, size.y, parentRect.top, parentRect.bottom, false)
        childRect.set(left, top, left + size.x, top + size.y)
    }

    fun apply(size: Point, parentRect: Rect, rtl: Boolean) =
            Rect().apply { apply(size, parentRect, rtl, this) }


    fun apply(size: PointF, parentRect: RectF, rtl: Boolean, childRect: RectF) {
        val left = calcChildBegin(horizontalValue, size.x, parentRect.left, parentRect.right, rtl)
        val top = calcChildBegin(verticalValue, size.y, parentRect.top, parentRect.bottom, false)
        childRect.set(left, top, left + size.x, top + size.y)
    }

    fun apply(size: PointF, parentRect: RectF, rtl: Boolean) =
            RectF().apply { apply(size, parentRect, rtl, this) }


}

fun Side.toHGravity() = when (this) {
    Side.START -> HGravity.START_CENTER_VERTICAL
    Side.TOP -> HGravity.TOP_CENTER_HORIZONTAL
    Side.END -> HGravity.END_CENTER_VERTICAL
    Side.BOTTOM -> HGravity.BOTTOM_CENTER_HORIZONTAL
}