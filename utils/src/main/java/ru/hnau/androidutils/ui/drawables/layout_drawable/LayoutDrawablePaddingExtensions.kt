package ru.hnau.androidutils.ui.drawables.layout_drawable

import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


fun LayoutDrawable.setPadding(horizontal: Int, vertical: Int) =
        setPadding(horizontal, vertical, horizontal, vertical)

fun LayoutDrawable.setPadding(padding: Int) =
        setPadding(padding, padding)

fun LayoutDrawable.setPadding(left: Float, top: Float, right: Float, bottom: Float) =
        setPadding(left.toInt(), top.toInt(), right.toInt(), bottom.toInt())

fun LayoutDrawable.setPadding(horizontal: Float, vertical: Float) =
        setPadding(horizontal, vertical, horizontal, vertical)

fun LayoutDrawable.setPadding(padding: Float) =
        setPadding(padding, padding)

fun LayoutDrawable.setPadding(left: DpPxGetter, top: DpPxGetter, right: DpPxGetter, bottom: DpPxGetter) =
        setPadding(left.getPx(context), top.getPx(context), right.getPx(context), bottom.getPx(context))

fun LayoutDrawable.setPadding(horizontal: DpPxGetter, vertical: DpPxGetter) =
        setPadding(horizontal, vertical, horizontal, vertical)

fun LayoutDrawable.setPadding(padding: DpPxGetter) =
        setPadding(padding, padding)

fun LayoutDrawable.setLeftPadding(leftPadding: Int) =
        setPadding(leftPadding, paddingTop, paddingRight, paddingBottom)

fun LayoutDrawable.setLeftPadding(leftPadding: Float) =
        setLeftPadding(leftPadding.toInt())

fun LayoutDrawable.setLeftPadding(leftPadding: DpPxGetter) =
        setLeftPadding(leftPadding.getPx(context))

fun LayoutDrawable.setTopPadding(topPadding: Int) =
        setPadding(paddingLeft, topPadding, paddingRight, paddingBottom)

fun LayoutDrawable.setTopPadding(topPadding: Float) =
        setTopPadding(topPadding.toInt())

fun LayoutDrawable.setTopPadding(topPadding: DpPxGetter) =
        setTopPadding(topPadding.getPx(context))

fun LayoutDrawable.setRightPadding(rightPadding: Int) =
        setPadding(paddingLeft, paddingTop, rightPadding, paddingBottom)

fun LayoutDrawable.setRightPadding(rightPadding: Float) =
        setRightPadding(rightPadding.toInt())

fun LayoutDrawable.setRightPadding(rightPadding: DpPxGetter) =
        setRightPadding(rightPadding.getPx(context))

fun LayoutDrawable.setBottomPadding(bottomPadding: Int) =
        setPadding(paddingLeft, paddingTop, paddingRight, bottomPadding)

fun LayoutDrawable.setBottomPadding(bottomPadding: Float) =
        setBottomPadding(bottomPadding.toInt())

fun LayoutDrawable.setBottomPadding(bottomPadding: DpPxGetter) =
        setBottomPadding(bottomPadding.getPx(context))

fun LayoutDrawable.setHorizontalPadding(horizontalPadding: Int) =
        setPadding(horizontalPadding, paddingTop, horizontalPadding, paddingBottom)

fun LayoutDrawable.setHorizontalPadding(horizontalPadding: Float) =
        setHorizontalPadding(horizontalPadding.toInt())

fun LayoutDrawable.setHorizontalPadding(horizontalPadding: DpPxGetter) =
        setHorizontalPadding(horizontalPadding.getPx(context))

fun LayoutDrawable.setVerticalPadding(verticalPadding: Int) =
        setPadding(paddingLeft, verticalPadding, paddingRight, verticalPadding)

fun LayoutDrawable.setVerticalPadding(verticalPadding: Float) =
        setVerticalPadding(verticalPadding.toInt())

fun LayoutDrawable.setVerticalPadding(verticalPadding: DpPxGetter) =
        setVerticalPadding(verticalPadding.getPx(context))

val LayoutDrawable.verticalPaddingSum: Int
    get() = paddingTop + paddingBottom

val LayoutDrawable.horizontalPaddingSum: Int
    get() = paddingLeft + paddingRight