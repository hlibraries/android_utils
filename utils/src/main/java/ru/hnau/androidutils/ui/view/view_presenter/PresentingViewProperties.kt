package ru.hnau.androidutils.ui.view.view_presenter

import android.view.animation.Interpolator
import ru.hnau.androidutils.animations.AnimationsUtils
import ru.hnau.androidutils.animations.DECELERATE_INTERPOLATOR
import ru.hnau.androidutils.animations.inverted
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.jutils.TimeValue


data class PresentingViewProperties(
        val showInterpolator: Interpolator = DECELERATE_INTERPOLATOR,
        val hideInterpolator: Interpolator = showInterpolator.inverted,
        val animatingTime: TimeValue = AnimationsUtils.DEFAULT_ANIMATION_TIME,
        val gravity: HGravity = HGravity.CENTER,
        val fromSide: Side = Side.END,
        val toSide: Side = fromSide.getOpposite(),
        val scrollFactor: Float = 1f
)