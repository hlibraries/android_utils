package ru.hnau.androidutils.ui.view.header.button

import android.content.Context
import android.graphics.Point
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.view.clickable.ClickableView
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement
import ru.hnau.androidutils.ui.view.utils.horizontalPaddingSum
import ru.hnau.androidutils.ui.view.utils.verticalPaddingSum
import kotlin.math.min


abstract class HeaderButton(
        context: Context,
        onClick: () -> Unit,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : ClickableView(
        context,
        onClick,
        rippleDrawInfo
) {

    private val measuredSize = Point()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val maxWidth = getMaxMeasurement(widthMeasureSpec, Int.MAX_VALUE) - horizontalPaddingSum
        val maxHeight = getMaxMeasurement(heightMeasureSpec, Int.MAX_VALUE) - verticalPaddingSum
        val preferredSize = min(maxWidth, maxHeight)
        setMeasuredDimension(
                getDefaultMeasurement(widthMeasureSpec, preferredSize + horizontalPaddingSum),
                getDefaultMeasurement(heightMeasureSpec, preferredSize + verticalPaddingSum)
        )
    }

}