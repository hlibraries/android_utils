package ru.hnau.androidutils.ui.canvas_shape

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import ru.hnau.androidutils.ui.bounds_producer.BoundsProducer


class OvalCanvasShape(
        boundsProducer: BoundsProducer
) : CanvasShape(
        boundsProducer
) {

    private var bounds = RectF()
    private val path = Path()

    override fun doClip(canvas: Canvas) {
        canvas.clipPath(path)
    }

    override fun doDraw(canvas: Canvas, paint: Paint) {
        canvas.drawOval(bounds, paint)
    }

    override fun invalidate(bounds: RectF) {
        this.bounds = bounds
        path.reset()
        path.addOval(bounds, Path.Direction.CW)
    }

}