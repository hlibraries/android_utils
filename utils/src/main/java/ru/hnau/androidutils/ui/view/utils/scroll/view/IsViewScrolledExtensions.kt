package ru.hnau.androidutils.ui.view.utils.scroll.view

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import ru.hnau.androidutils.ui.view.utils.isLTR
import java.lang.Exception

private val View.isScrolledToLeft: Boolean
    get() = canScrollHorizontally(-1)

val View.isScrolledToTop: Boolean
    get() = canScrollVertically(-1)

private val View.isScrolledToRight: Boolean
    get() = canScrollHorizontally(1)

val View.isScrolledToBottom: Boolean
    get() = canScrollVertically(1)

val View.isScrolledToStart: Boolean
    get() = if (!isLTR) isScrolledToRight else isScrolledToLeft

val View.isScrolledToEnd: Boolean
    get() = if (!isLTR) isScrolledToLeft else isScrolledToRight