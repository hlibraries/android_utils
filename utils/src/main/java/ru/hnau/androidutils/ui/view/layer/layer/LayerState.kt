package ru.hnau.androidutils.ui.view.layer.layer


@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class LayerState