package ru.hnau.androidutils.ui.view.linear_layout

import android.content.Context
import android.widget.LinearLayout
import ru.hnau.androidutils.ui.view.utils.apply.applyHorizontalOrientation
import ru.hnau.androidutils.ui.view.utils.apply.applyVerticalOrientation


open class HorizontalLinearLayout(
        context: Context,
        configurator: (HorizontalLinearLayout.() -> Unit)? = null
) : LinearLayout(
        context
) {

    init {
        applyHorizontalOrientation()
        configurator?.let(this::apply)
    }

}