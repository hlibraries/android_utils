package ru.hnau.androidutils.ui.view.buttons.main_action

import ru.hnau.androidutils.animations.AnimationsUtils
import ru.hnau.androidutils.animations.AnimationsUtils.DEFAULT_ANIMATION_TIME
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.context_getters.dp_px.dp56
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.jutils.TimeValue


data class MainActionButtonInfo(
        val minSize: DpPxGetter = dp56,
        val rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        val shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT,
        val animatingTime: TimeValue = AnimationsUtils.DEFAULT_ANIMATION_TIME,
        val titleLabelInfo: LabelInfo = LabelInfo()
) {

    companion object {

        val DEFAULT = MainActionButtonInfo()

    }

}