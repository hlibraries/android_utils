package ru.hnau.androidutils.ui.view.list.base

import android.graphics.Canvas
import android.view.View


interface BaseListViewWrapper<T> {

    companion object {

        private val DEFAULT_SUPPORTED_SWIPE_AND_DRAG_DIRECTIONS =
                emptySet<SwipeOrDragDirection>()

    }

    val view: View

    fun setContent(content: T, position: Int)

    fun setViewHolder(viewHolder: BaseListViewHolder<T>) {}

    fun getSupportedSwipeDirections() = DEFAULT_SUPPORTED_SWIPE_AND_DRAG_DIRECTIONS
    fun getSupportedDragDirections() = DEFAULT_SUPPORTED_SWIPE_AND_DRAG_DIRECTIONS

    fun handleDrawContentViewForSwiping(c: Canvas, dX: Float, dY: Float) = false
    fun handleDrawContentViewForDragging(c: Canvas, dX: Float, dY: Float) = false

    fun onSelectedForSwiping() {}
    fun onSelectedForDragging() {}
    fun onUnselected() {}

}