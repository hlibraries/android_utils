package ru.hnau.androidutils.ui.view.utils.apply

import android.view.View
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.view.utils.isLTR


fun <V: View> V.applyPadding(start: Int, top: Int, end: Int, bottom: Int) = apply {
    val left = if (!isLTR) end else start
    val right = if (!isLTR) start else end
    setPadding(left, top, right, bottom)
}

fun <V: View> V.applyPadding(start: DpPxGetter, top: DpPxGetter, end: DpPxGetter, bottom: DpPxGetter) =
        applyPadding(start.getPxInt(context), top.getPxInt(context), end.getPxInt(context), bottom.getPxInt(context))

fun <V: View> V.applyPadding(start: Float, top: Float, end: Float, bottom: Float) =
        applyPadding(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())

fun <V: View> V.applyPadding(start: Double, top: Double, end: Double, bottom: Double) =
        applyPadding(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())

fun <V: View> V.applyPadding(start: Short, top: Short, end: Short, bottom: Short) =
        applyPadding(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())

fun <V: View> V.applyPadding(start: Byte, top: Byte, end: Byte, bottom: Byte) =
        applyPadding(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())

fun <V: View> V.applyPadding(start: Long, top: Long, end: Long, bottom: Long) =
        applyPadding(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())


fun <V: View> V.applyPadding(horizontal: Int, vertical: Int) =
        applyPadding(horizontal, vertical, horizontal, vertical)

fun <V: View> V.applyPadding(horizontal: DpPxGetter, vertical: DpPxGetter) =
        applyPadding(horizontal.getPxInt(context), vertical.getPxInt(context))

fun <V: View> V.applyPadding(horizontal: Float, vertical: Float) =
        applyPadding(horizontal.toInt(), vertical.toInt())

fun <V: View> V.applyPadding(horizontal: Double, vertical: Double) =
        applyPadding(horizontal.toInt(), vertical.toInt())

fun <V: View> V.applyPadding(horizontal: Short, vertical: Short) =
        applyPadding(horizontal.toInt(), vertical.toInt())

fun <V: View> V.applyPadding(horizontal: Byte, vertical: Byte) =
        applyPadding(horizontal.toInt(), vertical.toInt())

fun <V: View> V.applyPadding(horizontal: Long, vertical: Long) =
        applyPadding(horizontal.toInt(), vertical.toInt())


fun <V: View> V.applyPadding(all: Int) =
        applyPadding(all, all)

fun <V: View> V.applyPadding(all: DpPxGetter) =
        applyPadding(all.getPxInt(context))

fun <V: View> V.applyPadding(all: Float) =
        applyPadding(all.toInt())

fun <V: View> V.applyPadding(all: Double) =
        applyPadding(all.toInt())

fun <V: View> V.applyPadding(all: Short) =
        applyPadding(all.toInt())

fun <V: View> V.applyPadding(all: Byte) =
        applyPadding(all.toInt())

fun <V: View> V.applyPadding(all: Long) =
        applyPadding(all.toInt())


fun <V: View> V.applyStartPadding(start: Int) =
        applyPadding(start, paddingTop, paddingEnd, paddingBottom)

fun <V: View> V.applyStartPadding(start: DpPxGetter) =
        applyStartPadding(start.getPxInt(context))

fun <V: View> V.applyStartPadding(start: Float) =
        applyStartPadding(start.toInt())

fun <V: View> V.applyStartPadding(start: Double) =
        applyStartPadding(start.toInt())

fun <V: View> V.applyStartPadding(start: Short) =
        applyStartPadding(start.toInt())

fun <V: View> V.applyStartPadding(start: Byte) =
        applyStartPadding(start.toInt())

fun <V: View> V.applyStartPadding(start: Long) =
        applyStartPadding(start.toInt())


fun <V: View> V.applyTopPadding(top: Int) =
        applyPadding(paddingStart, top, paddingEnd, paddingBottom)

fun <V: View> V.applyTopPadding(top: DpPxGetter) =
        applyTopPadding(top.getPxInt(context))

fun <V: View> V.applyTopPadding(top: Float) =
        applyTopPadding(top.toInt())

fun <V: View> V.applyTopPadding(top: Double) =
        applyTopPadding(top.toInt())

fun <V: View> V.applyTopPadding(top: Short) =
        applyTopPadding(top.toInt())

fun <V: View> V.applyTopPadding(top: Byte) =
        applyTopPadding(top.toInt())

fun <V: View> V.applyTopPadding(top: Long) =
        applyTopPadding(top.toInt())


fun <V: View> V.applyEndPadding(end: Int) =
        applyPadding(paddingStart, paddingTop, end, paddingBottom)

fun <V: View> V.applyEndPadding(end: DpPxGetter) =
        applyEndPadding(end.getPxInt(context))

fun <V: View> V.applyEndPadding(end: Float) =
        applyEndPadding(end.toInt())

fun <V: View> V.applyEndPadding(end: Double) =
        applyEndPadding(end.toInt())

fun <V: View> V.applyEndPadding(end: Short) =
        applyEndPadding(end.toInt())

fun <V: View> V.applyEndPadding(end: Byte) =
        applyEndPadding(end.toInt())

fun <V: View> V.applyEndPadding(end: Long) =
        applyEndPadding(end.toInt())


fun <V: View> V.applyBottomPadding(bottom: Int) =
        applyPadding(paddingStart, paddingTop, paddingEnd, bottom)

fun <V: View> V.applyBottomPadding(bottom: DpPxGetter) =
        applyBottomPadding(bottom.getPxInt(context))

fun <V: View> V.applyBottomPadding(bottom: Float) =
        applyBottomPadding(bottom.toInt())

fun <V: View> V.applyBottomPadding(bottom: Double) =
        applyBottomPadding(bottom.toInt())

fun <V: View> V.applyBottomPadding(bottom: Short) =
        applyBottomPadding(bottom.toInt())

fun <V: View> V.applyBottomPadding(bottom: Byte) =
        applyBottomPadding(bottom.toInt())

fun <V: View> V.applyBottomPadding(bottom: Long) =
        applyBottomPadding(bottom.toInt())


fun <V: View> V.applyHorizontalPadding(horizontal: Int) =
        applyPadding(horizontal, paddingTop, horizontal, paddingBottom)

fun <V: View> V.applyHorizontalPadding(horizontal: DpPxGetter) =
        applyHorizontalPadding(horizontal.getPxInt(context))

fun <V: View> V.applyHorizontalPadding(horizontal: Float) =
        applyHorizontalPadding(horizontal.toInt())

fun <V: View> V.applyHorizontalPadding(horizontal: Double) =
        applyHorizontalPadding(horizontal.toInt())

fun <V: View> V.applyHorizontalPadding(horizontal: Short) =
        applyHorizontalPadding(horizontal.toInt())

fun <V: View> V.applyHorizontalPadding(horizontal: Byte) =
        applyHorizontalPadding(horizontal.toInt())

fun <V: View> V.applyHorizontalPadding(horizontal: Long) =
        applyHorizontalPadding(horizontal.toInt())


fun <V: View> V.applyVerticalPadding(vertical: Int) =
        applyPadding(paddingStart, vertical, paddingEnd, vertical)

fun <V: View> V.applyVerticalPadding(vertical: DpPxGetter) =
        applyVerticalPadding(vertical.getPxInt(context))

fun <V: View> V.applyVerticalPadding(vertical: Float) =
        applyVerticalPadding(vertical.toInt())

fun <V: View> V.applyVerticalPadding(vertical: Double) =
        applyVerticalPadding(vertical.toInt())

fun <V: View> V.applyVerticalPadding(vertical: Short) =
        applyVerticalPadding(vertical.toInt())

fun <V: View> V.applyVerticalPadding(vertical: Byte) =
        applyVerticalPadding(vertical.toInt())

fun <V: View> V.applyVerticalPadding(vertical: Long) =
        applyVerticalPadding(vertical.toInt())