package ru.hnau.androidutils.ui.drawer

import ru.hnau.androidutils.ui.canvas_shape.CanvasShape
import ru.hnau.jutils.producer.Producer


abstract class CanvasShapeDrawer(
        protected val canvasShape: CanvasShape
) : Producer<Unit>() {

    init {
        canvasShape.attach {
            onCanvasShapeChanged(canvasShape)
            call(Unit)
        }
    }

    protected open fun onCanvasShapeChanged(canvasShape: CanvasShape) {}

}