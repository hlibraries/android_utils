package ru.hnau.androidutils.ui.view.label

import android.view.View
import ru.hnau.androidutils.ui.view.utils.getMeasuredSpecMode
import ru.hnau.androidutils.ui.view.utils.getMeasuredSpecSize
import kotlin.math.min


interface LabelDrawableHelper {

    fun calculateDefaultMeasurement(measureSpec: Int, size: Int): Int

    fun calculateMaxMeasurement(measureSpec: Int, sizeForUnspecified: Int): Int

    fun calculateMaxMeasurementOrNullIfUnspecified(measureSpec: Int): Int?

}