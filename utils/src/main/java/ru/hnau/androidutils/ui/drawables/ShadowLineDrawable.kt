package ru.hnau.androidutils.ui.drawables

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.Paint
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.drawables.DrawableOpacity
import ru.hnau.androidutils.ui.drawables.HDrawable
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.utils.isLTR
import ru.hnau.jutils.handle
import ru.hnau.jutils.takeIfPositive
import kotlin.math.sqrt


open class ShadowLineDrawable(
        context: Context,
        private val fromSide: Side,
        color: ColorGetter = ColorGetter.BLACK
) : HDrawable() {

    private var drawAlpha = 255

    private val paint = Paint().apply {
        this.color = color.get(context)
    }

    private val fromLeft = (!isLTR).handle(
            forTrue = fromSide == Side.END,
            forFalse = fromSide == Side.START
    )

    override fun draw(canvas: Canvas, width: Float, height: Float) {

        val rows = fromSide.horizontal.handle(forTrue = width, forFalse = height)
                .takeIfPositive() ?: return

        var x1 = 0f
        var y1 = 0f
        var x2 = 0f
        var y2 = 0f

        repeat(rows.toInt()) { row ->

            val rowPos = (fromLeft || fromSide == Side.TOP).handle(
                    onTrue = { rows - row - 1 },
                    onFalse = { row.toFloat() }
            )

            fromSide.horizontal.handle(
                    onTrue = { y1 = 0f; y2 = height; x1 = rowPos; x2 = x1 },
                    onFalse = { x1 = 0f; x2 = width; y1 = rowPos; y2 = y1 }
            )

            val visibilityPercentage = row.toFloat() / rows
            val alpha = (1.0 - sqrt(1.0 - visibilityPercentage * visibilityPercentage)).toFloat()
            paint.alpha = (alpha * drawAlpha).toInt()

            canvas.drawLine(x1, y1, x2, y2, paint)
        }
    }

    override fun getDrawableOpacity() =
            DrawableOpacity.TRANSLUCENT

    override fun setAlpha(alpha: Int) {
        drawAlpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.colorFilter = colorFilter
    }
}