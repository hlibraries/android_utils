package ru.hnau.androidutils.ui.view.utils

import android.view.Gravity
import android.widget.FrameLayout


@Deprecated("Use FrameLayout.LayoutParams.setGravity(gravity: HGravity)")
fun FrameLayout.LayoutParams.setFrameParamsCenterGravity() {
    gravity = Gravity.CENTER
}

@Deprecated("Use FrameLayout.LayoutParams.setGravity(gravity: HGravity)")
fun FrameLayout.LayoutParams.setFrameParamsStartTopGravity() {
    gravity = Gravity.START or Gravity.TOP
}

@Deprecated("Use FrameLayout.LayoutParams.setGravity(gravity: HGravity)")
fun FrameLayout.LayoutParams.setFrameParamsEndTopGravity() {
    gravity = Gravity.END or Gravity.TOP
}

@Deprecated("Use FrameLayout.LayoutParams.setGravity(gravity: HGravity)")
fun FrameLayout.LayoutParams.setFrameParamsEndBottomGravity() {
    gravity = Gravity.END or Gravity.BOTTOM
}

@Deprecated("Use FrameLayout.LayoutParams.setGravity(gravity: HGravity)")
fun FrameLayout.LayoutParams.setFrameParamsStartBottomGravity() {
    gravity = Gravity.START or Gravity.BOTTOM
}

@Deprecated("Use FrameLayout.LayoutParams.setGravity(gravity: HGravity)")
fun FrameLayout.LayoutParams.setFrameParamsTopCenterHorizontalGravity() {
    gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
}

@Deprecated("Use FrameLayout.LayoutParams.setGravity(gravity: HGravity)")
fun FrameLayout.LayoutParams.setFrameParamsBottomCenterHorizontalGravity() {
    gravity = Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM
}

@Deprecated("Use FrameLayout.LayoutParams.setGravity(gravity: HGravity)")
fun FrameLayout.LayoutParams.setFrameParamsStartCenterVerticalGravity() {
    gravity = Gravity.START or Gravity.CENTER_VERTICAL
}

@Deprecated("Use FrameLayout.LayoutParams.setGravity(gravity: HGravity)")
fun FrameLayout.LayoutParams.setFrameParamsEndCenterVerticalGravity() {
    gravity = Gravity.END or Gravity.CENTER_VERTICAL
}