package ru.hnau.androidutils.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp0


import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.apply.applyBackground
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams


@SuppressLint("ViewConstructor")
class LinearLayoutSeparator(
        context: Context,
        weight: Float = 1f,
        width: DpPxGetter = dp0,
        height: DpPxGetter = dp0,
        backgroundColor: ColorGetter? = null
) : View(
        context
) {

    init {

        applyLinearParams {
            setWidth(width)
            setHeight(height)
            setWeight(weight)
        }

        applyBackground(backgroundColor)

    }

}

fun <G : ViewGroup> G.addLinearSeparator(
        weight: Float = 1f,
        width: DpPxGetter = dp0,
        height: DpPxGetter = dp0,
        backgroundColor: ColorGetter? = null,
        viewConfigurator: (LinearLayoutSeparator.() -> Unit)? = null
) =
        addChild(
                LinearLayoutSeparator(
                        context,
                        weight,
                        width,
                        height,
                        backgroundColor
                ),
                viewConfigurator
        )