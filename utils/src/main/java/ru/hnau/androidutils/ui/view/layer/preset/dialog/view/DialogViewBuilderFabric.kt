package ru.hnau.androidutils.ui.view.layer.preset.dialog.view

import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector


abstract class DialogViewBuilderFabric<V : DialogView, I : Any>(
        private val dialogViewCreator: (
                layerManagerConnector: LayerManagerConnector,
                info: I
        ) -> V
) {

    fun create(
            info: I,
            dialogViewConfigurator: V.() -> Unit
    ) =
            DialogViewBuilder(dialogViewCreator, dialogViewConfigurator, info)

}