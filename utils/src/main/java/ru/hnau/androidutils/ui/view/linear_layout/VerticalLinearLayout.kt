package ru.hnau.androidutils.ui.view.linear_layout

import android.content.Context
import android.widget.LinearLayout
import ru.hnau.androidutils.ui.view.utils.apply.applyVerticalOrientation


open class VerticalLinearLayout(
        context: Context,
        configurator: (VerticalLinearLayout.() -> Unit)? = null
) : LinearLayout(
        context
) {

    init {
        applyVerticalOrientation()
        configurator?.let(this::apply)
    }

}