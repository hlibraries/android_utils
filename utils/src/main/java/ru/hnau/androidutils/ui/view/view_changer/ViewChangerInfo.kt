package ru.hnau.androidutils.ui.view.view_changer

import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.view.animation.Interpolator
import ru.hnau.androidutils.animations.ACCELERATE_INTERPOLATOR
import ru.hnau.androidutils.animations.AnimationsUtils
import ru.hnau.androidutils.animations.AnimationsUtils.DEFAULT_ANIMATION_TIME
import ru.hnau.androidutils.animations.DECELERATE_INTERPOLATOR
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.jutils.TimeValue


data class ViewChangerInfo(
        val viewDecorationDrawer: ViewDecorationDrawer? = VIEW_DECORATION_DRAWER_DEFAULT,
        val fromSide: Side = FROM_SIZE_DEFAULT,
        val gravity: HGravity = GRAVITY_DEFAULT,
        val animationTime: TimeValue = ANIMATION_TIME_DEFAULT,
        val scrollFactor: Float = SCROLL_FACTOR_DEFAULT,
        val showInterpolator: Interpolator = SHOW_INTERPOLATOR_DEFAULT,
        val hideInterpolator: Interpolator = HIDE_INTERPOLATOR_DEFAULT,
        val onTop: Boolean = ON_TOP_DEFAULT,
        val slapToZeroForNullView: Boolean = SLAP_TO_ZERO_SIZE_FOR_NULL_VIEW_DEFAULT
) {

    companion object {

        val VIEW_DECORATION_DRAWER_DEFAULT: ViewDecorationDrawer? = null
        val FROM_SIZE_DEFAULT = Side.END
        val GRAVITY_DEFAULT = HGravity.CENTER
        val ANIMATION_TIME_DEFAULT = AnimationsUtils.DEFAULT_ANIMATION_TIME
        val HIDE_INTERPOLATOR_DEFAULT = ACCELERATE_INTERPOLATOR
        val SHOW_INTERPOLATOR_DEFAULT = DECELERATE_INTERPOLATOR
        const val SCROLL_FACTOR_DEFAULT = 1f
        const val ON_TOP_DEFAULT = true
        const val SLAP_TO_ZERO_SIZE_FOR_NULL_VIEW_DEFAULT = true

    }

}