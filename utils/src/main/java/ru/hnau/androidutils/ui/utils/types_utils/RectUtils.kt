package ru.hnau.androidutils.ui.utils.types_utils

import android.graphics.Point
import android.graphics.PointF
import android.graphics.Rect
import android.graphics.RectF


fun Rect.toRectF() = RectF(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat())

fun Rect.offset(point: Point) = offset(point.x, point.y)

fun Rect.offset(dx: Float, dy: Float) = offset(dx.toInt(), dy.toInt())

fun Rect.offset(point: PointF) = offset(point.x, point.y)

fun Rect.getSize(point: Point) = point.set(width(), height())

fun Rect.getSize() = Point().apply { getSize(this) }

fun Rect.getPos(point: Point) = point.set(left, top)

fun Rect.getPos() = Point().apply { getPos(this) }

fun Rect.getCenter(point: Point) = point.set(centerX(), centerY())

fun Rect.getCenter() = Point().apply { getCenter(this) }