package ru.hnau.androidutils.ui.view.label

import android.content.Context
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.Point
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.drawables.DrawableOpacity
import ru.hnau.androidutils.ui.drawables.HDrawable
import ru.hnau.androidutils.ui.font_type.FontTypeGetter
import ru.hnau.androidutils.ui.font_type.setFontType
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.utils.h_gravity.isCenter
import ru.hnau.androidutils.ui.view.utils.isLTR
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurementOrNullIfUnspecified
import kotlin.math.max


class LabelDrawable(
        private val context: Context,
        initialText: StringGetter = StringGetter(),
        info: LabelInfo = LabelInfo(),
        private val helper: LabelDrawableHelper
) : HDrawable() {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = info.textColor.get(context)
        textSize = info.textSize.getPxInt(context).toFloat()
        setFontType(info.fontType?.get(context))
    }

    private var measuredSize = Point()

    var text = initialText
        set(value) {
            if (field != value) {
                field = value
                onNeedUpdateMeasurements()
            }
        }

    var fontType = info.fontType
        set(value) {
            if (field != value) {
                field = value
                paint.setFontType(value?.get(context))
                onNeedUpdateMeasurements()
            }
        }

    var textColor = info.textColor
        set(value) {
            if (field != value) {
                field = value
                paint.color = value.get(context)
                invalidateSelf()
            }
        }

    var textSize = info.textSize
        set(value) {
            if (field != value) {
                field = value
                paint.textSize = value.getPxInt(context).toFloat()
                onNeedUpdateMeasurements()
            }
        }

    var gravity = info.gravity
        set(value) {
            if (field != value) {
                field = value
                onNeedUpdateMeasurements()
            }
        }

    var maxLines = info.maxLines
        set(value) {
            if (field != value) {
                field = value
                onNeedUpdateMeasurements()
            }
        }

    var minLines = info.minLines
        set(value) {
            if (field != value) {
                field = value
                onNeedUpdateMeasurements()
            }
        }

    var customLineHeight = info.customLineHeight
        set(value) {
            if (field != value) {
                field = value
                onNeedUpdateMeasurements()
            }
        }

    var ellipsize = info.ellipsize
        set(value) {
            if (field != value) {
                field = value
                onNeedUpdateMeasurements()
            }
        }

    var normalizeForSingleLine = info.normalizeForSingleLine
        set(value) {
            if (field != value) {
                field = value
                onNeedUpdateMeasurements()
            }
        }

    var underline = info.underline
        set(value) {
            if (field != value) {
                field = value
                updateUnderline(value)
                onNeedUpdateMeasurements()
            }
        }

    private val lineHeight: Float
        get() = customLineHeight?.getPx(context) ?: paint.textSize

    private val lines = LabelLines { !isLTR }

    init {
        updateUnderline(underline)
    }

    private fun updateUnderline(underline: Boolean) {
        paint.flags = if (underline) {
            paint.flags or Paint.UNDERLINE_TEXT_FLAG
        } else {
            paint.flags and Paint.UNDERLINE_TEXT_FLAG.inv()
        }
    }

    fun setLabelInfo(labelInfo: LabelInfo) {
        fontType = labelInfo.fontType
        textColor = labelInfo.textColor
        textSize = labelInfo.textSize
        gravity = labelInfo.gravity
        maxLines = labelInfo.maxLines
        minLines = labelInfo.minLines
        customLineHeight = labelInfo.customLineHeight
        ellipsize = labelInfo.ellipsize
        underline = labelInfo.underline
    }

    override fun draw(canvas: Canvas, width: Float, height: Float) {
        lines.lines.forEach {
            val x = lines.offset.x + it.left
            val y = lines.offset.y + it.bottom - paint.fontMetrics.descent
            canvas.drawText(it.text, x, y, paint)
        }
    }

    private fun onNeedUpdateMeasurements() {
        invalidateSelf()
    }

    override fun getDrawableOpacity() = DrawableOpacity.TRANSLUCENT

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.colorFilter = colorFilter
    }

    override fun getIntrinsicWidth() = measuredSize.x
    override fun getIntrinsicHeight() = measuredSize.y

    fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val maxWidth = helper.calculateMaxMeasurementOrNullIfUnspecified(widthMeasureSpec)?.let { (it).toFloat() }
        val maxHeight = helper.calculateMaxMeasurementOrNullIfUnspecified(heightMeasureSpec)?.let { (it).toFloat() }

        lines.update(
                text = text.get(context),
                stringWidthResolver = paint::measureText,
                maxWidth = maxWidth,
                maxLines = maxLines,
                ellipsize = ellipsize,
                gravity = gravity,
                lineHeight = lineHeight,
                maxHeight = maxHeight
        )

        val linesHeight = max(lines.size.y, lineHeight * (minLines ?: 0))

        val resultWidth = lines.size.x.toInt()
        val resultHeight = linesHeight.toInt()

        val measuredResultWidth = helper.calculateDefaultMeasurement(widthMeasureSpec, resultWidth)
        val measuredResultHeight = helper.calculateDefaultMeasurement(heightMeasureSpec, resultHeight)

        lines.offset.x = HGravity.calcChildBegin(gravity.horizontalValue, lines.size.x, 0f, measuredResultWidth.toFloat(), !isLTR)
        lines.offset.y = HGravity.calcChildBegin(gravity.verticalValue, lines.size.y + getLinesOffsetY(), 0f, measuredResultHeight.toFloat(), false)

        measuredSize.set(
                measuredResultWidth,
                measuredResultHeight
        )
    }

    private fun getLinesOffsetY(): Float {
        if (!normalizeForSingleLine) {
            return 0f
        }
        if (!gravity.verticalValue.isCenter()) {
            return 0f
        }
        if (lines.lines.size > 1) {
            return 0f
        }
        return -paint.fontMetrics.descent / 2
    }

}