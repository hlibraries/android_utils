package ru.hnau.androidutils.ui.view.view_presenter


data class PresenterViewInfo(
        val horizontalSizeInterpolator: SizeInterpolator = SizeInterpolator.DEFAULT,
        val verticalSizeInterpolator: SizeInterpolator = SizeInterpolator.DEFAULT
)