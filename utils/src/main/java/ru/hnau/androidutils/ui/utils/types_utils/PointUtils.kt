package ru.hnau.androidutils.ui.utils.types_utils

import android.graphics.Point
import android.graphics.PointF

fun Point.toPointF(pointF: PointF) = pointF.set(this)

fun Point.toPointF() = PointF().apply { toPointF(this) }

fun Point.scale(scale: Int) = set(x * scale, y * scale)

fun Point.scale(scale: Long) = set((x * scale).toInt(), (y * scale).toInt())

fun Point.scale(scale: Float) = set((x * scale).toInt(), (y * scale).toInt())

fun Point.scale(scale: Double) = set((x * scale).toInt(), (y * scale).toInt())

fun Point.set(point: Point) = set(point.x, point.y)

fun Point.set(point: PointF) = set(point.x.toInt(), point.y.toInt())

fun Point.offset(dx: Float, dy: Float) = offset(dx.toInt(), dy.toInt())

fun Point.offset(offset: Point) = offset(offset.x, offset.y)

fun Point.offset(offset: PointF) = offset(offset.x, offset.y)

fun Point.pointInterThisAndOther(other: Point, pos: Float, result: Point) = result.set(
        (this.x + (other.x - this.x) * pos).toInt(),
        (this.y + (other.y - this.y) * pos).toInt()
)

fun Point.pointInterThisAndOther(other: Point, pos: Float): Point {
    val result = Point()
    pointInterThisAndOther(other, pos, result)
    return result
}