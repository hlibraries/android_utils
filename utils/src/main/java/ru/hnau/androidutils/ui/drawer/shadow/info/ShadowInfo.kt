package ru.hnau.androidutils.ui.drawer.shadow.info

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.*


import ru.hnau.androidutils.ui.drawer.Insets
import ru.hnau.androidutils.ui.utils.BLUR_SIZE_BY_RADIUS


data class ShadowInfo(
        val offset: DpPxGetter,
        val blur: DpPxGetter,
        val color: ColorGetter,
        val alpha: Float
) {

    companion object {

        val DEFAULT = ShadowInfo(dp4, dp8, ColorGetter.BLACK, 0.5f)

        val DEFAULT_PRESSED = ShadowInfo(dp2, dp4, ColorGetter.BLACK, 0.5f)

    }

    val insets = run {
        val blur = blur * BLUR_SIZE_BY_RADIUS
        Insets(
                start = blur,
                top = (blur - offset).coerceAtLeast(dp0),
                end = blur,
                bottom = blur + offset
        )
    }

}