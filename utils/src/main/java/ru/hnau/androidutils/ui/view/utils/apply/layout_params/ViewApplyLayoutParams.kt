package ru.hnau.androidutils.ui.view.utils.apply.layout_params

import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.builders.*


fun <V : View> V.applyLayoutParams(layoutParams: ViewGroup.LayoutParams) =
        apply { this.layoutParams = layoutParams }

fun <V : View> V.applyLayoutParams(layoutParamsBuilder: LayoutParamsBuilder) =
        applyLayoutParams(layoutParamsBuilder.build())

fun <V : View> V.applyLayoutParams(configurator: (LayoutParamsBuilder.() -> Unit)? = null) =
        applyLayoutParams(LayoutParamsBuilder(this).also { configurator?.invoke(it) })

fun <V : View> V.applyMarginParams(configurator: (MarginLayoutParamsBuilder.() -> Unit)? = null) =
        applyLayoutParams(MarginLayoutParamsBuilder(this).also { configurator?.invoke(it) })

fun <V : View> V.applyLinearParams(configurator: (LinearLayoutParamsBuilder.() -> Unit)? = null) =
        applyLayoutParams(LinearLayoutParamsBuilder(this).also { configurator?.invoke(it) })

fun <V : View> V.applyFrameParams(configurator: (FrameLayoutParamsBuilder.() -> Unit)? = null) =
        applyLayoutParams(FrameLayoutParamsBuilder(this).also { configurator?.invoke(it) })

fun <V : View> V.applyRecycleParams(configurator: (RecycleLayoutParamsBuilder.() -> Unit)? = null) =
        applyLayoutParams(RecycleLayoutParamsBuilder(this).also { configurator?.invoke(it) })