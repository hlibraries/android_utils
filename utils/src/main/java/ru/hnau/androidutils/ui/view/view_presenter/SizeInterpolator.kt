package ru.hnau.androidutils.ui.view.view_presenter

import android.content.Context
import ru.hnau.androidutils.animations.ACCELERATE_DECELERATE_INTERPOLATOR
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.jutils.getIntInterInts
import kotlin.math.max
import kotlin.math.min


interface SizeInterpolator {

    companion object {

        fun create(interpolator: (from: Int, to: Int, percentage: Float) -> Int) =
                object : SizeInterpolator {
                    override fun interpolate(from: Int, to: Int, percentage: Float) =
                            interpolator.invoke(from, to, percentage)
                }

        fun create(context: Context, size: DpPxGetter) =
                Companion.create { _, _, _ -> size.getPxInt(context) }

        val SMOOTH = create { from, to, percentage ->
            getIntInterInts(from, to, ACCELERATE_DECELERATE_INTERPOLATOR.getInterpolation(percentage))
        }

        val LINEAR = create(::getIntInterInts)
        val MAX = create { from, to, _ -> max(from, to) }
        val MIN = create { from, to, _ -> min(from, to) }

        val DEFAULT = SMOOTH
    }

    fun interpolate(from: Int, to: Int, percentage: Float): Int

}
