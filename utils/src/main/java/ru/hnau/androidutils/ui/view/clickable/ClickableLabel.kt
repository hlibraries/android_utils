package ru.hnau.androidutils.ui.view.clickable

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RectCanvasShape
import ru.hnau.androidutils.ui.font_type.FontTypeGetter
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


@SuppressLint("ViewConstructor")
open class ClickableLabel(
        context: Context,
        initialText: StringGetter = StringGetter(),
        private val onClick: (() -> Unit)? = null,
        info: LabelInfo = LabelInfo(),
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : Label(
        context,
        initialText,
        info
) {

    constructor(
            context: Context,
            initialText: StringGetter = StringGetter(),
            onClick: (() -> Unit)? = null,
            fontType: FontTypeGetter? = LabelInfo.DEFAULT_FONT_TYPE,
            textColor: ColorGetter = LabelInfo.DEFAULT_TEXT_COLOR,
            textSize: DpPxGetter = LabelInfo.DEFAULT_TEXT_SIZE,
            gravity: HGravity = LabelInfo.DEFAULT_GRAVITY,
            maxLines: Int? = LabelInfo.DEFAULT_MAX_LINES,
            minLines: Int? = LabelInfo.DEFAULT_MIN_LINES,
            customLineHeight: DpPxGetter? = LabelInfo.DEFAULT_CUSTOM_LINE_HEIGHT,
            ellipsize: Boolean = LabelInfo.DEFAULT_ELLIPSIZE,
            normalizeForSingleLine: Boolean = LabelInfo.DEFAULT_NORMALIZE_FOR_SINGLE_LINE,
            underline: Boolean = LabelInfo.DEFAULT_UNDERLINE,
            rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
    ) : this(
            context = context,
            initialText = initialText,
            onClick = onClick,
            info = LabelInfo(fontType, textColor, textSize, gravity, maxLines, minLines, customLineHeight, ellipsize, normalizeForSingleLine, underline),
            rippleDrawInfo = rippleDrawInfo
    )

    private val boundsProducer =
            createBoundsProducer(false)

    private val canvasShape = RectCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClick = this::onClick
    )

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            animatingViewIsVisibleToUser = isVisibleToUserProducer,
            touchHandler = touchHandler,
            canvasShape = canvasShape,
            rippleDrawInfo = rippleDrawInfo
    )

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

    protected open fun onClick() {
        onClick?.invoke()
    }

    override fun draw(canvas: Canvas) {
        rippleDrawer.draw(canvas)
        super.draw(canvas)
    }

}

fun <G: ViewGroup> G.addClickableLabel(
        text: StringGetter = StringGetter(),
        onClick: (() -> Unit)? = null,
        info: LabelInfo = LabelInfo(),
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (Label.() -> Unit)? = null
) =
        addChild(
                ClickableLabel(
                        context,
                        text,
                        onClick,
                        info,
                        rippleDrawInfo
                ),
                viewConfigurator
        )

fun <G: ViewGroup> G.addClickableLabel(
        text: StringGetter = StringGetter(),
        onClick: (() -> Unit)? = null,
        fontType: FontTypeGetter? = LabelInfo.DEFAULT_FONT_TYPE,
        textColor: ColorGetter = LabelInfo.DEFAULT_TEXT_COLOR,
        textSize: DpPxGetter = LabelInfo.DEFAULT_TEXT_SIZE,
        gravity: HGravity = LabelInfo.DEFAULT_GRAVITY,
        maxLines: Int? = LabelInfo.DEFAULT_MAX_LINES,
        minLines: Int? = LabelInfo.DEFAULT_MIN_LINES,
        customLineHeight: DpPxGetter? = LabelInfo.DEFAULT_CUSTOM_LINE_HEIGHT,
        ellipsize: Boolean = LabelInfo.DEFAULT_ELLIPSIZE,
        normalizeForSingleLine: Boolean = LabelInfo.DEFAULT_NORMALIZE_FOR_SINGLE_LINE,
        underline: Boolean = LabelInfo.DEFAULT_UNDERLINE,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (Label.() -> Unit)? = null
) =
        addChild(
                ClickableLabel(
                        context,
                        text,
                        onClick,
                        fontType,
                        textColor,
                        textSize,
                        gravity,
                        maxLines,
                        minLines,
                        customLineHeight,
                        ellipsize,
                        normalizeForSingleLine,
                        underline,
                        rippleDrawInfo
                ),
                viewConfigurator
        )