package ru.hnau.androidutils.ui.view.waiter.material

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.waiter.WaiterView
import ru.hnau.androidutils.ui.view.waiter.material.drawer.MaterialWaiterDrawer
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterAnimationParams
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterColor
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterSize
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.locked_producer.LockedProducer


@SuppressLint("ViewConstructor")
open class MaterialWaiterView(
        context: Context,
        lockedProducer: LockedProducer,
        color: MaterialWaiterColor = MaterialWaiterColor.DEFAULT,
        size: MaterialWaiterSize = MaterialWaiterSize.DEFAULT,
        animationParams: MaterialWaiterAnimationParams = MaterialWaiterAnimationParams.DEFAULT,
        visibilitySwitchingTime: TimeValue = WaiterView.DEFAULT_VISIBILITY_SWITCHING_TIME,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : WaiterView(
        context = context,
        drawer = MaterialWaiterDrawer(context, color, size, animationParams),
        lockedProducer = lockedProducer,
        visibilitySwitchingTime = visibilitySwitchingTime,
        attrs = attrs,
        defStyleAttr = defStyleAttr
)


fun <G: ViewGroup> G.addMaterialWaiter(
        lockedProducer: LockedProducer,
        color: MaterialWaiterColor = MaterialWaiterColor.DEFAULT,
        size: MaterialWaiterSize = MaterialWaiterSize.DEFAULT,
        animationParams: MaterialWaiterAnimationParams = MaterialWaiterAnimationParams.DEFAULT,
        visibilitySwitchingTime: TimeValue = WaiterView.DEFAULT_VISIBILITY_SWITCHING_TIME,
        viewConfigurator: (MaterialWaiterView.() -> Unit)? = null
) =
        addChild(
                MaterialWaiterView(
                        context,
                        lockedProducer,
                        color,
                        size,
                        animationParams,
                        visibilitySwitchingTime
                ),
                viewConfigurator
        )