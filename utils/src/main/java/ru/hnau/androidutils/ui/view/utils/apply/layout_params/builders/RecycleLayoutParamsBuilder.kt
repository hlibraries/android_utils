package ru.hnau.androidutils.ui.view.utils.apply.layout_params.builders

import android.support.v7.widget.RecyclerView
import android.view.View


class RecycleLayoutParamsBuilder(
        view: View
) : MarginLayoutParamsBuilder(
        view
) {

    override fun build() =
            RecyclerView.LayoutParams(width, height).applyMargins()

}