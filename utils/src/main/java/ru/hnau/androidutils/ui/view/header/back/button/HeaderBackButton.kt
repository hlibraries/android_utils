package ru.hnau.androidutils.ui.view.header.back.button

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.utils.types_utils.doInState
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.header.button.HeaderButton
import ru.hnau.androidutils.ui.view.utils.isLTR


@SuppressLint("ViewConstructor")
class HeaderBackButton(
        context: Context,
        onClick: () -> Unit,
        private val info: HeaderBackButtonInfo = HeaderBackButtonInfo.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : HeaderButton(
        context,
        onClick,
        rippleDrawInfo
) {

    constructor(
            context: Context,
            onClick: () -> Unit,
            color: ColorGetter = HeaderBackButtonInfo.DEFAULT.color,
            iconSize: DpPxGetter = HeaderBackButtonInfo.DEFAULT.iconSize,
            IconLineWidth: DpPxGetter = HeaderBackButtonInfo.DEFAULT.iconLineWidth,
            iconLineCap: Paint.Cap = HeaderBackButtonInfo.DEFAULT.iconLineCap,
            rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
    ) : this(
            context = context,
            onClick = onClick,
            info = HeaderBackButtonInfo(
                    color = color,
                    iconSize = iconSize,
                    iconLineWidth = IconLineWidth,
                    iconLineCap = iconLineCap
            ),
            rippleDrawInfo = rippleDrawInfo
    )

    private val iconPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = info.color.get(context)
        style = Paint.Style.STROKE
        strokeCap = info.iconLineCap
        strokeWidth = info.iconLineWidth.getPx(context)
    }

    private val iconRadius = (info.iconSize - info.iconLineWidth).getPx(context) / 2

    private val iconPath = Path()

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        iconPath.reset()
        val cx = width / 2f
        val cy = height / 2f

        iconPath.moveTo(cx + iconRadius, cy)
        iconPath.lineTo(cx - iconRadius + info.iconLineWidth.getPx(context) / 2, cy)
        iconPath.moveTo(cx, cy - iconRadius)
        iconPath.lineTo(cx - iconRadius, cy)
        iconPath.lineTo(cx, cy + iconRadius)
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        if (!isLTR) {
            canvas.doInState {
                scale(-1f, 1f, width / 2f, 0f)
                canvas.drawPath(iconPath, iconPaint)
            }
        } else {
            canvas.drawPath(iconPath, iconPaint)
        }
    }

}

fun <G: ViewGroup> G.addHeaderBackButton(
        onClick: () -> Unit,
        info: HeaderBackButtonInfo = HeaderBackButtonInfo.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (HeaderBackButton.() -> Unit)? = null
) =
        addChild(
                HeaderBackButton(
                        context,
                        onClick,
                        info,
                        rippleDrawInfo
                ),
                viewConfigurator
        )

fun <G: ViewGroup> G.addHeaderBackButton(
        onClick: () -> Unit,
        color: ColorGetter = HeaderBackButtonInfo.DEFAULT.color,
        iconSize: DpPxGetter = HeaderBackButtonInfo.DEFAULT.iconSize,
        iconLineWidth: DpPxGetter = HeaderBackButtonInfo.DEFAULT.iconLineWidth,
        iconLineCap: Paint.Cap = HeaderBackButtonInfo.DEFAULT.iconLineCap,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (HeaderBackButton.() -> Unit)? = null
) =
        addChild(
                HeaderBackButton(
                        context,
                        onClick,
                        color,
                        iconSize,
                        iconLineWidth,
                        iconLineCap,
                        rippleDrawInfo
                ),
                viewConfigurator
        )