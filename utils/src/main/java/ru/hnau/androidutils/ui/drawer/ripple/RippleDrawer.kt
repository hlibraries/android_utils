package ru.hnau.androidutils.ui.drawer.ripple

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import ru.hnau.androidutils.ui.canvas_shape.CanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.animator.RippleAnimator
import ru.hnau.androidutils.ui.drawer.ripple.animator.RippleAnimatorCircle
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.utils.types_utils.doInState
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.utils.invalidate
import ru.hnau.androidutils.ui.view.utils.observeWhenVisibleToUser
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen


class RippleDrawer(
        context: Context,
        private val canvasShape: CanvasShape,
        touchHandler: TouchHandler,
        private val rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : Producer<Unit>() {

    constructor(
            animatingView: View,
            animatingViewIsVisibleToUser: Producer<Boolean>,
            canvasShape: CanvasShape,
            touchHandler: TouchHandler,
            rippleDrawInfo: RippleDrawInfo
    ) : this(
            context = animatingView.context,
            canvasShape = canvasShape,
            touchHandler = touchHandler,
            rippleDrawInfo = rippleDrawInfo
    ) {
        observeWhen(animatingViewIsVisibleToUser, animatingView::invalidate)
    }

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        this.color = rippleDrawInfo.color.get(context)
    }

    private val backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        this.color = rippleDrawInfo.backgroundColor.get(context)
    }

    private val rippleAnimator = RippleAnimator(
            context = context,
            rippleInfo = rippleDrawInfo.rippleInfo,
            touchHandler = touchHandler
    )

    init {
        rippleAnimator.attach(this::call)
    }

    fun draw(canvas: Canvas) = canvas.doInState {
        canvasShape.draw(canvas, backgroundPaint)
        rippleAnimator.draw(
                canvas,
                this@RippleDrawer::drawCircle,
                this@RippleDrawer::drawShadow
        )
    }

    private fun drawCircle(canvas: Canvas, drawInfo: RippleAnimatorCircle.DrawInfo) =
            canvas.doInState {
                canvasShape.clip(canvas)
                paint.alpha = (drawInfo.alpha * 255 * rippleDrawInfo.rippleAlpha).toInt()
                canvas.drawCircle(drawInfo.center.x, drawInfo.center.y, drawInfo.radius, paint)
            }

    private fun drawShadow(canvas: Canvas, alpha: Float) {
        paint.alpha = (alpha * 255 * rippleDrawInfo.rippleAlpha).toInt()
        canvasShape.draw(canvas, paint)
    }

}