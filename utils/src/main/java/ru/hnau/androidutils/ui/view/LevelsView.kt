package ru.hnau.androidutils.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.FrameLayout
import ru.hnau.androidutils.go_back_handler.GoBackHandler

@Deprecated("")
@SuppressLint("ViewConstructor")
class LevelsView(
        context: Context,
        innerViews: List<View>
) : FrameLayout(
        context
), GoBackHandler {

    private val innersHandlers = innerViews.mapNotNull { it as? GoBackHandler }.reversed()

    init {
        innerViews.forEach(this::addView)
    }

    override fun handleGoBack() =
            innersHandlers.find(GoBackHandler::handleGoBack) != null

}