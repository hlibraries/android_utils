package ru.hnau.androidutils.ui.drawer.shadow.drawer

import android.content.Context
import android.graphics.BlurMaskFilter
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.view.View
import android.view.animation.LinearInterpolator
import ru.hnau.androidutils.animations.TwoStatesAnimator
import ru.hnau.androidutils.ui.canvas_shape.CanvasShape
import ru.hnau.androidutils.ui.drawer.CanvasShapeDrawer
import ru.hnau.androidutils.ui.utils.types_utils.doInState
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.utils.invalidate
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler
import ru.hnau.jutils.helpers.Box
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen
import ru.hnau.jutils.takeIfPositive


class ButtonShadowDrawer(
        private val context: Context,
        canvasShape: CanvasShape,
        touchHandler: TouchHandler,
        private val shadowInfo: ButtonShadowInfo
) : CanvasShapeDrawer(
        canvasShape
) {

    constructor(
            animatingView: View,
            animatingViewIsVisibleToUser: Producer<Boolean>,
            canvasShape: CanvasShape,
            touchHandler: TouchHandler,
            shadowInfo: ButtonShadowInfo
    ) : this(
            context = animatingView.context,
            canvasShape = canvasShape,
            touchHandler = touchHandler,
            shadowInfo = shadowInfo
    ) {
        observeWhen(animatingViewIsVisibleToUser, animatingView::invalidate)
    }

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var shadowOffset = 0f

    private val pressedPercentageAnimator = TwoStatesAnimator(
            switchingTime = shadowInfo.animationTime,
            interpolator = LinearInterpolator()
    )

    private var pressedPercentage = 0f
        set(value) {
            field = value
            call(Unit)
        }

    val extraSize = shadowInfo.insets

    init {
        pressedPercentageAnimator.attach(this::onPressedPercentageChanged)
        touchHandler.attach(this::isPressedChanged)
    }

    fun draw(canvas: Canvas) = canvas.doInState {
        translate(0f, shadowOffset)
        canvasShape.draw(canvas, paint)
    }

    private fun onPressedPercentageChanged(pressedPercentage: Float) {
        this.pressedPercentage = pressedPercentage

        shadowOffset = shadowInfo.getOffset(context, pressedPercentage)

        paint.color = shadowInfo.getColor(context, pressedPercentage)

        val blurRadius = shadowInfo.getBlur(context, pressedPercentage)
        paint.maskFilter = blurRadius.takeIfPositive()?.let { BlurMaskFilter(it, BlurMaskFilter.Blur.NORMAL) }

        paint.alpha = (shadowInfo.getAlpha(pressedPercentage) * 255).toInt()

    }

    private fun isPressedChanged(pressedPointBox: Box<PointF?>) {
        pressedPercentageAnimator.animateTo(pressedPointBox.value != null)
    }

}