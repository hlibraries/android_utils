package ru.hnau.androidutils.ui.view.utils.apply

import android.widget.LinearLayout


enum class LinearLayoutOrientation(
        val id: Int
) {

    HORIZONTAL(LinearLayout.HORIZONTAL),
    VERTICAL(LinearLayout.VERTICAL)

}

fun <L: LinearLayout> L.applyOrientation(orientation: LinearLayoutOrientation) =
        apply { this.orientation = orientation.id }

fun <L: LinearLayout> L.applyHorizontalOrientation() =
        applyOrientation(LinearLayoutOrientation.HORIZONTAL)

fun <L: LinearLayout> L.applyVerticalOrientation() =
        applyOrientation(LinearLayoutOrientation.VERTICAL)