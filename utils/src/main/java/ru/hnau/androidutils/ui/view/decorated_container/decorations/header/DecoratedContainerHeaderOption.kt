package ru.hnau.androidutils.ui.view.decorated_container.decorations.header

import ru.hnau.androidutils.context_getters.StringGetter


data class DecoratedContainerHeaderOption(
        val title: StringGetter,
        val onClick: () -> Unit
)