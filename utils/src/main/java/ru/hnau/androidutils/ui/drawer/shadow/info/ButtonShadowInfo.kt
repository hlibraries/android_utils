package ru.hnau.androidutils.ui.drawer.shadow.info

import android.content.Context
import ru.hnau.androidutils.ui.drawer.max
import ru.hnau.androidutils.ui.drawer.Insets
import ru.hnau.androidutils.ui.utils.types_utils.ColorUtils
import ru.hnau.jutils.TimeValue


data class ButtonShadowInfo(
        val normal: ShadowInfo = ShadowInfo.DEFAULT,
        val pressed: ShadowInfo = ShadowInfo.DEFAULT_PRESSED,
        val animationTime: TimeValue = TimeValue.MILLISECOND * 150
) {

    companion object {

        val DEFAULT = ButtonShadowInfo()

    }

    val insets = max(normal.insets, pressed.insets)

    fun getOffset(context: Context, pressedPercentage: Float) =
            normal.offset.getPx(context) + (pressed.offset.getPx(context) - normal.offset.getPx(context)) * pressedPercentage

    fun getBlur(context: Context, pressedPercentage: Float) =
            normal.blur.getPx(context) + (pressed.blur.getPx(context) - normal.blur.getPx(context)) * pressedPercentage

    fun getColor(context: Context, pressedPercentage: Float) =
            ColorUtils.colorInterColors(normal.color.get(context), pressed.color.get(context), pressedPercentage)

    fun getAlpha(pressedPercentage: Float) =
            normal.alpha + (pressed.alpha - normal.alpha) * pressedPercentage
}