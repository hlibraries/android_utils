package ru.hnau.androidutils.ui.view.decorated_container.decorations.header

import android.graphics.drawable.ColorDrawable
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.toDrawableGetter
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.shadow_line.ShadowLineViewInfo


data class DecoratedContainerHeaderInfo(
        val buttonRippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        val backButtonDrawable: DrawableGetter = DrawableGetter.EMPTY,
        val optionsButtonDrawable: DrawableGetter = DrawableGetter.EMPTY,
        val titleLabelInfo: LabelInfo = LabelInfo(),
        val height: DpPxGetter = DpPxGetter.dp(44),
        val underStatusBar: Boolean = true,
        val background: DrawableGetter = ColorGetter.WHITE.mapWithAlpha(0.75f).toDrawableGetter(),
        val shadow: ShadowLineViewInfo = ShadowLineViewInfo.DEFAULT
) {

    companion object {

        val DEFAULT = DecoratedContainerHeaderInfo()

    }

}