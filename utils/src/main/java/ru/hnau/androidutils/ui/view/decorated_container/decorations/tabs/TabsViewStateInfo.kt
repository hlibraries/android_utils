package ru.hnau.androidutils.ui.view.decorated_container.decorations.tabs

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


data class TabsViewStateInfo(
        val color: ColorGetter,
        val titleTextSize: DpPxGetter
)