package ru.hnau.androidutils.ui.view.utils

import android.graphics.Rect
import android.graphics.RectF
import android.view.View
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


@Deprecated("Use applyPadding")
fun View.setPadding(horizontal: Int, vertical: Int) =
        setPadding(horizontal, vertical, horizontal, vertical)

@Deprecated("Use applyPadding")
fun View.setPadding(padding: Int) =
        setPadding(padding, padding)

@Deprecated("Use applyPadding")
fun View.setPadding(left: Float, top: Float, right: Float, bottom: Float) =
        setPadding(left.toInt(), top.toInt(), right.toInt(), bottom.toInt())

@Deprecated("Use applyPadding")
fun View.setPadding(horizontal: Float, vertical: Float) =
        setPadding(horizontal, vertical, horizontal, vertical)

@Deprecated("Use applyPadding")
fun View.setPadding(padding: Float) =
        setPadding(padding, padding)

@Deprecated("Use applyPadding")
fun View.setPadding(left: DpPxGetter, top: DpPxGetter, right: DpPxGetter, bottom: DpPxGetter) =
        setPadding(left.getPx(context), top.getPx(context), right.getPx(context), bottom.getPx(context))

@Deprecated("Use applyPadding")
fun View.setPadding(horizontal: DpPxGetter, vertical: DpPxGetter) =
        setPadding(horizontal, vertical, horizontal, vertical)

@Deprecated("Use applyPadding")
fun View.setPadding(padding: DpPxGetter) =
        setPadding(padding, padding)

@Deprecated("Use applyPadding")
fun View.setLeftPadding(leftPadding: Int) =
        setPadding(leftPadding, paddingTop, paddingRight, paddingBottom)

@Deprecated("Use applyPadding")
fun View.setLeftPadding(leftPadding: Float) =
        setLeftPadding(leftPadding.toInt())

@Deprecated("Use applyPadding")
fun View.setLeftPadding(leftPadding: DpPxGetter) =
        setLeftPadding(leftPadding.getPx(context))

@Deprecated("Use applyPadding")
fun View.setTopPadding(topPadding: Int) =
        setPadding(paddingLeft, topPadding, paddingRight, paddingBottom)

@Deprecated("Use applyPadding")
fun View.setTopPadding(topPadding: Float) =
        setTopPadding(topPadding.toInt())

@Deprecated("Use applyPadding")
fun View.setTopPadding(topPadding: DpPxGetter) =
        setTopPadding(topPadding.getPx(context))

@Deprecated("Use applyPadding")
fun View.setRightPadding(rightPadding: Int) =
        setPadding(paddingLeft, paddingTop, rightPadding, paddingBottom)

@Deprecated("Use applyPadding")
fun View.setRightPadding(rightPadding: Float) =
        setRightPadding(rightPadding.toInt())

@Deprecated("Use applyPadding")
fun View.setRightPadding(rightPadding: DpPxGetter) =
        setRightPadding(rightPadding.getPx(context))

@Deprecated("Use applyPadding")
fun View.setBottomPadding(bottomPadding: Int) =
        setPadding(paddingLeft, paddingTop, paddingRight, bottomPadding)

@Deprecated("Use applyPadding")
fun View.setBottomPadding(bottomPadding: Float) =
        setBottomPadding(bottomPadding.toInt())

@Deprecated("Use applyPadding")
fun View.setBottomPadding(bottomPadding: DpPxGetter) =
        setBottomPadding(bottomPadding.getPx(context))

@Deprecated("Use applyPadding")
fun View.setHorizontalPadding(horizontalPadding: Int) =
        setPadding(horizontalPadding, paddingTop, horizontalPadding, paddingBottom)

@Deprecated("Use applyPadding")
fun View.setHorizontalPadding(horizontalPadding: Float) =
        setHorizontalPadding(horizontalPadding.toInt())

@Deprecated("Use applyPadding")
fun View.setHorizontalPadding(horizontalPadding: DpPxGetter) =
        setHorizontalPadding(horizontalPadding.getPx(context))

@Deprecated("Use applyPadding")
fun View.setVerticalPadding(verticalPadding: Int) =
        setPadding(paddingLeft, verticalPadding, paddingRight, verticalPadding)

@Deprecated("Use applyPadding")
fun View.setVerticalPadding(verticalPadding: Float) =
        setVerticalPadding(verticalPadding.toInt())

@Deprecated("Use applyPadding")
fun View.setVerticalPadding(verticalPadding: DpPxGetter) =
        setVerticalPadding(verticalPadding.getPx(context))

val View.verticalPaddingSum: Int
    get() = paddingTop + paddingBottom

val View.horizontalPaddingSum: Int
    get() = paddingLeft + paddingRight

fun View.getContentRect(result: Rect) =
        result.set(
                paddingLeft,
                paddingTop,
                width - paddingRight,
                height - paddingBottom
        )

fun View.getContentRect(result: RectF) =
        result.set(
                paddingLeft.toFloat(),
                paddingTop.toFloat(),
                (width - paddingRight).toFloat(),
                (height - paddingBottom).toFloat()
        )