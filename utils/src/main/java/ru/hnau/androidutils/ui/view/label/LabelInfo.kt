package ru.hnau.androidutils.ui.view.label

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.ui.font_type.FontTypeGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


data class LabelInfo(
        val fontType: FontTypeGetter? = DEFAULT_FONT_TYPE,
        val textColor: ColorGetter = DEFAULT_TEXT_COLOR,
        val textSize: DpPxGetter = DEFAULT_TEXT_SIZE,
        val gravity: HGravity = DEFAULT_GRAVITY,
        val maxLines: Int? = DEFAULT_MAX_LINES,
        val minLines: Int? = DEFAULT_MIN_LINES,
        val customLineHeight: DpPxGetter? = DEFAULT_CUSTOM_LINE_HEIGHT,
        val ellipsize: Boolean = DEFAULT_ELLIPSIZE,
        val normalizeForSingleLine: Boolean = DEFAULT_NORMALIZE_FOR_SINGLE_LINE,
        val underline: Boolean = DEFAULT_UNDERLINE
) {

    companion object {

        val DEFAULT_FONT_TYPE: FontTypeGetter? = null
        val DEFAULT_TEXT_COLOR = ColorGetter.BLACK
        val DEFAULT_TEXT_SIZE = dp16
        val DEFAULT_GRAVITY = HGravity.START_TOP
        val DEFAULT_MAX_LINES: Int? = null
        val DEFAULT_MIN_LINES: Int? = null
        val DEFAULT_CUSTOM_LINE_HEIGHT: DpPxGetter? = null
        const val DEFAULT_ELLIPSIZE = true
        const val DEFAULT_NORMALIZE_FOR_SINGLE_LINE = true
        const val DEFAULT_UNDERLINE = false


    }

}