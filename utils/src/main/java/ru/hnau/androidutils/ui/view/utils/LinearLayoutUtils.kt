package ru.hnau.androidutils.ui.view.utils

import android.widget.LinearLayout
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


@Deprecated("Use LinearLayout.applyGravity")
fun LinearLayout.setLinearLayoutGravity(gravity: HGravity) {
    this.gravity = gravity.resolveAndroidGravity()
}

