package ru.hnau.androidutils.ui.view.utils.apply.layout_params.builders

import android.content.Context
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT


open class LayoutParamsBuilder(
        val view: View
) {

    val context = view.context

    var width: Int = ViewGroup.LayoutParams.WRAP_CONTENT
        private set

    var height: Int = ViewGroup.LayoutParams.WRAP_CONTENT
        private set

    fun setWidth(value: Int) {
        width = value
    }

    fun setWidth(value: DpPxGetter) =
            setWidth(value.getPxInt(context))

    fun setWidth(value: Float) =
            setWidth(value.toInt())

    fun setWidth(value: Byte) =
            setWidth(value.toInt())

    fun setWidth(value: Double) =
            setWidth(value.toInt())

    fun setWidth(value: Short) =
            setWidth(value.toInt())

    fun setWidth(value: Long) =
            setWidth(value.toInt())

    fun setMatchParentWidth() =
            setWidth(ViewGroup.LayoutParams.MATCH_PARENT)

    fun setHeight(value: Int) {
        height = value
    }

    fun setHeight(value: DpPxGetter) =
            setHeight(value.getPxInt(context))

    fun setHeight(value: Float) =
            setHeight(value.toInt())

    fun setHeight(value: Byte) =
            setHeight(value.toInt())

    fun setHeight(value: Double) =
            setHeight(value.toInt())

    fun setHeight(value: Short) =
            setHeight(value.toInt())

    fun setHeight(value: Long) =
            setHeight(value.toInt())

    fun setMatchParentHeight() =
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT)

    fun setSize(width: Int, height: Int) {
        setWidth(width)
        setHeight(height)
    }

    fun setSize(width: DpPxGetter, height: DpPxGetter) =
            setSize(width.getPxInt(context), height.getPxInt(context))

    fun setSize(width: Float, height: Float) =
            setSize(width.toInt(), height.toInt())

    fun setSize(width: Byte, height: Byte) =
            setSize(width.toInt(), height.toInt())

    fun setSize(width: Double, height: Double) =
            setSize(width.toInt(), height.toInt())

    fun setSize(width: Short, height: Short) =
            setSize(width.toInt(), height.toInt())

    fun setSize(width: Long, height: Long) =
            setSize(width.toInt(), height.toInt())

    fun setSize(value: Int) =
            setSize(value, value)

    fun setSize(value: DpPxGetter) =
            setSize(value.getPxInt(context))

    fun setSize(value: Float) =
            setSize(value.toInt())

    fun setSize(value: Byte) =
            setSize(value.toInt())

    fun setSize(value: Double) =
            setSize(value.toInt())

    fun setSize(value: Short) =
            setSize(value.toInt())

    fun setSize(value: Long) =
            setSize(value.toInt())

    fun setMatchParentSize() =
            setSize(ViewGroup.LayoutParams.MATCH_PARENT)

    open fun build() =
            ViewGroup.LayoutParams(width, height)

}