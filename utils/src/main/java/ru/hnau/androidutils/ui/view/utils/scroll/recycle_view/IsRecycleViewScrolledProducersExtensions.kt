package ru.hnau.androidutils.ui.view.utils.scroll.recycle_view

import android.support.v7.widget.RecyclerView
import ru.hnau.jutils.producer.AlwaysProducer
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filterUnique


fun RecyclerView.createOnRecyclerViewScrolledProducer() =
        object : AlwaysProducer<Unit>() {

            override val value = Unit

            private val onScrollListener =
                    object : RecyclerView.OnScrollListener() {
                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                            super.onScrolled(recyclerView, dx, dy)
                            onValueChanged()
                        }
                    }

            init {
                addOnScrollListener(onScrollListener)
            }

        }

fun RecyclerView.createRecycleViewIsScrolledToStartProducer(onRecycleScrolledProducer: Producer<Unit>) =
        onRecycleScrolledProducer.map { isRecycleViewScrolledToStart }.filterUnique()

fun RecyclerView.createRecycleViewIsScrolledToTopProducer(onRecycleScrolledProducer: Producer<Unit>) =
        onRecycleScrolledProducer.map { isRecycleViewScrolledToTop }.filterUnique()

fun RecyclerView.createRecycleViewIsScrolledToEndProducer(onRecycleScrolledProducer: Producer<Unit>) =
        onRecycleScrolledProducer.map { isRecycleViewScrolledToEnd }.filterUnique()

fun RecyclerView.createRecycleViewIsScrolledToBottomProducer(onRecycleScrolledProducer: Producer<Unit>) =
        onRecycleScrolledProducer.map { isRecycleViewScrolledToBottom }.filterUnique()