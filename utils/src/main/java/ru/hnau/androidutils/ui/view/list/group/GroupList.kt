package ru.hnau.androidutils.ui.view.list.group

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.list.base.BaseList
import ru.hnau.androidutils.ui.view.list.base.BaseListItemsDivider
import ru.hnau.androidutils.ui.view.list.base.BaseListOrientation
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.toInt


@SuppressLint("ViewConstructor")
open class GroupList<G, T>(
        context: Context,
        itemsProducer: Producer<List<ListGroup<G, T>>>,
        private val groupsViewWrappersCreator: () -> BaseListViewWrapper<G>,
        private val itemsViewWrappersCreator: () -> BaseListViewWrapper<T>,
        orientation: BaseListOrientation = BaseListOrientation.VERTICAL,
        fixedSize: Boolean = true,
        itemsDecoration: RecyclerView.ItemDecoration = BaseListItemsDivider(context)
) : BaseList<GroupListItem<G, T>>(
        context = context,
        fixedSize = fixedSize,
        orientation = orientation,
        itemsDecoration = itemsDecoration,
        itemsProducer = itemsProducer.map {
            it.map {
                val titleList = listOf(GroupListItem.createGroup<G, T>(it.group))
                val itemsList = it.items.map { GroupListItem.createItem<G, T>(it) }
                titleList + itemsList
            }.flatten()
        },
        itemTypeResolver = { it.isGroup.toInt() },
        viewWrappersCreator = { itemType ->
            GroupListViewWrapper.create(
                    itemType > 0,
                    groupsViewWrappersCreator,
                    itemsViewWrappersCreator
            )
        }
)

fun <G, T, VG: ViewGroup> VG.addGroupListView(
        itemsProducer: Producer<List<ListGroup<G, T>>>,
        groupsViewWrappers: () -> BaseListViewWrapper<G>,
        itemsViewWrappers: () -> BaseListViewWrapper<T>,
        orientation: BaseListOrientation = BaseListOrientation.VERTICAL,
        fixedSize: Boolean = true,
        itemsDecorationGetter: RecyclerView.ItemDecoration = BaseListItemsDivider(context),
        viewConfigurator: (GroupList<G, T>.() -> Unit)? = null
) =
        addChild(
                GroupList(
                        context,
                        itemsProducer,
                        groupsViewWrappers,
                        itemsViewWrappers,
                        orientation,
                        fixedSize,
                        itemsDecorationGetter
                ),
                viewConfigurator
        )