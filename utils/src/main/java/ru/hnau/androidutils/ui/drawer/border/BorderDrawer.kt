package ru.hnau.androidutils.ui.drawer.border

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import ru.hnau.androidutils.ui.canvas_shape.CanvasShape
import ru.hnau.androidutils.ui.drawer.CanvasShapeDrawer
import ru.hnau.jutils.producer.Producer


class BorderDrawer(
        private val context: Context,
        borderInfo: BorderInfo?,
        canvasShape: CanvasShape
) : CanvasShapeDrawer(
        canvasShape = canvasShape
) {

    private val paint = borderInfo?.let { borderInfo ->
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.STROKE
            strokeWidth = borderInfo.width.getPx(context)
            strokeJoin = borderInfo.lineJoin
            strokeCap = borderInfo.lineCap
            color = borderInfo.color.get(context)
            alpha = (borderInfo.alpha * 255).toInt()
        }
    }

    val preferredShapeTrimming = borderInfo?.width?.getPx(context)?.div(2f)

    fun draw(canvas: Canvas) = paint?.let { canvasShape.draw(canvas, it) }

}