package ru.hnau.androidutils.ui.drawables

import android.graphics.PixelFormat


enum class DrawableOpacity(val value: Int) {

    //С полупрозрачностью
    TRANSLUCENT(PixelFormat.TRANSLUCENT),

    //С прозрачностью
    TRANSPARENT(PixelFormat.TRANSPARENT),

    //Без прозрачности
    OPAQUE(PixelFormat.OPAQUE);

    companion object {

        fun fromValue(value: Int) = values().find { it.value == value } ?: OPAQUE

    }

}