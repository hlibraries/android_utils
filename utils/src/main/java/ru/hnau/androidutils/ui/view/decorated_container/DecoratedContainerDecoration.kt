package ru.hnau.androidutils.ui.view.decorated_container

import android.view.View
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.go_back_handler.GoBackHandler
import ru.hnau.androidutils.ui.utils.Side


interface DecoratedContainerDecoration: GoBackHandler {

    val view: View
    val side: Side
    val preferredSize: DpPxGetter

}