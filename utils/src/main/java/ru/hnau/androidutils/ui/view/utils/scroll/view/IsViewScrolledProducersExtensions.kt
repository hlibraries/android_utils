package ru.hnau.androidutils.ui.view.utils.scroll.view

import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.View
import ru.hnau.androidutils.utils.ContextConnector.init
import ru.hnau.jutils.producer.AlwaysProducer
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.filterUnique


fun View.createOnScrolledProducer() =
        object : AlwaysProducer<Unit>() {

            override val value = Unit

            init {
                addOnScrollChangedListener()
            }

            private fun addOnScrollChangedListener() {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    viewTreeObserver.addOnScrollChangedListener(this::onValueChanged)
                    return
                }

                setOnScrollChangeListener(this::onScrollChanged)
            }

            private fun onScrollChanged(view: View, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) =
                    onValueChanged()

        }

fun View.createIsScrolledToStartProducer(onScrolledProducer: Producer<Unit>) =
        onScrolledProducer.map { isScrolledToStart }.filterUnique()

fun View.createIsScrolledToTopProducer(onScrolledProducer: Producer<Unit>) =
        onScrolledProducer.map { isScrolledToTop }.filterUnique()

fun View.createIsScrolledToEndProducer(onScrolledProducer: Producer<Unit>) =
        onScrolledProducer.map { isScrolledToEnd }.filterUnique()

fun View.createIsScrolledToBottomProducer(onScrolledProducer: Producer<Unit>) =
        onScrolledProducer.map { isScrolledToBottom }.filterUnique()

@Deprecated(
        message = "Use createOnRecyclerViewScrolledProducer() instead",
        level = DeprecationLevel.ERROR,
        replaceWith = ReplaceWith(
                "this.createOnRecyclerViewScrolledProducer()",
                "ru.hnau.androidutils.ui.view.utils.scroll.recycle_view.createOnRecyclerViewScrolledProducer"
        )
)
fun RecyclerView.createOnScrolledProducer() {
}

@Deprecated(
        message = "Use createIsRecyclerViewScrolledToStartProducer(onScrolledProducer: Producer<Unit>) instead",
        level = DeprecationLevel.ERROR,
        replaceWith = ReplaceWith(
                "this.createRecycleViewIsScrolledToStartProducer(onScrolledProducer)",
                "ru.hnau.androidutils.ui.view.utils.scroll.recycle_view.createRecycleViewIsScrolledToStartProducer"
        )
)
fun RecyclerView.createIsScrolledToStartProducer(onScrolledProducer: Producer<Unit>) {
}

@Deprecated(
        message = "Use createIsRecyclerViewScrolledToTopProducer(onScrolledProducer: Producer<Unit>) instead",
        level = DeprecationLevel.ERROR,
        replaceWith = ReplaceWith(
                "this.createRecycleViewIsScrolledToTopProducer(onScrolledProducer)",
                "ru.hnau.androidutils.ui.view.utils.scroll.recycle_view.createRecycleViewIsScrolledToTopProducer"
        )
)
fun RecyclerView.createIsScrolledToTopProducer(onScrolledProducer: Producer<Unit>) {
}

@Deprecated(
        message = "Use createIsRecyclerViewScrolledToEndProducer(onScrolledProducer: Producer<Unit>) instead",
        level = DeprecationLevel.ERROR,
        replaceWith = ReplaceWith(
                "this.createRecycleViewIsScrolledToEndProducer(onScrolledProducer)",
                "ru.hnau.androidutils.ui.view.utils.scroll.recycle_view.createRecycleViewIsScrolledToEndProducer"
        )
)
fun RecyclerView.createIsScrolledToEndProducer(onScrolledProducer: Producer<Unit>) {
}

@Deprecated(
        message = "Use createIsRecyclerViewScrolledToBottomProducer(onScrolledProducer: Producer<Unit>) instead",
        level = DeprecationLevel.ERROR,
        replaceWith = ReplaceWith(
                "this.createRecycleViewIsScrolledToBottomProducer(onScrolledProducer)",
                "ru.hnau.androidutils.ui.view.utils.scroll.recycle_view.createRecycleViewIsScrolledToBottomProducer"
        )
)
fun RecyclerView.createIsScrolledToBottomProducer(onScrolledProducer: Producer<Unit>) {
}