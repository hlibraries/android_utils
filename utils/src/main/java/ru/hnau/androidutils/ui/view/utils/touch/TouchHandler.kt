package ru.hnau.androidutils.ui.view.utils.touch

import android.graphics.PointF
import android.view.MotionEvent
import ru.hnau.androidutils.ui.canvas_shape.CanvasShape
import ru.hnau.jutils.helpers.Box
import ru.hnau.jutils.helpers.toBox
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.producer.*
import ru.hnau.jutils.producer.detacher.ProducerDetachers


class TouchHandler(
        private val canvasShape: CanvasShape,
        private val cancelAfterLeaveCanvasShape: Boolean = false,
        private val onClick: (() -> Unit)? = null
) : StateProducer<Box<PointF?>>() {

    companion object {

        private val NO_POINT = null.toBox<PointF?>()

    }

    private val pressedPoint = PointF()

    val isPressed: Boolean
        get() = state != null

    private val detachers = ProducerDetachers()

    private var isEventInShapeCached: Boolean? = null

    fun handle(event: MotionEvent) {

        isEventInShapeCached = null

        if (event.isDown) {
            pressedPoint.set(event.x, event.y)
            update(pressedPoint.takeIf { isEventInShape(event) }.toBox())
            return
        }

        isPressed.ifFalse { return }

        if (cancelAfterLeaveCanvasShape && !isEventInShape(event)) {
            update(NO_POINT)
            return
        }

        if (event.isMove) {
            pressedPoint.set(event.x, event.y)
            update(pressedPoint.toBox())
            return
        }

        if (event.isUp && isEventInShape(event)) {
            onClick?.invoke()
        }

        if (event.isUpOrCancel) {
            update(NO_POINT)
        }

    }

    private fun isEventInShape(event: MotionEvent): Boolean {
        var eventInShape = isEventInShapeCached
        if (eventInShape == null) {
            eventInShape = canvasShape.contains(event.x, event.y)
            isEventInShapeCached = eventInShape
        }
        return eventInShape
    }

    override fun onFirstAttached() {
        super.onFirstAttached()
        canvasShape.attach(detachers) {}
    }

    override fun onLastDetached() {
        detachers.detachAllAndClear()
        super.onLastDetached()
    }

}