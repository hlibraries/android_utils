package ru.hnau.androidutils.ui.view.pager

import ru.hnau.androidutils.utils.savable.Savable


interface PageInfoContainer<S : Savable> {

    val pageInfo: PageInfo<S>

}