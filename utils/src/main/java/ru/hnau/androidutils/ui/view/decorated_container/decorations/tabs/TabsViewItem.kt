package ru.hnau.androidutils.ui.view.decorated_container.decorations.tabs

import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter


data class TabsViewItem(
        val title: StringGetter,
        val icon: DrawableGetter
)