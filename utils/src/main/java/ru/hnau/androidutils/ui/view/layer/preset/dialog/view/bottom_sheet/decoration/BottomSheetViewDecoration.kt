package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.decoration

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogViewDecoration
import ru.hnau.androidutils.ui.view.utils.makeAtMostMeasureSpec
import ru.hnau.androidutils.ui.view.utils.makeExactlyMeasureSpec
import kotlin.math.min


class BottomSheetViewDecoration(
        private val context: Context,
        info: BottomSheetViewDecorationInfo = BottomSheetViewDecorationInfo.DEFAULT
) : DialogViewDecoration {

    private val backgroundPaint = Paint().apply {
        color = info.backgroundColor.get(context)
    }

    private val contentRect = Rect()

    override fun measureDialogView(maxWidth: Int, maxHeight: Int, dialogView: View) {
        dialogView.measure(
                makeExactlyMeasureSpec(maxWidth),
                makeAtMostMeasureSpec(maxHeight)
        )
    }

    override fun layoutDialogView(maxWidth: Int, maxHeight: Int, dialogView: View) {
        val height = min(maxHeight, dialogView.measuredHeight)
        contentRect.set(0, maxHeight - height, maxWidth, maxHeight)
        dialogView.layout(contentRect.left, contentRect.top, contentRect.right, contentRect.bottom)
    }

    override fun draw(canvas: Canvas) {
        canvas.drawRect(contentRect, backgroundPaint)
    }

}