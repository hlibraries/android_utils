package ru.hnau.androidutils.ui.view.list.group

import ru.hnau.jutils.handle
import ru.hnau.jutils.helpers.Box


class GroupListItem<G, T> private constructor(
        private val group: Box<G>? = null,
        private val item: Box<T>? = null
) {

    companion object {

        fun <G, T> createGroup(group: G) =
                GroupListItem<G, T>(group = Box(group))

        fun <G, T> createItem(item: T) =
                GroupListItem<G, T>(item = Box(item))

    }

    val isGroup = group != null

    val groupOrItem: Any?
        get() = group.handle(
                ifNotNull = { it.value },
                ifNull = { item!!.value }
        )

}