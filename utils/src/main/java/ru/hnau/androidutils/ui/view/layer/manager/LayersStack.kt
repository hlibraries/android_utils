package ru.hnau.androidutils.ui.view.layer.manager

import ru.hnau.androidutils.ui.view.layer.layer.LayerWrapper
import ru.hnau.androidutils.ui.view.layer.transaction.BaseLayerTransaction
import ru.hnau.jutils.collections.findPos
import java.util.*


class LayersStack {

    private val stack = LinkedList<LayerWrapper>()

    val isEmpty: Boolean
        get() = stack.isEmpty()

    val canPoll: Boolean
        get() = stack.size > 1

    val peek: LayerWrapper?
        get() = synchronized(this) { if (isEmpty) null else stack.peek() }

    fun push(layerWrapper: LayerWrapper) {
        stack.push(layerWrapper)
        hideInvisibleLayers()
    }

    fun clear() = synchronized(this) {
        stack.forEach { it.release() }
        stack.clear()
    }

    fun pollOrNull(): LayerWrapper? {
        if (stack.size <= 1) {
            return null
        }
        val result = stack.poll() ?: return null
        showVisibleLayers()
        return result
    }

    fun forEach(action: (LayerWrapper) -> Unit) =
            stack.forEach(action)

    private fun hideInvisibleLayers() {
        var invisible = false
        stack.forEach { layerWrapper ->
            when {
                invisible -> layerWrapper.release()
                !layerWrapper.transparent -> invisible = true
            }
        }
    }

    fun forEachVisibleLayer(action: (layerWrapper: LayerWrapper) -> Unit) {

        val size = stack.size
        if (size <= 0) {
            return
        }

        val layersToShowCount = stack.findPos { !it.transparent }
                ?.let { it + 1 } ?: size

        repeat(layersToShowCount) {
            val i = layersToShowCount - it - 1
            action.invoke(stack[i])
        }
    }

    private fun showVisibleLayers() =
            forEachVisibleLayer { it.get() }

    fun invalidateAllExistenceLayers() =
            stack.reversed().forEach(LayerWrapper::invalidateLayerIfExists)

}