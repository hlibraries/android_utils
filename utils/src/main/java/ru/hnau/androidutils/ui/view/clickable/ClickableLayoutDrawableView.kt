package ru.hnau.androidutils.ui.view.clickable

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RectCanvasShape
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutType
import ru.hnau.androidutils.ui.drawables.layout_drawable.view.LayoutDrawableView
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


@SuppressLint("ViewConstructor")
open class ClickableLayoutDrawableView(
        context: Context,
        initialContent: DrawableGetter,
        initialLayoutType: LayoutType = LayoutType.DEFAULT,
        initialGravity: HGravity = HGravity.CENTER,
        private val onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : LayoutDrawableView(
        context,
        initialContent,
        initialLayoutType,
        initialGravity
) {

    private val boundsProducer =
            createBoundsProducer(false)

    private val canvasShape = RectCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClick = this::onClick
    )

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            animatingViewIsVisibleToUser = isVisibleToUserProducer,
            touchHandler = touchHandler,
            canvasShape = canvasShape,
            rippleDrawInfo = rippleDrawInfo
    )

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

    protected open fun onClick() {
        onClick?.invoke()
    }

    override fun draw(canvas: Canvas) {
        rippleDrawer.draw(canvas)
        super.draw(canvas)
    }

}

fun <G: ViewGroup> G.addClickableLayoutDrawableView(
        content: DrawableGetter,
        layoutType: LayoutType = LayoutType.DEFAULT,
        gravity: HGravity = HGravity.CENTER,
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableLayoutDrawableView.() -> Unit)? = null
) =
        addChild(
                ClickableLayoutDrawableView(context, content, layoutType, gravity, onClick, rippleDrawInfo),
                viewConfigurator
        )