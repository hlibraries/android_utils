package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.text

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.setPadding


@SuppressLint("ViewConstructor")
class BottomSheetText(
        context: Context,
        text: StringGetter,
        info: BottomSheetTextInfo = BottomSheetTextInfo.DEFAULT
) : Label(
        context = context,
        info = info.labelInfo,
        initialText = text
) {

    init {
        setPadding(
                info.paddingHorizontal,
                info.paddingTop,
                info.paddingHorizontal,
                info.paddingBottom
        )
    }

}