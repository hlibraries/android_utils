package ru.hnau.androidutils.ui.view.utils

import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Window
import android.view.WindowManager

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
private const val TRANSPARENT_STATUS_BAR_BASE_FLAGS = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
private const val TRANSPARENT_STATUS_BAR_LIGHT_ICONS_FLAGS = TRANSPARENT_STATUS_BAR_BASE_FLAGS

@RequiresApi(Build.VERSION_CODES.M)
private const val TRANSPARENT_STATUS_BAR_DARK_ICONS_FLAGS = TRANSPARENT_STATUS_BAR_BASE_FLAGS or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
private fun getTransparentStatusBarBaseFlags(lightStatusBarIcons: Boolean) =
        if (lightStatusBarIcons || Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            TRANSPARENT_STATUS_BAR_LIGHT_ICONS_FLAGS
        } else {
            TRANSPARENT_STATUS_BAR_DARK_ICONS_FLAGS
        }

@Deprecated("Use TransparentStatusBarActivity instead")
fun AppCompatActivity.initializeTransparentStatusBar(lightStatusBarIcons: Boolean) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return
    window.statusBarColor = Color.TRANSPARENT
    window.decorView.systemUiVisibility = getTransparentStatusBarBaseFlags(lightStatusBarIcons)
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
}