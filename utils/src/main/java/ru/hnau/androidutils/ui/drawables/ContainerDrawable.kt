package ru.hnau.androidutils.ui.drawables

import android.content.Context
import android.graphics.ColorFilter
import android.graphics.drawable.Drawable
import android.os.Build
import ru.hnau.androidutils.context_getters.DrawableGetter


abstract class ContainerDrawable(
        private val context: Context,
        initialContent: DrawableGetter
) : HDrawable(), Drawable.Callback {

    var content = initialContent
        set(value) = synchronized(this) {
            if (field != value) {
                field.get(context).callback = null
                field = value
                initNewContent(value)
                onContentChanged()
            }
        }

    protected val contentInner: Drawable
        get() = content.get(context)

    private var alphaInner = 255
        set(value) {
            if (field != value) {
                field = value
                contentInner.alpha = value
            }
        }

    private var colorFilterInner: ColorFilter? = null
        set(value) {
            if (field != value) {
                field = value
                contentInner.colorFilter = value
            }
        }

    init {
        initNewContent(initialContent)
    }

    protected open fun onContentChanged() {}

    private fun initNewContent(newContent: DrawableGetter) = with(newContent.get(context)) {
        callback = this@ContainerDrawable
        alpha = alphaInner
        colorFilter = colorFilterInner
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layoutDirection = this@ContainerDrawable.layoutDirection
        }
    }

    override fun getIntrinsicWidth() = contentInner.intrinsicWidth
    override fun getIntrinsicHeight() = contentInner.intrinsicHeight

    override fun getDrawableOpacity() =
            DrawableOpacity.fromValue(contentInner.opacity)

    override fun setAlpha(alpha: Int) {
        alphaInner = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        colorFilterInner = colorFilter
    }

    override fun onLayoutDirectionChanged(layoutDirection: Int) =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    contentInner.setLayoutDirection(layoutDirection)

    override fun unscheduleDrawable(who: Drawable, what: Runnable) {
        callback?.unscheduleDrawable(this, what)
    }

    override fun invalidateDrawable(who: Drawable) {
        callback?.invalidateDrawable(this)
    }

    override fun scheduleDrawable(who: Drawable, what: Runnable, time: Long) {
        callback?.scheduleDrawable(this, what, time)
    }

}