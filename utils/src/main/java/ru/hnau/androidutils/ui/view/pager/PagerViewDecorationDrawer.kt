package ru.hnau.androidutils.ui.view.pager

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.view.view_changer.ViewDecorationDrawer


class PagerViewDecorationDrawer(
        context: Context,
        shadowColor: ColorGetter = DEFAULT_SHADOW_COLOR,
        private val darkestShadowAlpha: Float = DEFAULT_DARKEST_SHADOW_ALPHA
) : ViewDecorationDrawer {

    companion object {
        val DEFAULT_SHADOW_COLOR = ColorGetter.BLACK
        const val DEFAULT_DARKEST_SHADOW_ALPHA = 0.75f
    }

    private val viewBounds = Rect()

    private val shadowPaint = Paint().apply {
        color = shadowColor.get(context)
    }

    override fun draw(
            canvas: Canvas,
            view: View,
            showing: Boolean,
            showingHidingPercentage: Float,
            scrollFactor: Float,
            onTop: Boolean
    ) {
        if (onTop) {
            return
        }

        val shadowPercentage = if (showing) 1 - showingHidingPercentage else showingHidingPercentage
        shadowPaint.alpha = (255 * shadowPercentage * darkestShadowAlpha).toInt()
        viewBounds.set(0, 0, view.width, view.height)
        canvas.drawRect(viewBounds, shadowPaint)
    }

}