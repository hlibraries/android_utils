package ru.hnau.androidutils.ui.drawer

import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp0
import ru.hnau.androidutils.ui.view.utils.handleLayoutDirection
import ru.hnau.androidutils.context_getters.dp_px.max
import ru.hnau.androidutils.context_getters.dp_px.min


data class Insets(
        val start: DpPxGetter = dp0,
        val top: DpPxGetter = dp0,
        val end: DpPxGetter = dp0,
        val bottom: DpPxGetter = dp0
) {

    companion object {

        val EMPTY = Insets(dp0)

    }

    constructor(
            horizontal: DpPxGetter,
            vertical: DpPxGetter
    ) : this(
            start = horizontal,
            top = vertical,
            end = horizontal,
            bottom = vertical
    )

    constructor(
            value: DpPxGetter
    ) : this(
            horizontal = value,
            vertical = value
    )

    val horizontalSum = start + end

    val verticalSum = top + bottom

    val left: DpPxGetter
        get() = handleLayoutDirection(start, end)

    val right: DpPxGetter
        get() = handleLayoutDirection(end, start)

    operator fun plus(other: Insets) = Insets(
            start = this.start + other.start,
            top = this.top + other.top,
            end = this.end + other.end,
            bottom = this.bottom + other.bottom
    )

}

fun max(insets1: Insets, insets2: Insets) = Insets(
        start = max(insets1.start, insets2.start),
        top = max(insets1.top, insets2.top),
        end = max(insets1.end, insets2.end),
        bottom = max(insets1.bottom, insets2.bottom)
)

fun min(insets1: Insets, insets2: Insets) = Insets(
        start = min(insets1.start, insets2.start),
        top = min(insets1.top, insets2.top),
        end = min(insets1.end, insets2.end),
        bottom = min(insets1.bottom, insets2.bottom)
)