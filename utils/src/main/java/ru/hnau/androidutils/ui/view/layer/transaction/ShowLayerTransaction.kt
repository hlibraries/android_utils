package ru.hnau.androidutils.ui.view.layer.transaction

import android.graphics.Point
import ru.hnau.androidutils.ui.view.layer.layer.LayerWrapper


class ShowLayerTransaction(
        layerWrapper: LayerWrapper,
        info: TransactionInfo,
        val clearStack: Boolean,
        onFinished: (BaseLayerTransaction) -> Unit
) : BaseLayerTransaction(
        layerWrapper,
        info,
        onFinished
) {

    override val positive = true

    override val interpolator = info.showInterpolator

    override fun beforeStart() {
        layerWrapper.get()
        super.beforeStart()
    }

    override fun calculateFromAndToOffsets(
            width: Int, height: Int,
            rtl: Boolean,
            fromOffset: Point, toOffset: Point
    ) {
        toOffset.set(0, 0)
        val direction = info.emersionSide.getDirection(rtl)
        fromOffset.set(direction.x * width, direction.y * height)
    }

}