package ru.hnau.androidutils.ui

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Window
import android.view.WindowManager
import ru.hnau.androidutils.ui.utils.ThemeManager
import ru.hnau.jutils.producer.detacher.ProducerDetachers


abstract class TransparentStatusBarActivity : AppCompatActivity() {

    companion object {

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
        private const val TRANSPARENT_STATUS_BAR_BASE_FLAGS = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
        private const val TRANSPARENT_STATUS_BAR_LIGHT_ICONS_FLAGS = TRANSPARENT_STATUS_BAR_BASE_FLAGS

        @RequiresApi(Build.VERSION_CODES.M)
        private const val TRANSPARENT_STATUS_BAR_DARK_ICONS_FLAGS = TRANSPARENT_STATUS_BAR_BASE_FLAGS or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
        private fun getTransparentStatusBarBaseFlags(lightStatusBarIcons: Boolean) =
                if (lightStatusBarIcons || Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    TRANSPARENT_STATUS_BAR_LIGHT_ICONS_FLAGS
                } else {
                    TRANSPARENT_STATUS_BAR_DARK_ICONS_FLAGS
                }

    }

    private var isStatusBarIconsLight: Boolean? = null
        set(value) {
            if (field != value && value != null) {
                field = value
                updateStatusBarIonsIsLight(value)
            }
        }

    private val detachers = ProducerDetachers()

    override fun onCreate(savedInstanceState: Bundle?) {
        initInvisibleStatusBar()
        ThemeManager.attach(detachers) { updateStatusBarIconsIsLight() }
        super.onCreate(savedInstanceState)
    }

    private fun initInvisibleStatusBar() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return
        window.statusBarColor = Color.TRANSPARENT
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        updateStatusBarIconsIsLight()
    }

    override fun onDestroy() {
        detachers.detachAllAndClear()
        super.onDestroy()
    }

    protected fun updateStatusBarIconsIsLight() {
        isStatusBarIconsLight = getIsStatusBarIconsLight()
    }

    protected abstract fun getIsStatusBarIconsLight(): Boolean

    private fun updateStatusBarIonsIsLight(iconsIsLight: Boolean) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) return
        window.decorView.systemUiVisibility = getTransparentStatusBarBaseFlags(iconsIsLight)
    }

}