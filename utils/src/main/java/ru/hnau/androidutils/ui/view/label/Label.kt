package ru.hnau.androidutils.ui.view.label

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.font_type.FontTypeGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.*


@SuppressLint("ViewConstructor")
open class Label(
        context: Context,
        initialText: StringGetter = StringGetter(),
        info: LabelInfo = LabelInfo()
) : View(
        context
), LabelDrawableHelper {

    constructor(
            context: Context,
            initialText: StringGetter = StringGetter(),
            fontType: FontTypeGetter? = LabelInfo.DEFAULT_FONT_TYPE,
            textColor: ColorGetter = LabelInfo.DEFAULT_TEXT_COLOR,
            textSize: DpPxGetter = LabelInfo.DEFAULT_TEXT_SIZE,
            gravity: HGravity = LabelInfo.DEFAULT_GRAVITY,
            maxLines: Int? = LabelInfo.DEFAULT_MAX_LINES,
            minLines: Int? = LabelInfo.DEFAULT_MIN_LINES,
            customLineHeight: DpPxGetter? = LabelInfo.DEFAULT_CUSTOM_LINE_HEIGHT,
            ellipsize: Boolean = LabelInfo.DEFAULT_ELLIPSIZE,
            normalizeForSingleLine: Boolean = LabelInfo.DEFAULT_NORMALIZE_FOR_SINGLE_LINE,
            underline: Boolean = LabelInfo.DEFAULT_UNDERLINE
    ) : this(
            context = context,
            initialText = initialText,
            info = LabelInfo(fontType, textColor, textSize, gravity, maxLines, minLines, customLineHeight, ellipsize, normalizeForSingleLine, underline)
    )

    private val drawableCallback = object : Drawable.Callback {
        override fun unscheduleDrawable(who: Drawable?, what: Runnable?) {}

        override fun invalidateDrawable(who: Drawable?) {
            requestLayout()
            invalidate()
        }

        override fun scheduleDrawable(who: Drawable?, what: Runnable?, time: Long) {}

    }

    private val drawable = LabelDrawable(context, initialText, info, this).apply {
        callback = drawableCallback
    }

    var text: StringGetter
        set(value) {
            drawable.text = value
        }
        get() = drawable.text

    var fontType: FontTypeGetter?
        set(value) {
            drawable.fontType = value
        }
        get() = drawable.fontType


    var textColor: ColorGetter
        set(value) {
            drawable.textColor = value
        }
        get() = drawable.textColor

    var textSize: DpPxGetter
        set(value) {
            drawable.textSize = value
        }
        get() = drawable.textSize

    var gravity: HGravity
        set(value) {
            drawable.gravity = value
        }
        get() = drawable.gravity

    var maxLines: Int?
        set(value) {
            drawable.maxLines = value
        }
        get() = drawable.maxLines

    var minLines: Int?
        set(value) {
            drawable.minLines = value
        }
        get() = drawable.minLines

    var customLineHeight: DpPxGetter?
        set(value) {
            drawable.customLineHeight = value
        }
        get() = drawable.customLineHeight

    var ellipsize: Boolean
        set(value) {
            drawable.ellipsize = value
        }
        get() = drawable.ellipsize

    var underline: Boolean
        set(value) {
            drawable.underline = value
        }
        get() = drawable.underline

    var normalizeForSingleLine: Boolean
        set(value) {
            drawable.normalizeForSingleLine = value
        }
        get() = drawable.normalizeForSingleLine

    fun setLabelInfo(labelInfo: LabelInfo) = drawable.setLabelInfo(labelInfo)

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        drawable.draw(canvas)
    }

    override fun layout(l: Int, t: Int, r: Int, b: Int) {
        super.layout(l, t, r, b)
        drawable.setBounds(
                paddingLeft,
                paddingTop,
                width - horizontalPaddingSum,
                height - verticalPaddingSum
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val widthMS = resizeMeasureSpec(widthMeasureSpec, -horizontalPaddingSum)
        val heightMS = resizeMeasureSpec(heightMeasureSpec, -verticalPaddingSum)

        drawable.onMeasure(widthMS, heightMS)

        setMeasuredDimension(
                drawable.intrinsicWidth + horizontalPaddingSum,
                drawable.intrinsicHeight + verticalPaddingSum
        )
    }


    override fun calculateDefaultMeasurement(measureSpec: Int, size: Int) =
            getDefaultMeasurement(measureSpec, size)

    override fun calculateMaxMeasurement(measureSpec: Int, sizeForUnspecified: Int) =
            getMaxMeasurement(measureSpec, sizeForUnspecified)

    override fun calculateMaxMeasurementOrNullIfUnspecified(measureSpec: Int) =
            getMaxMeasurementOrNullIfUnspecified(measureSpec)

}

fun <G : ViewGroup> G.addLabel(
        text: StringGetter = StringGetter(),
        info: LabelInfo = LabelInfo(),
        viewConfigurator: (Label.() -> Unit)? = null
) =
        addChild(
                Label(
                        context,
                        text,
                        info
                ),
                viewConfigurator
        )

fun <G : ViewGroup> G.addLabel(
        text: StringGetter = StringGetter(),
        fontType: FontTypeGetter? = LabelInfo.DEFAULT_FONT_TYPE,
        textColor: ColorGetter = LabelInfo.DEFAULT_TEXT_COLOR,
        textSize: DpPxGetter = LabelInfo.DEFAULT_TEXT_SIZE,
        gravity: HGravity = LabelInfo.DEFAULT_GRAVITY,
        maxLines: Int? = LabelInfo.DEFAULT_MAX_LINES,
        minLines: Int? = LabelInfo.DEFAULT_MIN_LINES,
        customLineHeight: DpPxGetter? = LabelInfo.DEFAULT_CUSTOM_LINE_HEIGHT,
        ellipsize: Boolean = LabelInfo.DEFAULT_ELLIPSIZE,
        normalizeForSingleLine: Boolean = LabelInfo.DEFAULT_NORMALIZE_FOR_SINGLE_LINE,
        underline: Boolean = LabelInfo.DEFAULT_UNDERLINE,
        viewConfigurator: (Label.() -> Unit)? = null
) =
        addChild(
                Label(
                        context,
                        text,
                        fontType,
                        textColor,
                        textSize,
                        gravity,
                        maxLines,
                        minLines,
                        customLineHeight,
                        ellipsize,
                        normalizeForSingleLine,
                        underline
                ),
                viewConfigurator
        )