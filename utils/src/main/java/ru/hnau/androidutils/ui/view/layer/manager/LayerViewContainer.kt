package ru.hnau.androidutils.ui.view.layer.manager

import ru.hnau.androidutils.ui.view.layer.layer.Layer


interface LayerViewContainer {

    fun addLayer(layer: Layer)

    fun removeLayer(layer: Layer)

}