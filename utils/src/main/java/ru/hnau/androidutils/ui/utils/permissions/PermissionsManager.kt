package ru.hnau.androidutils.ui.utils.permissions

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import kotlinx.coroutines.CompletableDeferred
import ru.hnau.androidutils.utils.AutokeyMapCache
import ru.hnau.androidutils.utils.generateId
import ru.hnau.jutils.ifTrue


object PermissionsManager {

    private val finishers = AutokeyMapCache<Int, CompletableDeferred<Boolean>>(
            keysGenerator = ::generateId,
            tryCount = 1000
    )

    suspend fun requestPermission(
            activity: Activity,
            permissionName: String,
            showExplanation: suspend () -> Unit = {}
    ) {

        if (checkHasPermission(permissionName)) {
            return
        }

        if (needShowExplanation(activity, permissionName)) {
            showExplanation()
        }

        val deferredResult: CompletableDeferred<Boolean> = CompletableDeferred<Boolean>()
        val requestCode = finishers.put(deferredResult)!!

        ActivityCompat.requestPermissions(
                activity,
                arrayOf(permissionName),
                requestCode
        )

        deferredResult.await().ifTrue { return }

        if (needShowExplanation(activity, permissionName)) {
            throw OnPermissionDeniedException(permissionName)
        }

        throw OnPermissionDeniedForeverException(permissionName)
    }

    private fun needShowExplanation(activity: Activity, permissionName: String) =
            ActivityCompat.shouldShowRequestPermissionRationale(activity, permissionName)

    fun onRequestResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        val deferredResult =
                finishers.remove(requestCode) ?: return

        val result =
                grantResults.getOrNull(0) ?: return

        deferredResult.complete(result == PackageManager.PERMISSION_GRANTED)
    }

}