package ru.hnau.androidutils.ui.view.layer.transaction

import android.graphics.Point
import android.view.animation.Interpolator
import ru.hnau.androidutils.ui.view.layer.layer.LayerWrapper


class HideLayerTransaction(
        layerWrapper: LayerWrapper,
        info: TransactionInfo,
        onFinished: (BaseLayerTransaction) -> Unit
) : BaseLayerTransaction(
        layerWrapper,
        info,
        onFinished
) {

    override val positive = false

    override val interpolator = info.hideInterpolator

    override fun afterFinish() {
        super.afterFinish()
        layerWrapper.release()
    }

    override fun calculateFromAndToOffsets(
            width: Int, height: Int,
            rtl: Boolean,
            fromOffset: Point, toOffset: Point
    ) {
        fromOffset.set(0, 0)
        val direction = info.emersionSide.getDirection(rtl)
        toOffset.set(direction.x * width, direction.y * height)
    }

}