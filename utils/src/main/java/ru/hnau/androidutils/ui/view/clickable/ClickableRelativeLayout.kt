package ru.hnau.androidutils.ui.view.clickable

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.RelativeLayout
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RectCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


@SuppressLint("ViewConstructor")
open class ClickableRelativeLayout(
        context: Context,
        private val onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : RelativeLayout(
        context
) {

    private val boundsProducer =
            createBoundsProducer(false)

    private val canvasShape = RectCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClick = this::onClick
    )

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            animatingViewIsVisibleToUser = isVisibleToUserProducer,
            touchHandler = touchHandler,
            canvasShape = canvasShape,
            rippleDrawInfo = rippleDrawInfo
    )

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

    protected open fun onClick() {
        onClick?.invoke()
    }

    override fun dispatchDraw(canvas: Canvas) {
        rippleDrawer.draw(canvas)
        super.dispatchDraw(canvas)
    }

}

fun <G: ViewGroup> G.addClickableRelativeLayout(
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableRelativeLayout.() -> Unit)? = null
) =
        addChild(
                ClickableRelativeLayout(
                        context,
                        onClick,
                        rippleDrawInfo
                ),
                viewConfigurator
        )