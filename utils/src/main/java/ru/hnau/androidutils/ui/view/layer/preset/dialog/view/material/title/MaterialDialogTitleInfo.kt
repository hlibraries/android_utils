package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.title

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.font_type.FontTypeGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.LabelInfo


data class MaterialDialogTitleInfo(
        val labelInfo: LabelInfo = LabelInfo(
                fontType = FontTypeGetter.DEFAULT_BOLD,
                gravity = HGravity.START_CENTER_VERTICAL,
                textSize = dp(20),
                maxLines = null,
                minLines = 1,
                textColor = ColorGetter.BLACK
        ),
        val paddingHorizontal: DpPxGetter = dp16,
        val paddingTop: DpPxGetter = dp8,
        val paddingBottom: DpPxGetter = dp16
) {

    companion object {

        val DEFAULT = MaterialDialogTitleInfo()

    }

}