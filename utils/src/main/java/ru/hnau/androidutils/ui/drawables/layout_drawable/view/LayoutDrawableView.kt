package ru.hnau.androidutils.ui.drawables.layout_drawable.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutType
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement


@SuppressLint("ViewConstructor")
open class LayoutDrawableView(
        context: Context,
        initialContent: DrawableGetter,
        initialLayoutType: LayoutType = LayoutType.DEFAULT,
        initialGravity: HGravity = HGravity.CENTER
) : View(context) {

    companion object;

    private val drawable = LayoutDrawable(
            context,
            initialContent,
            initialLayoutType,
            initialGravity
    ).apply {
        super.setBackgroundDrawable(this)
    }

    var layoutType: LayoutType
        set(value) {
            drawable.layoutType = value
        }
        get() = drawable.layoutType

    var gravity: HGravity
        set(value) {
            drawable.gravity = value
        }
        get() = drawable.gravity

    var content: DrawableGetter
        set(value) {
            drawable.content = value
        }
        get() = drawable.content

    override fun setBackgroundDrawable(background: Drawable?) {}

    override fun setPadding(left: Int, top: Int, right: Int, bottom: Int) {
        super.setPadding(left, top, right, bottom)
        drawable.setPadding(left, top, right, bottom)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) = setMeasuredDimension(
            getDefaultMeasurement(widthMeasureSpec, drawable.intrinsicWidth),
            getDefaultMeasurement(heightMeasureSpec, drawable.intrinsicHeight)
    )

}

fun <G: ViewGroup> G.addLayoutDrawableView(
        content: DrawableGetter,
        layoutType: LayoutType = LayoutType.DEFAULT,
        gravity: HGravity = HGravity.CENTER,
        viewConfigurator: (LayoutDrawableView.() -> Unit)? = null
) =
        addChild(
                LayoutDrawableView(context, content, layoutType, gravity),
                viewConfigurator
        )