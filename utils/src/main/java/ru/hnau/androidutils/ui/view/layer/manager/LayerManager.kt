package ru.hnau.androidutils.ui.view.layer.manager

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.ViewGroup
import ru.hnau.androidutils.animations.AnimationMetronome
import ru.hnau.androidutils.ui.utils.ScaleManager
import ru.hnau.androidutils.ui.utils.ThemeManager
import ru.hnau.androidutils.ui.utils.types_utils.ColorUtils
import ru.hnau.androidutils.ui.view.layer.layer.Layer
import ru.hnau.androidutils.ui.view.layer.layer.LayerWrapper
import ru.hnau.androidutils.ui.view.layer.transaction.BaseLayerTransaction
import ru.hnau.androidutils.ui.view.layer.transaction.HideLayerTransaction
import ru.hnau.androidutils.ui.view.layer.transaction.ShowLayerTransaction
import ru.hnau.androidutils.ui.view.layer.transaction.TransactionInfo
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.detacher.ProducerDetachers
import ru.hnau.jutils.producer.extensions.observeWhen
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList


@SuppressLint("ViewConstructor")
class LayerManager(
        context: Context,
        transactionInfo: TransactionInfo = TransactionInfo.DEFAULT
) : ViewGroup(
        context
),
        LayerViewContainer,
        LayerManagerConnector {

    private val shadingColorMaxAlpha = ColorUtils.extractAlpha(transactionInfo.shadingColor.get(context))

    private val shadingPaint = Paint().apply {
        color = ColorUtils.removeAlpha(transactionInfo.shadingColor.get(context))
    }

    override val defaultTransactionInfo = transactionInfo

    private val shadingRect = Rect()

    private val stack = LayersStack()

    private val transactions = CopyOnWriteArrayList<BaseLayerTransaction>()

    private var isVisibleToUser = false
        set(value) {
            field = value
            updateIsAnimating()
        }

    private val animatingDetachers = ProducerDetachers()

    private var animating = false
        set(value) = synchronized(this) {
            if (field != value) {
                field = value
                value.handle(
                        onTrue = { AnimationMetronome.attach(animatingDetachers, this::requestLayout) },
                        onFalse = animatingDetachers::detachAllAndClear
                )
            }
        }

    override val viewContext = context

    override val isEmpty: Boolean
        get() = stack.isEmpty

    override val canGoBack: Boolean
        get() = stack.canPoll

    private val isVisibleToUserProducer = createIsVisibleToUserProducer()

    init {
        isVisibleToUserProducer.attach { isVisibleToUser = it }
        ThemeManager.observeWhen(isVisibleToUserProducer) { invalidateAllLayers() }
        ScaleManager.observeWhen(isVisibleToUserProducer) { invalidateAllLayers() }
    }

    override fun dispatchDraw(canvas: Canvas) {

        stack.forEachVisibleLayer { drawLayer(canvas, it) }

        (transactions as List<BaseLayerTransaction>).forEach { transaction ->
            drawLayer(
                    canvas = canvas,
                    child = transaction.layerWrapper,
                    visibility = transaction.visibility,
                    forceShading = true
            )
        }
    }

    private fun drawLayer(
            canvas: Canvas,
            child: LayerWrapper,
            visibility: Float = 1f,
            forceShading: Boolean = false
    ) {
        val layer = child.get()
        if (layer.shading || forceShading) {
            shadingPaint.alpha = (shadingColorMaxAlpha * visibility).toInt()
            canvas.drawRect(shadingRect, shadingPaint)
        }
        drawChild(canvas, layer.view, drawingTime)
    }

    private fun onTransactionFinished(transaction: BaseLayerTransaction) {
        transactions.remove(transaction)
        updateIsAnimating()
        when (transaction) {
            is ShowLayerTransaction -> {
                if (transaction.clearStack) {
                    stack.clear()
                }
                stack.push(transaction.layerWrapper)
            }
        }
    }

    private fun addTransaction(transaction: BaseLayerTransaction) {
        transactions.add(transaction)
        updateIsAnimating()
    }

    private fun updateIsAnimating() {
        animating = isVisibleToUser && transactions.isNotEmpty()
    }

    override fun showLayer(
            layer: Layer,
            clearStack: Boolean,
            transactionInfo: TransactionInfo
    ) {
        val layerWrapper = LayerWrapper(
                context,
                layer,
                this,
                this,
                transactionInfo
        )
        if (isEmpty) {
            stack.push(layerWrapper)
            return
        }
        val transaction = ShowLayerTransaction(
                layerWrapper,
                transactionInfo,
                clearStack,
                this::onTransactionFinished
        )
        addTransaction(transaction)
        transaction.start()
    }

    override fun handleGoBack(): Boolean {

        if (stack.peek?.handleGoBack() == true) {
            return true
        }

        return goBack()
    }

    override fun goBack(): Boolean {
        val layerWrapper = stack.pollOrNull() ?: return false
        val transaction = HideLayerTransaction(
                layerWrapper,
                layerWrapper.transactionInfo,
                this::onTransactionFinished
        )
        addTransaction(transaction)
        transaction.start()
        return true
    }

    private fun invalidateAllLayers() {
        stack.invalidateAllExistenceLayers()
        transactions.forEach(BaseLayerTransaction::invalidateLayer)
    }

    override fun addLayer(layer: Layer) = addView(layer.view)

    override fun removeLayer(layer: Layer) = removeView(layer.view)

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val left = paddingLeft
        val top = paddingTop
        val right = width - paddingRight
        val bottom = height - paddingBottom
        stack.forEach { it.existenceLayer?.view?.layout(left, top, right, bottom) }
        transactions.forEach { it.layout(left, top, right, bottom, !isLTR) }
        shadingRect.set(paddingLeft, paddingTop, width - paddingRight, height - paddingBottom)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val width = getMaxMeasurement(widthMeasureSpec, 0)
        val height = getMaxMeasurement(heightMeasureSpec, 0)
        setMeasuredDimension(width, height)

        val childWidthMeasureSpec = makeExactlyMeasureSpec(width - horizontalPaddingSum)
        val childHeightMeasureSpec = makeExactlyMeasureSpec(height - verticalPaddingSum)
        forEachChildren { it.measure(childWidthMeasureSpec, childHeightMeasureSpec) }

    }

}