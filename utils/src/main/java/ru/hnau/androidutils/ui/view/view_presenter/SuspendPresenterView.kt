package ru.hnau.androidutils.ui.view.view_presenter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import ru.hnau.androidutils.coroutines.createUIJob
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.view_changer.ViewChanger
import ru.hnau.androidutils.ui.view.view_changer.ViewChangerInfo
import ru.hnau.androidutils.ui.view.waiter.WaiterView
import ru.hnau.androidutils.ui.view.waiter.loader.SuspendLoader
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.getter.base.get
import ru.hnau.jutils.helpers.Box
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.StateProducer
import ru.hnau.jutils.producer.StateProducerSimple
import ru.hnau.jutils.producer.extensions.observeWhen
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.jutils.producer.locked_producer.SuspendLockedProducer
import ru.hnau.jutils.tryCatch


abstract class SuspendPresenterView<T : Any>(
        context: Context,
        private val producer: Producer<GetterAsync<Unit, T>>,
        presenterViewInfo: PresenterViewInfo = PresenterViewInfo()
) : FrameLayout(
        context
) {

    companion object {

        fun <T : Any> create(
                context: Context,
                producer: Producer<GetterAsync<Unit, T>>,
                waiterViewGenerator: (LockedProducer) -> View,
                contentViewGenerator: (T) -> PresentingViewInfo,
                errorViewGenerator: (Throwable) -> PresentingViewInfo,
                isNeedUpdateDataResolver: (oldData: T, newData: T) -> Boolean = { oldData: T, newData: T -> oldData != newData },
                presenterViewInfo: PresenterViewInfo = PresenterViewInfo()
        ) = object : SuspendPresenterView<T>(context, producer, presenterViewInfo) {

            override fun generateWaiterView(lockedProducer: LockedProducer) =
                    waiterViewGenerator.invoke(lockedProducer)

            override fun generateContentView(data: T) =
                    contentViewGenerator.invoke(data)

            override fun generateErrorView(error: Throwable) =
                    errorViewGenerator.invoke(error)

            override fun isNeedUpdateData(oldData: T, newData: T) =
                    isNeedUpdateDataResolver.invoke(oldData, newData)

        }

    }

    private val presentingViewProducer = StateProducerSimple<PresentingViewInfo>()

    private val presenterView = PresenterView(
            context = context,
            presenterViewInfo = presenterViewInfo,
            presentingViewProducer = presentingViewProducer
    )

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val uiJob = createUIJob(
            isVisibleToUserProducer = isVisibleToUserProducer,
            errorsHandler = this::onNewErrorReceived
    )

    private val lockedProducer = SuspendLockedProducer()

    private var oldValue: Box<T>? = null
    private var oldError: Throwable? = null

    init {
        this.addView(presenterView)
        this.addView(generateWaiterView(lockedProducer))

        producer.observeWhen(isVisibleToUserProducer) { deferredValue ->
            uiJob {
                lockedProducer {
                    loadNewValueUnsafe(deferredValue)
                }
            }
        }
    }

    @Throws(Throwable::class)
    private suspend fun loadNewValueUnsafe(deferredValue: GetterAsync<Unit, T>) {
        val newValue = deferredValue.get()
        onNewValueReceived(newValue)
    }

    private fun onNewErrorReceived(th: Throwable) {
        oldValue = null
        if (oldError == th) {
            return
        }
        oldError = th
        updateView(generateErrorView(th))
    }

    private fun onNewValueReceived(newValue: T) {
        oldError = null
        if (oldValue?.takeIf { !isNeedUpdateData(it.value, newValue) } != null) {
            return
        }
        oldValue = Box(newValue)
        updateView(generateContentView(newValue))
    }

    private fun updateView(view: PresentingViewInfo) {
        presentingViewProducer.updateState(view)
    }

    protected abstract fun generateWaiterView(lockedProducer: LockedProducer): View

    protected abstract fun generateContentView(data: T): PresentingViewInfo

    protected abstract fun generateErrorView(error: Throwable): PresentingViewInfo

    protected open fun isNeedUpdateData(oldData: T, newData: T) =
            oldData != newData

}


fun <T : Any, G : ViewGroup> G.addSuspendLoader(
        producer: Producer<GetterAsync<Unit, T>>,
        waiterViewGenerator: (LockedProducer) -> View,
        contentViewGenerator: (T) -> PresentingViewInfo,
        errorViewGenerator: (Throwable) -> PresentingViewInfo,
        isNeedUpdateDataResolver: (oldData: T, newData: T) -> Boolean = { oldData: T, newData: T -> oldData != newData },
        presenterViewInfo: PresenterViewInfo = PresenterViewInfo(),
        viewConfigurator: (SuspendPresenterView<T>.() -> Unit)? = null
) =
        addChild(
                SuspendPresenterView.create(
                        context,
                        producer,
                        waiterViewGenerator,
                        contentViewGenerator,
                        errorViewGenerator,
                        isNeedUpdateDataResolver,
                        presenterViewInfo
                ),
                viewConfigurator
        )