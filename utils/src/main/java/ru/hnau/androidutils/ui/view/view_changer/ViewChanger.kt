package ru.hnau.androidutils.ui.view.view_changer

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Point
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import android.view.animation.Interpolator
import ru.hnau.androidutils.animations.AnimationHeaviness
import ru.hnau.androidutils.animations.Animator
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.utils.types_utils.pointInterThisAndOther
import ru.hnau.androidutils.ui.utils.types_utils.set
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.TimeValue

@Deprecated("Use PresenterView")
@SuppressLint("ViewConstructor")
open class ViewChanger(
        context: Context,
        private val info: ViewChangerInfo = ViewChangerInfo()
) : ViewGroup(context) {

    companion object {

        private val ZERO_POINT = Point()

    }

    constructor(
            context: Context,
            viewDecorationDrawer: ViewDecorationDrawer? = ViewChangerInfo.VIEW_DECORATION_DRAWER_DEFAULT,
            fromSide: Side = ViewChangerInfo.FROM_SIZE_DEFAULT,
            gravity: HGravity = ViewChangerInfo.GRAVITY_DEFAULT,
            animationTime: TimeValue = ViewChangerInfo.ANIMATION_TIME_DEFAULT,
            scrollFactor: Float = ViewChangerInfo.SCROLL_FACTOR_DEFAULT,
            showInterpolator: Interpolator = ViewChangerInfo.SHOW_INTERPOLATOR_DEFAULT,
            hideInterpolator: Interpolator = ViewChangerInfo.HIDE_INTERPOLATOR_DEFAULT,
            onTop: Boolean = ViewChangerInfo.ON_TOP_DEFAULT,
            slapToZeroForNullView: Boolean = ViewChangerInfo.SLAP_TO_ZERO_SIZE_FOR_NULL_VIEW_DEFAULT
    ) : this(
            context = context,
            info = ViewChangerInfo(
                    viewDecorationDrawer,
                    fromSide,
                    gravity,
                    animationTime,
                    scrollFactor,
                    showInterpolator,
                    hideInterpolator,
                    onTop,
                    slapToZeroForNullView
            )
    )

    private var lastViewWrapper: ViewChangedViewWrapper? = null
    private var currentViewWrapper: ViewChangedViewWrapper? = null
    private var nextViewWrapper: ViewChangedViewWrapper? = null

    private val viewIsShown: Boolean
        get() = lastViewWrapper != null || currentViewWrapper != null || nextViewWrapper != null

    private var animating = false

    private val contentRect = Rect()

    private var showingHidingPercentage = 0f
        set(value) {
            field = value
            requestLayout()
            invalidate()
        }

    private val lastMeasuredSize = Point()
    private val currentMeasuredSize = Point()
    private val measuredSize = Point()

    fun showView(
            view: View?,
            info: ViewChangerInfo = this.info.copy(
                    animationTime = getPreferredAnimationTime()
            )
    ): Boolean = synchronized(this) {

        val currentViewMover = this.currentViewWrapper
        val allowed = currentViewMover == null || view == null || currentViewMover.view != view
        if (!allowed) {
            return@synchronized false
        }

        view?.let(this::removeViewFromParent)

        nextViewWrapper = ViewChangedViewWrapper(
                view = view,
                viewChangerInfo = info,
                isRtlGetter = { !isLTR }
        )

        tryStartAnimation()
        return@synchronized true
    }

    fun showView(
            view: View?,
            viewDecorationDrawer: ViewDecorationDrawer? = info.viewDecorationDrawer,
            fromSide: Side = info.fromSide,
            gravity: HGravity = info.gravity,
            animationTime: TimeValue = getPreferredAnimationTime(),
            scrollFactor: Float = info.scrollFactor,
            showInterpolator: Interpolator = info.showInterpolator,
            hideInterpolator: Interpolator = info.hideInterpolator,
            onTop: Boolean = info.onTop,
            slapToZeroForNullView: Boolean = info.slapToZeroForNullView
    ) =
            showView(
                    view = view,
                    info = ViewChangerInfo(
                            viewDecorationDrawer,
                            fromSide,
                            gravity,
                            animationTime,
                            scrollFactor,
                            showInterpolator,
                            hideInterpolator,
                            onTop,
                            slapToZeroForNullView
                    )
            )

    fun getPreferredAnimationTime() =
            if (viewIsShown) info.animationTime else TimeValue.ZERO

    private fun removeViewFromParent(view: View) {
        val parent = view.parent ?: return
        val parentViewGroup = parent as? ViewGroup ?: return
        parentViewGroup.removeView(view)
    }

    private fun tryStartAnimation(): Unit = synchronized(this) {

        val nextViewMover = this.nextViewWrapper

        if (animating || nextViewMover == null) {
            return@synchronized
        }

        this.nextViewWrapper = null

        animating = true
        val lastViewMover = currentViewWrapper
        this.lastViewWrapper = lastViewMover
        this.currentViewWrapper = nextViewMover
        nextViewMover.addToParent(this)

        Animator.doAnimation(
                duration = nextViewMover.viewChangerInfo.animationTime,
                heaviness = AnimationHeaviness.HARD,
                onProgress = {
                    showingHidingPercentage = it
                },
                onEnd = {
                    lastViewMover?.removeFromParent(this)
                    this.lastViewWrapper = null
                    animating = false
                    tryStartAnimation()
                }
        )
    }

    override fun dispatchDraw(canvas: Canvas) {
        val currentViewMover = this.currentViewWrapper ?: return
        if (currentViewMover.viewChangerInfo.onTop) {
            drawViewMover(canvas, lastViewWrapper, false, showingHidingPercentage, false)
            drawViewMover(canvas, currentViewMover, true, showingHidingPercentage, true)
        } else {
            drawViewMover(canvas, currentViewMover, true, showingHidingPercentage, false)
            drawViewMover(canvas, lastViewWrapper, false, showingHidingPercentage, true)
        }
    }

    private fun drawViewMover(
            canvas: Canvas,
            viewWrapper: ViewChangedViewWrapper?,
            showing: Boolean,
            showingHidingPercentage: Float,
            onTop: Boolean
    ) {
        val view = viewWrapper?.view ?: return
        drawChild(canvas, view, drawingTime)
        viewWrapper.drawViewDecoration(canvas, showing, showingHidingPercentage, onTop)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val currentViewMover = this.currentViewWrapper ?: return
        getContentRect(contentRect)
        lastViewWrapper?.layout(contentRect, currentViewMover.viewChangerInfo.fromSide, false, showingHidingPercentage)
        currentViewMover.layout(contentRect, currentViewMover.viewChangerInfo.fromSide, true, showingHidingPercentage)
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) = synchronized(this) {

        val childWidthMeasureSpec = resizeMeasureSpec(widthMeasureSpec, -horizontalPaddingSum)
        val childHeightMeasureSpec = resizeMeasureSpec(heightMeasureSpec, -verticalPaddingSum)


        val lastViewWrapper = this.lastViewWrapper
        lastViewWrapper?.measure(childWidthMeasureSpec, childHeightMeasureSpec, lastMeasuredSize)

        val currentViewWrapper = currentViewWrapper
        currentViewWrapper?.measure(childWidthMeasureSpec, childHeightMeasureSpec, currentMeasuredSize)

        val slapToZeroForNullView = lastViewWrapper?.viewChangerInfo?.slapToZeroForNullView
                ?: currentViewWrapper?.viewChangerInfo?.slapToZeroForNullView ?: true

        calcMeasuredSize(
                lastMeasuredSize = lastViewWrapper?.view?.let { lastMeasuredSize },
                currentMeasuredSize = currentViewWrapper?.view?.let { currentMeasuredSize },
                measuredSize = measuredSize,
                showingHidingPercentage = showingHidingPercentage,
                slapToZeroForNullView = slapToZeroForNullView
        )

        setMeasuredDimension(
                getDefaultMeasurement(widthMeasureSpec, measuredSize.x),
                getDefaultMeasurement(heightMeasureSpec, measuredSize.y)
        )
    }

    private fun calcMeasuredSize(
            lastMeasuredSize: Point?,
            currentMeasuredSize: Point?,
            slapToZeroForNullView: Boolean,
            measuredSize: Point,
            showingHidingPercentage: Float
    ) {

        if (lastMeasuredSize != null && currentMeasuredSize != null) {
            lastMeasuredSize.pointInterThisAndOther(currentMeasuredSize, showingHidingPercentage, measuredSize)
            return
        }

        val singleSize = lastMeasuredSize ?: currentMeasuredSize
        if (singleSize == null) {
            measuredSize.set(0, 0)
            return
        }

        if (!slapToZeroForNullView) {
            measuredSize.set(singleSize)
            return
        }

        if (lastMeasuredSize == null) {
            ZERO_POINT.pointInterThisAndOther(singleSize, showingHidingPercentage, measuredSize)
            return
        }

        singleSize.pointInterThisAndOther(ZERO_POINT, showingHidingPercentage, measuredSize)

    }

}

fun <G: ViewGroup> G.addViewChanger(
        info: ViewChangerInfo = ViewChangerInfo(),
        viewConfigurator: (ViewChanger.() -> Unit)? = null
) =
        addChild(
                ViewChanger(
                        context,
                        info
                ),
                viewConfigurator
        )

fun <G: ViewGroup> G.addViewChanger(
        viewDecorationDrawer: ViewDecorationDrawer? = ViewChangerInfo.VIEW_DECORATION_DRAWER_DEFAULT,
        fromSide: Side = ViewChangerInfo.FROM_SIZE_DEFAULT,
        gravity: HGravity = ViewChangerInfo.GRAVITY_DEFAULT,
        animationTime: TimeValue = ViewChangerInfo.ANIMATION_TIME_DEFAULT,
        scrollFactor: Float = ViewChangerInfo.SCROLL_FACTOR_DEFAULT,
        showInterpolator: Interpolator = ViewChangerInfo.SHOW_INTERPOLATOR_DEFAULT,
        hideInterpolator: Interpolator = ViewChangerInfo.HIDE_INTERPOLATOR_DEFAULT,
        onTop: Boolean = ViewChangerInfo.ON_TOP_DEFAULT,
        slapToZeroForNullView: Boolean = ViewChangerInfo.SLAP_TO_ZERO_SIZE_FOR_NULL_VIEW_DEFAULT,
        viewConfigurator: (ViewChanger.() -> Unit)? = null
) =
        addChild(
                ViewChanger(
                        context,
                        viewDecorationDrawer,
                        fromSide,
                        gravity,
                        animationTime,
                        scrollFactor,
                        showInterpolator,
                        hideInterpolator,
                        onTop,
                        slapToZeroForNullView
                ),
                viewConfigurator
        )