package ru.hnau.androidutils.ui.view.header.back.button

import android.graphics.Paint
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp2


data class HeaderBackButtonInfo(
        val color: ColorGetter = ColorGetter.BLACK,
        val iconSize: DpPxGetter = dp(18),
        val iconLineWidth: DpPxGetter = dp2,
        val iconLineCap: Paint.Cap = Paint.Cap.SQUARE
) {

    companion object {

        val DEFAULT = HeaderBackButtonInfo()

    }

}