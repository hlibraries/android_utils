package ru.hnau.androidutils.ui.view.utils.apply

import android.graphics.drawable.Drawable
import android.view.View
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.view.utils.setBackgroundColor


fun <V : View> V.applyBackground(background: Drawable?) =
        apply { setBackground(background) }

fun <V : View> V.applyBackground(color: Int?) =
        apply { color?.let(this::setBackgroundColor) }

fun <V : View> V.applyBackground(background: DrawableGetter?) =
        applyBackground(background?.get(context))

fun <V : View> V.applyBackground(color: ColorGetter?) =
        applyBackground(color?.get(context))