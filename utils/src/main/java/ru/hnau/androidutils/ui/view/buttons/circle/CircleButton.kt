package ru.hnau.androidutils.ui.view.buttons.circle

import android.content.Context
import android.graphics.Canvas
import android.graphics.PointF
import android.view.MotionEvent
import android.view.View
import ru.hnau.androidutils.ui.bounds_producer.ViewBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.CircleCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.drawer.shadow.drawer.ButtonShadowDrawer
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


abstract class CircleButton(
        context: Context,
        onClick: () -> Unit,
        size: CircleButtonSize = CircleButtonSize.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT
) : View(context) {

    private val diameter = size.diameter.getPx(context)
    private val radius = diameter / 2f

    private val center = PointF()

    private val boundsProducer = ViewBoundsProducer(this)
            .applyInsents(context, shadowInfo.insets)

    private val canvasShape = CircleCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClick = onClick
    )

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            animatingViewIsVisibleToUser = isVisibleToUserProducer,
            touchHandler = touchHandler,
            rippleDrawInfo = rippleDrawInfo,
            canvasShape = canvasShape
    )

    private val shadowDrawer = ButtonShadowDrawer(
            animatingView = this,
            animatingViewIsVisibleToUser = isVisibleToUserProducer,
            touchHandler = touchHandler,
            shadowInfo = shadowInfo,
            canvasShape = canvasShape
    )

    init {
        setSoftwareRendering()
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        shadowDrawer.draw(canvas)
        rippleDrawer.draw(canvas)

        drawContent(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        touchHandler.handle(event)
        return true
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        val cx = paddingLeft + shadowDrawer.extraSize.left.getPxInt(context) + (width - horizontalPaddingSum - shadowDrawer.extraSize.horizontalSum.getPxInt(context)) / 2f
        val cy = paddingTop + shadowDrawer.extraSize.top.getPxInt(context) + (height - verticalPaddingSum - shadowDrawer.extraSize.verticalSum.getPxInt(context)) / 2f

        center.set(cx, cy)

        layoutContent(
                cx - radius,
                cy - radius,
                cx + radius,
                cy + radius
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = (diameter + shadowDrawer.extraSize.horizontalSum.getPxInt(context) + horizontalPaddingSum).toInt()
        val height = (diameter + shadowDrawer.extraSize.verticalSum.getPxInt(context) + verticalPaddingSum).toInt()
        setMeasuredDimension(
                getDefaultMeasurement(widthMeasureSpec, width),
                getDefaultMeasurement(heightMeasureSpec, height)
        )
    }

    protected abstract fun drawContent(canvas: Canvas)

    protected abstract fun layoutContent(left: Float, top: Float, right: Float, bottom: Float)

}