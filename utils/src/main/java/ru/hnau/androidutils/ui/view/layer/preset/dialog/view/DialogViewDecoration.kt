package ru.hnau.androidutils.ui.view.layer.preset.dialog.view

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View


interface DialogViewDecoration {

    fun measureDialogView(maxWidth: Int, maxHeight: Int, dialogView: View)

    fun layoutDialogView(maxWidth: Int, maxHeight: Int, dialogView: View)

    fun draw(canvas: Canvas)

}