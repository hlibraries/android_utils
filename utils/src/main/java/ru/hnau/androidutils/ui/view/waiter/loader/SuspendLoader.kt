package ru.hnau.androidutils.ui.view.waiter.loader

import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import ru.hnau.androidutils.coroutines.createUIJob
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.createIsVisibleToUserProducer
import ru.hnau.androidutils.ui.view.view_changer.ViewChanger
import ru.hnau.androidutils.ui.view.view_changer.ViewChangerInfo
import ru.hnau.androidutils.ui.view.waiter.WaiterView
import ru.hnau.jutils.coroutines.launch
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.getter.base.get
import ru.hnau.jutils.helpers.Box
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.jutils.producer.locked_producer.SuspendLockedProducer
import ru.hnau.jutils.tryCatch


@Deprecated("Use SuspendPresenterView")
abstract class SuspendLoader<T : Any>(
        context: Context,
        private val producer: Producer<GetterAsync<Unit, T>>,
        viewChangerInfo: ViewChangerInfo
) : FrameLayout(
        context
) {

    companion object {

        fun <T : Any> create(
                context: Context,
                producer: Producer<GetterAsync<Unit, T>>,
                waiterViewGenerator: (LockedProducer) -> WaiterView,
                contentViewGenerator: (T) -> View,
                errorViewGenerator: (Throwable) -> View? = { null },
                isNeedUpdateDataResolver: (oldData: T, newData: T) -> Boolean = { oldData: T, newData: T -> oldData != newData },
                info: ViewChangerInfo = ViewChangerInfo()
        ) = object : SuspendLoader<T>(context, producer, info) {

            override fun generateWaiterView(lockedProducer: LockedProducer) =
                    waiterViewGenerator.invoke(lockedProducer)

            override fun generateContentView(data: T) =
                    contentViewGenerator.invoke(data)

            override fun generateErrorView(error: Throwable) =
                    errorViewGenerator.invoke(error)

            override fun isNeedUpdateData(oldData: T, newData: T) =
                    isNeedUpdateDataResolver.invoke(oldData, newData)

        }

    }

    private val viewChanger = ViewChanger(
            context = context,
            info = viewChangerInfo
    )

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private val uiJob = createUIJob(
            isVisibleToUserProducer = isVisibleToUserProducer,
            errorsHandler = this::onNewErrorReceived
    )

    private val lockedProducer = SuspendLockedProducer()

    private var oldValue: Box<T>? = null
    private var oldError: Throwable? = null

    init {
        this.addView(viewChanger)
        this.addView(generateWaiterView(lockedProducer))

        producer.observeWhen(isVisibleToUserProducer) { deferredValue ->
            uiJob {
                lockedProducer {
                    loadNewValueUnsafe(deferredValue)
                }
            }
        }
    }

    @Throws(Throwable::class)
    private suspend fun loadNewValueUnsafe(deferredValue: GetterAsync<Unit, T>) {
        val newValue = deferredValue.get()
        onNewValueReceived(newValue)
    }

    private fun onNewErrorReceived(th: Throwable) {
        oldValue = null
        if (oldError == th) {
            return
        }
        oldError = th
        updateView(generateErrorView(th))
    }

    private fun onNewValueReceived(newValue: T) {
        oldError = null
        if (oldValue?.takeIf { !isNeedUpdateData(it.value, newValue) } != null) {
            return
        }
        oldValue = Box(newValue)
        updateView(generateContentView(newValue))
    }

    private fun updateView(view: View?) {
        viewChanger.showView(view)
    }

    protected abstract fun generateWaiterView(lockedProducer: LockedProducer): WaiterView

    protected abstract fun generateContentView(data: T): View

    protected open fun generateErrorView(error: Throwable): View? = null

    protected open fun isNeedUpdateData(oldData: T, newData: T) =
            oldData != newData

}


fun <T : Any, G : ViewGroup> G.addSuspendLoader(
        producer: Producer<GetterAsync<Unit, T>>,
        waiterViewGenerator: (LockedProducer) -> WaiterView,
        contentViewGenerator: (T) -> View,
        errorViewGenerator: (Throwable) -> View? = { null },
        isNeedUpdateDataResolver: (oldData: T, newData: T) -> Boolean = { oldData: T, newData: T -> oldData != newData },
        viewChangerInfo: ViewChangerInfo = ViewChangerInfo(),
        viewConfigurator: (SuspendLoader<T>.() -> Unit)? = null
) =
        addChild(
                SuspendLoader.create(
                        context,
                        producer,
                        waiterViewGenerator,
                        contentViewGenerator,
                        errorViewGenerator,
                        isNeedUpdateDataResolver,
                        viewChangerInfo
                ),
                viewConfigurator
        )