package ru.hnau.androidutils.ui.view.pager

import android.os.Bundle
import ru.hnau.androidutils.utils.savable.Savable
import ru.hnau.jutils.possible.Possible
import java.lang.reflect.Constructor


class PageWrapper(
        val pageInfo: PageInfo<*>
) {

    companion object {

        private const val PAGE_CLASS_NAME_SUFFIX = ".pageClass"
        private const val STATE_CLASS_NAME_SUFFIX = ".stateClass"
        private const val STATE_VALUE_SUFFIX = ".state"

        @Suppress("UNCHECKED_CAST")
        fun createFromBundle(
                bundle: Bundle,
                prefix: String
        ): PageWrapper {

            val pageClass = Class.forName(bundle.getString(prefix + PAGE_CLASS_NAME_SUFFIX)!!) as Class<Page<Savable>>

            val stateClass = Class.forName(bundle.getString(prefix + STATE_CLASS_NAME_SUFFIX)!!)!! as Class<Savable>
            val state = Savable.restoreFromBundle(bundle, prefix + STATE_VALUE_SUFFIX, stateClass)

            return PageWrapper(PageInfo(pageClass, state))
        }

    }

    private val constructor: Possible<Constructor<Page<*>>> by lazy(this::findPageConstructor)

    private var pageInner: Page<*>? = null

    fun saveToBundle(
            bundle: Bundle,
            prefix: String
    ) {
        bundle.putString(prefix + PAGE_CLASS_NAME_SUFFIX, pageInfo.pageClass.name)
        bundle.putString(prefix + STATE_CLASS_NAME_SUFFIX, pageInfo.state::class.java.name)
        pageInfo.state.saveInstanceState(bundle, prefix + STATE_VALUE_SUFFIX)
    }

    fun actualizeState(releasePage: Boolean) = synchronized(this) {
        pageInner?.actualizeState()
        if (releasePage) {
            pageInner = null
        }
    }

    fun getPage(pager: Pager): Page<*> {
        var page = pageInner
        if (page == null) {
            page = newPageInstance(pager)
            pageInner = page
        }
        return page
    }

    private fun newPageInstance(pager: Pager): Page<*> {
        val constructor = getConstructor()
        return constructor.newInstance(pager, pageInfo.state)
    }

    private fun getConstructor() = this.constructor.data
            ?: throw IllegalStateException("Page class: '${pageInfo.pageClass}' has no Constructor<Page<Pager, Savable>>")

    @Suppress("UNCHECKED_CAST")
    private fun findPageConstructor() = Possible.trySuccessCatchError {
        pageInfo.pageClass.constructors.find { constructor ->
            val params = constructor.parameterTypes as Array<Class<*>>
            params.size == 2 &&
                    Pager::class.java.isAssignableFrom(params[0]) &&
                    Savable::class.java.isAssignableFrom(params[1])

        }!! as Constructor<Page<*>>
    }


}