package ru.hnau.androidutils.ui.view.decorated_container.decorations.tabs

import android.content.Context
import android.graphics.Color
import ru.hnau.androidutils.animations.interpolateAccelerateDecelerate
import ru.hnau.androidutils.ui.utils.types_utils.ColorUtils
import ru.hnau.androidutils.ui.view.clickable.ClickableView
import ru.hnau.jutils.getFloatInterFloats
import ru.hnau.jutils.producer.Producer
import kotlin.math.abs


class TabsViewTab(
        context: Context,
        private val info: TabsViewInfo,
        position: Int,
        currentPosition: Producer<Float>,
        onClick: (Int) -> Unit
) : ClickableView(context) {

    private val activityPercentage =
            currentPosition.map { currentPosition ->
                1 - abs(currentPosition - position).coerceAtMost(1f)
            }

    private val inactiveTitleTextSize = info.inactiveInfo.titleTextSize.getPxInt(context).toFloat()
    private val activeTitleTextSize = info.inactiveInfo.titleTextSize.getPxInt(context).toFloat()
    private var titleTextSize: Float = 0f
        set(value) {
            field = value
            invalidate()
        }

    private var color: Int = Color.BLACK
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }

    init {
        initTitleTextSize()
        initColor()
    }

    private fun initColor() {
        val inactiveColor = info.inactiveInfo.color.get(context)
        val activeColor = info.activeInfo.color.get(context)
        activityPercentage
                .map { activityPercentage ->
                    ColorUtils.colorInterColors(inactiveColor, activeColor, activityPercentage)
                }
                .attach { color = it }
    }

    private fun initTitleTextSize() {
        activityPercentage
                .interpolateAccelerateDecelerate()
                .map { activityPercentage ->
                    getFloatInterFloats(inactiveTitleTextSize, activeTitleTextSize, activityPercentage)
                }
                .attach { titleTextSize = it }
    }

}