package ru.hnau.androidutils.ui.view.utils.apply.layout_params.builders

import android.view.View
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


abstract class MarginGravityLayoutParamsBuilder(
        view: View
) : MarginLayoutParamsBuilder(
        view
) {

    var gravity: HGravity = HGravity.CENTER
        private set

    fun setGravity(gravity: HGravity) {
        this.gravity = gravity
    }

    fun setStartGravity() =
            setGravity(HGravity.START_CENTER_VERTICAL)

    fun setTopGravity() =
            setGravity(HGravity.TOP_CENTER_HORIZONTAL)

    fun setEndGravity() =
            setGravity(HGravity.END_CENTER_VERTICAL)

    fun setBottomGravity() =
            setGravity(HGravity.BOTTOM_CENTER_HORIZONTAL)

    fun setStartTopGravity() =
            setGravity(HGravity.START_TOP)

    fun setStartBottomGravity() =
            setGravity(HGravity.START_BOTTOM)

    fun setEndTopGravity() =
            setGravity(HGravity.END_TOP)

    fun setEndBottomGravity() =
            setGravity(HGravity.END_BOTTOM)

    protected fun resolveGravity() =
            gravity.resolveAndroidGravity()

}