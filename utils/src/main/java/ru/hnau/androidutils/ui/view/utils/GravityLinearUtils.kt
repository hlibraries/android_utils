package ru.hnau.androidutils.ui.view.utils

import android.widget.LinearLayout
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


@Deprecated("Use LinearLayout.applyGravity(gravity: HGravity)")
fun LinearLayout.setCenterForegroundGravity() {
    gravity = HGravity.CENTER.resolveAndroidGravity()
}

@Deprecated("Use LinearLayout.applyGravity(gravity: HGravity)")
fun LinearLayout.setStartTopForegroundGravity() {
    gravity = HGravity.START_TOP.resolveAndroidGravity()
}

@Deprecated("Use LinearLayout.applyGravity(gravity: HGravity)")
fun LinearLayout.setEndTopForegroundGravity() {
    gravity = HGravity.END_TOP.resolveAndroidGravity()
}

@Deprecated("Use LinearLayout.applyGravity(gravity: HGravity)")
fun LinearLayout.setEndBottomForegroundGravity() {
    gravity = HGravity.END_BOTTOM.resolveAndroidGravity()
}

@Deprecated("Use LinearLayout.applyGravity(gravity: HGravity)")
fun LinearLayout.setStartBottomForegroundGravity() {
    gravity = HGravity.START_BOTTOM.resolveAndroidGravity()
}

@Deprecated("Use LinearLayout.applyGravity(gravity: HGravity)")
fun LinearLayout.setTopCenterHorizontalForegroundGravity() {
    gravity = HGravity.TOP_CENTER_HORIZONTAL.resolveAndroidGravity()
}

@Deprecated("Use LinearLayout.applyGravity(gravity: HGravity)")
fun LinearLayout.setBottomCenterHorizontalForegroundGravity() {
    gravity = HGravity.BOTTOM_CENTER_HORIZONTAL.resolveAndroidGravity()
}

@Deprecated("Use LinearLayout.applyGravity(gravity: HGravity)")
fun LinearLayout.setStartCenterVerticalForegroundGravity() {
    gravity = HGravity.START_CENTER_VERTICAL.resolveAndroidGravity()
}

@Deprecated("Use LinearLayout.applyGravity(gravity: HGravity)")
fun LinearLayout.setEndCenterVerticalForegroundGravity() {
    gravity = HGravity.END_CENTER_VERTICAL.resolveAndroidGravity()
}