package ru.hnau.androidutils.ui.view.utils.apply.layout_params.builders

import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.view.utils.handleLayoutDirection
import ru.hnau.androidutils.ui.view.utils.isLTR


open class MarginLayoutParamsBuilder(
        view: View
) : LayoutParamsBuilder(
        view
) {

    var marginStart: Int = 0
        private set

    var marginTop: Int = 0
        private set

    var marginEnd: Int = 0
        private set

    var marginBottom: Int = 0
        private set

    fun setMargins(start: Int, top: Int, end: Int, bottom: Int) {
        marginStart = start
        marginTop = top
        marginEnd = end
        marginBottom = bottom
    }

    fun setMargins(start: DpPxGetter, top: DpPxGetter, end: DpPxGetter, bottom: DpPxGetter) =
            setMargins(start.getPxInt(context), top.getPxInt(context), end.getPxInt(context), bottom.getPxInt(context))

    fun setMargins(start: Float, top: Float, end: Float, bottom: Float) =
            setMargins(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())

    fun setMargins(start: Short, top: Short, end: Short, bottom: Short) =
            setMargins(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())

    fun setMargins(start: Double, top: Double, end: Double, bottom: Double) =
            setMargins(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())

    fun setMargins(start: Long, top: Long, end: Long, bottom: Long) =
            setMargins(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())

    fun setMargins(start: Byte, top: Byte, end: Byte, bottom: Byte) =
            setMargins(start.toInt(), top.toInt(), end.toInt(), bottom.toInt())

    fun setMargins(horizontal: Int, vertical: Int) =
            setMargins(horizontal, vertical, horizontal, vertical)

    fun setMargins(horizontal: DpPxGetter, vertical: DpPxGetter) =
            setMargins(horizontal.getPxInt(context), vertical.getPxInt(context))

    fun setMargins(horizontal: Float, vertical: Float) =
            setMargins(horizontal.toInt(), vertical.toInt())

    fun setMargins(horizontal: Short, vertical: Short) =
            setMargins(horizontal.toInt(), vertical.toInt())

    fun setMargins(horizontal: Long, vertical: Long) =
            setMargins(horizontal.toInt(), vertical.toInt())

    fun setMargins(horizontal: Double, vertical: Double) =
            setMargins(horizontal.toInt(), vertical.toInt())

    fun setMargins(horizontal: Byte, vertical: Byte) =
            setMargins(horizontal.toInt(), vertical.toInt())

    fun setMargins(all: Int) =
            setMargins(all, all)

    fun setMargins(all: DpPxGetter) =
            setMargins(all.getPxInt(context))

    fun setMargins(all: Float) =
            setMargins(all.toInt())

    fun setMargins(all: Short) =
            setMargins(all.toInt())

    fun setMargins(all: Long) =
            setMargins(all.toInt())

    fun setMargins(all: Double) =
            setMargins(all.toInt())

    fun setMargins(all: Byte) =
            setMargins(all.toInt())

    fun setHorizontalMargins(horizontal: Int) {
        marginStart = horizontal
        marginEnd = horizontal
    }

    fun setHorizontalMargins(horizontal: DpPxGetter) =
            setHorizontalMargins(horizontal.getPxInt(context))

    fun setHorizontalMargins(horizontal: Float) =
            setHorizontalMargins(horizontal.toInt())

    fun setHorizontalMargins(horizontal: Short) =
            setHorizontalMargins(horizontal.toInt())

    fun setHorizontalMargins(horizontal: Long) =
            setHorizontalMargins(horizontal.toInt())

    fun setHorizontalMargins(horizontal: Double) =
            setHorizontalMargins(horizontal.toInt())

    fun setHorizontalMargins(horizontal: Byte) =
            setHorizontalMargins(horizontal.toInt())

    fun setVerticalMargins(vertical: Int) {
        marginTop = vertical
        marginBottom = vertical
    }

    fun setVerticalMargins(vertical: DpPxGetter) =
            setVerticalMargins(vertical.getPxInt(context))

    fun setVerticalMargins(vertical: Float) =
            setVerticalMargins(vertical.toInt())

    fun setVerticalMargins(vertical: Short) =
            setVerticalMargins(vertical.toInt())

    fun setVerticalMargins(vertical: Long) =
            setVerticalMargins(vertical.toInt())

    fun setVerticalMargins(vertical: Double) =
            setVerticalMargins(vertical.toInt())

    fun setVerticalMargins(vertical: Byte) =
            setVerticalMargins(vertical.toInt())

    fun setStartMargin(start: Int) {
        marginStart = start
    }

    fun setStartMargin(start: DpPxGetter) =
            setStartMargin(start.getPxInt(context))

    fun setStartMargin(start: Float) =
            setStartMargin(start.toInt())

    fun setStartMargin(start: Short) =
            setStartMargin(start.toInt())

    fun setStartMargin(start: Long) =
            setStartMargin(start.toInt())

    fun setStartMargin(start: Double) =
            setStartMargin(start.toInt())

    fun setStartMargin(start: Byte) =
            setStartMargin(start.toInt())

    fun setTopMargin(top: Int) {
        marginTop = top
    }

    fun setTopMargin(top: DpPxGetter) =
            setTopMargin(top.getPxInt(context))

    fun setTopMargin(top: Float) =
            setTopMargin(top.toInt())

    fun setTopMargin(top: Short) =
            setTopMargin(top.toInt())

    fun setTopMargin(top: Long) =
            setTopMargin(top.toInt())

    fun setTopMargin(top: Double) =
            setTopMargin(top.toInt())

    fun setTopMargin(top: Byte) =
            setTopMargin(top.toInt())

    fun setEndMargin(end: Int) {
        marginEnd = end
    }

    fun setEndMargin(end: DpPxGetter) =
            setEndMargin(end.getPxInt(context))

    fun setEndMargin(end: Float) =
            setEndMargin(end.toInt())

    fun setEndMargin(end: Short) =
            setEndMargin(end.toInt())

    fun setEndMargin(end: Long) =
            setEndMargin(end.toInt())

    fun setEndMargin(end: Double) =
            setEndMargin(end.toInt())

    fun setEndMargin(end: Byte) =
            setEndMargin(end.toInt())

    fun setBottomMargin(bottom: Int) {
        marginBottom = bottom
    }

    fun setBottomMargin(bottom: DpPxGetter) =
            setBottomMargin(bottom.getPxInt(context))

    fun setBottomMargin(bottom: Float) =
            setBottomMargin(bottom.toInt())

    fun setBottomMargin(bottom: Short) =
            setBottomMargin(bottom.toInt())

    fun setBottomMargin(bottom: Long) =
            setBottomMargin(bottom.toInt())

    fun setBottomMargin(bottom: Double) =
            setBottomMargin(bottom.toInt())

    fun setBottomMargin(bottom: Byte) =
            setBottomMargin(bottom.toInt())

    protected fun ViewGroup.MarginLayoutParams.applyMargins() = apply {
        val marginLeft = handleLayoutDirection(
                forLTR = this@MarginLayoutParamsBuilder.marginStart,
                forRTL = this@MarginLayoutParamsBuilder.marginEnd
        )
        val marginRight = handleLayoutDirection(
                forLTR = this@MarginLayoutParamsBuilder.marginEnd,
                forRTL = this@MarginLayoutParamsBuilder.marginStart
        )
        setMargins(marginLeft, marginTop, marginRight, marginBottom)
    }

    override fun build() =
            ViewGroup.MarginLayoutParams(width, height).applyMargins()

}