package ru.hnau.androidutils.ui.view.layer.transaction

import android.graphics.Point
import android.view.animation.Interpolator
import ru.hnau.androidutils.ui.view.layer.layer.LayerWrapper
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.getIntInterInts


abstract class BaseLayerTransaction(
        val layerWrapper: LayerWrapper,
        protected val info: TransactionInfo,
        private val onFinished: (BaseLayerTransaction) -> Unit
) {

    protected abstract val positive: Boolean

    private var started: TimeValue? = null

    private val fromOffset = Point()
    private val toOffset = Point()
    private val offset = Point()

    protected abstract val interpolator: Interpolator

    protected open fun beforeStart() {}

    protected open fun afterFinish() {}

    private var finished = false

    private val animationPercentage: Float
        get() {
            val started = this.started ?: return 0f
            return calculateAnimationPercentage(started).coerceAtMost(1f)
        }

    val visibility: Float
        get() = if (positive) animationPercentage else 1 - animationPercentage

    fun start() {
        synchronized(this) {
            if (started != null) {
                return
            }
            started = TimeValue.now()
        }
        beforeStart()
        layerWrapper.get().view.bringToFront()
    }

    private fun calculateAnimationPercentage(started: TimeValue) =
            (System.currentTimeMillis() - started.milliseconds).toFloat() / info.duration.milliseconds.toFloat()

    fun layout(left: Int, top: Int, right: Int, bottom: Int, rtl: Boolean) {

        if (finished) {
            return
        }

        val animationPercentage = this.animationPercentage
        finished = animationPercentage >= 1

        calculateOffset(right - left, bottom - top, animationPercentage, rtl, offset)
        layerWrapper.get().view.layout(
                left + offset.x,
                top + offset.y,
                right + offset.x,
                bottom + offset.y
        )


        if (finished) {
            onFinished.invoke(this)
            afterFinish()
        }

    }

    private fun calculateOffset(
            width: Int, height: Int,
            animationPercentage: Float,
            rtl: Boolean,
            result: Point
    ) {
        calculateFromAndToOffsets(width, height, rtl, fromOffset, toOffset)
        val offsetPercentage = interpolator.getInterpolation(animationPercentage)
        result.set(
                getIntInterInts(fromOffset.x, toOffset.x, offsetPercentage),
                getIntInterInts(fromOffset.y, toOffset.y, offsetPercentage)
        )
    }

    protected abstract fun calculateFromAndToOffsets(
            width: Int, height: Int,
            rtl: Boolean,
            fromOffset: Point, toOffset: Point
    )

    fun invalidateLayer() = layerWrapper.invalidateLayerIfExists()

}