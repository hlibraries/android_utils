package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.item

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.clickable.ClickableLabel
import ru.hnau.androidutils.ui.view.utils.apply.applyHorizontalPadding
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement
import ru.hnau.androidutils.ui.view.utils.makeExactlyMeasureSpec
import ru.hnau.androidutils.ui.view.utils.setHorizontalPadding


@SuppressLint("ViewConstructor")
class BottomSheetItem(
        context: Context,
        text: StringGetter,
        onClick: () -> Unit,
        private val info: BottomSheetItemInfo = BottomSheetItemInfo.DEFAULT
) : ClickableLabel(
        context = context,
        info = info.labelInfo,
        initialText = text,
        onClick = onClick,
        rippleDrawInfo = info.rippleDrawInfo
) {

    init {
        applyHorizontalPadding(info.paddingHorizontal)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getMaxMeasurement(widthMeasureSpec, 0)
        val height = getDefaultMeasurement(heightMeasureSpec, info.height.getPxInt(context))
        super.onMeasure(
                makeExactlyMeasureSpec(width),
                makeExactlyMeasureSpec(height)
        )
    }

}