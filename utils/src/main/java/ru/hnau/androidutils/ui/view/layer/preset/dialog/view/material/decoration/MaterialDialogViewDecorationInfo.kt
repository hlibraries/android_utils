package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.decoration

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp32
import ru.hnau.androidutils.context_getters.dp_px.dp4
import ru.hnau.androidutils.context_getters.dp_px.dp48
import ru.hnau.androidutils.ui.drawer.shadow.info.ShadowInfo


class MaterialDialogViewDecorationInfo(
        val paddingHorizontal: DpPxGetter = dp32,
        val paddingVertical: DpPxGetter = dp32,
        val shadow: ShadowInfo = ShadowInfo(
                offset = dp32,
                blur = dp48,
                color = ColorGetter.BLACK,
                alpha = 0.5f
        ),
        val borderRadius: DpPxGetter = dp4,
        val background: ColorGetter = ColorGetter.WHITE
) {

    companion object {

        val DEFAULT = MaterialDialogViewDecorationInfo()

    }

}