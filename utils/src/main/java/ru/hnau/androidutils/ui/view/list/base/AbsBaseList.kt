package ru.hnau.androidutils.ui.view.list.base

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.ViewGroup


@SuppressLint("ViewConstructor")
abstract class AbsBaseList<T>(
        context: Context
) : RecyclerView(context) {

    private val itemTouchHelperCallback = BaseListItemTouchHelperCallback(
            onItemsMoved = this::onItemsChanged,
            onItemSwiped = this::onItemSwiped
    )

    private val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback).apply {
        attachToRecyclerView(this@AbsBaseList)
    }

    private val baseListAdapter = object : RecyclerView.Adapter<BaseListViewHolder<T>>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
                BaseListViewHolder(
                        vertical = vertical,
                        viewWrapper = createNewWrapper(viewType),
                        startDragging = itemTouchHelper::startDrag,
                        startSwiping = itemTouchHelper::startSwipe
                )

        override fun getItemCount() = this@AbsBaseList.getItemsCount()

        override fun onBindViewHolder(holder: BaseListViewHolder<T>, position: Int) =
                holder.setContent(getItem(position), position)

        override fun getItemViewType(position: Int) = this@AbsBaseList.resolveItemType(position)

    }

    protected abstract val vertical: Boolean

    init {
        adapter = baseListAdapter
    }

    protected abstract fun getItemsCount(): Int

    protected abstract fun getItem(position: Int): T

    protected abstract fun createNewWrapper(itemType: Int): BaseListViewWrapper<T>

    protected open fun resolveItemType(position: Int) = 0

    protected open fun onItemsChanged(first: BaseListViewHolder<T>, second: BaseListViewHolder<T>) = false

    protected open fun onItemSwiped(viewHolder: BaseListViewHolder<T>, direction: SwipeOrDragDirection) {}

}