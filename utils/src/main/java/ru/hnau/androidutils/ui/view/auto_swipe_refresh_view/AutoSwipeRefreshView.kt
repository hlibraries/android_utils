package ru.hnau.androidutils.ui.view.auto_swipe_refresh_view

import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.view.utils.apply.addChild


class AutoSwipeRefreshView(
        content: View,
        color: ColorGetter,
        private val updateContent: () -> Unit
) : AbstractAutoSwipeRefreshView(
        content.context,
        color
) {

    init {
        addView(content)
    }

    override fun updateContent() = updateContent.invoke()

}

fun <G: ViewGroup> G.addAutoSwipeRefreshView(
        color: ColorGetter,
        updateContent: () -> Unit,
        viewConfigurator: (AbstractAutoSwipeRefreshView.() -> Unit)? = null
) =
        addChild(
                object : AbstractAutoSwipeRefreshView(context, color) {
                    override fun updateContent() = updateContent.invoke()
                },
                viewConfigurator
        )