package ru.hnau.androidutils.ui.view.list.base

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp0
import ru.hnau.androidutils.context_getters.dp_px.dp1
import ru.hnau.androidutils.ui.view.utils.forEachChildren
import ru.hnau.androidutils.ui.view.utils.isVertical
import ru.hnau.androidutils.ui.view.utils.linearLayoutManager


class BaseListItemsDivider(
        context: Context,
        size: DpPxGetter = dp1,
        paddingStart: DpPxGetter = dp0,
        paddingEnd: DpPxGetter = dp0,
        color: ColorGetter? = null
) : RecyclerView.ItemDecoration() {

    private val size = size.getPxInt(context)

    private val paddingStart = paddingStart.getPxInt(context)

    private val paddingEnd = paddingEnd.getPxInt(context)

    private val paint = color?.let {
        Paint().apply {
            this.color = color.get(context)
        }
    }

    private val drawRect = Rect()

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(canvas, parent, state)

        paint ?: return

        parent.forEachChildren {
            if (parent.linearLayoutManager.isVertical) {
                updateDrawRectAsVertical(parent, it)
            } else {
                updateDrawRectAsHorizontal(parent, it)
            }
            canvas.drawRect(drawRect, paint)
        }
    }

    private fun updateDrawRectAsHorizontal(parent: RecyclerView, view: View) {
        val left = view.right
        val right = left + size
        val top = parent.paddingTop + paddingStart
        val bottom = parent.height - parent.paddingBottom - paddingEnd
        drawRect.set(left, top, right, bottom)
    }

    private fun updateDrawRectAsVertical(parent: RecyclerView, view: View) {
        val top = view.bottom
        val bottom = top + size
        val left = parent.paddingLeft + paddingStart
        val right = parent.width - parent.paddingRight - paddingEnd
        drawRect.set(left, top, right, bottom)
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        val last = parent.getChildAdapterPosition(view) >= (parent.adapter?.itemCount ?: 0) - 1

        val isVertical = parent.linearLayoutManager.isVertical
        val right = if (isVertical || last) 0 else size
        val bottom = if (!isVertical || last) 0 else size
        outRect.set(0, 0, right, bottom)
    }

}