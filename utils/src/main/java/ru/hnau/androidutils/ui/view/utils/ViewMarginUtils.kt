package ru.hnau.androidutils.ui.view.utils

import android.os.Build
import android.view.View
import android.view.ViewGroup

private val View.marginLayoutParams: ViewGroup.MarginLayoutParams?
    get() = layoutParams as? ViewGroup.MarginLayoutParams

val View.startMargin: Int
    get() = marginLayoutParams
            ?.let { if (!isLTR) it.rightMargin else it.leftMargin } ?: 0

val View.topMargin: Int
    get() = marginLayoutParams?.topMargin ?: 0

val View.endMargin: Int
    get() = marginLayoutParams
            ?.let { if (!isLTR) it.leftMargin else it.rightMargin } ?: 0

val View.bottomMargin: Int
    get() = marginLayoutParams?.bottomMargin ?: 0