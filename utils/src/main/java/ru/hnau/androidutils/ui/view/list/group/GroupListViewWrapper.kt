package ru.hnau.androidutils.ui.view.list.group

import android.view.View
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper


class GroupListViewWrapper<G, T, GT>(
        private val viewWrapper: BaseListViewWrapper<GT>
) : BaseListViewWrapper<GroupListItem<G, T>> {

    companion object {

        fun <G, T> create(
                isGroup: Boolean,
                groupsViewWrappersCreator: () -> BaseListViewWrapper<G>,
                itemsViewWrappersCreator: () -> BaseListViewWrapper<T>
        ) =
                if (isGroup) {
                    GroupListViewWrapper<G, T, G>(groupsViewWrappersCreator.invoke())
                } else {
                    GroupListViewWrapper<G, T, T>(itemsViewWrappersCreator.invoke())
                }

    }

    override val view: View
        get() = viewWrapper.view

    @Suppress("UNCHECKED_CAST")
    override fun setContent(content: GroupListItem<G, T>, position: Int) =
            viewWrapper.setContent(content.groupOrItem as GT, position)

}