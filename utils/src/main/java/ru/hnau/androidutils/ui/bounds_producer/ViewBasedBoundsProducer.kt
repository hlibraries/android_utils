package ru.hnau.androidutils.ui.bounds_producer

import android.graphics.RectF
import android.view.View


abstract class ViewBasedBoundsProducer(
        view: View
) : BoundsProducer() {

    init {
        view.addOnLayoutChangeListener { boundsView, _, _, _, _, _, _, _, _ ->
            onLayout(boundsView)
        }
    }

    private fun onLayout(view: View) =
            editBounds { calculateBounds(view, this) }

    protected abstract fun calculateBounds(view: View, bounds: RectF)

}