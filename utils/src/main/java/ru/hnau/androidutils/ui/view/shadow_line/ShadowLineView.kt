package ru.hnau.androidutils.ui.view.shadow_line

import android.content.Context
import android.graphics.Canvas
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.animations.smooth
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.drawables.ShadowLineDrawable
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen
import ru.hnau.jutils.producer.extensions.toFloat
import ru.hnau.jutils.producer.extensions.toProducer


class ShadowLineView(
        context: Context,
        private val fromSide: Side = DEFAULT_FROM_SIDE,
        private val info: ShadowLineViewInfo = ShadowLineViewInfo.DEFAULT,
        isVisibleProducer: Producer<Boolean> = true.toProducer()
) : View(
        context
) {

    companion object {

        val DEFAULT_FROM_SIDE = Side.TOP

    }

    private val isVisibleToUserProducer = ViewIsVisibleToUserProducer(this)

    private val drawable = ShadowLineDrawable(context, fromSide, info.color)

    private val preferredSize = info.preferredSize.getPxInt(context)


    init {
        isVisibleProducer
                .observeWhen(isVisibleToUserProducer)
                .toFloat()
                .smooth()
                .attach(this::onVisibilityPercentageChanged)
    }

    private fun onVisibilityPercentageChanged(visibilityPercentage: Float) {
        drawable.alpha = (visibilityPercentage * info.maxAlpha).toInt()
        invalidate()
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        drawable.draw(canvas)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        drawable.setBounds(
                paddingLeft,
                paddingTop,
                width - paddingRight,
                height - paddingBottom
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val width = fromSide.horizontal.handle(
                onTrue = { getDefaultMeasurement(widthMeasureSpec, preferredSize + horizontalPaddingSum) },
                onFalse = { getMaxMeasurement(widthMeasureSpec, 0) }
        )

        val height = fromSide.horizontal.handle(
                onTrue = { getMaxMeasurement(heightMeasureSpec, 0) },
                onFalse = { getDefaultMeasurement(heightMeasureSpec, preferredSize + verticalPaddingSum) }
        )

        setMeasuredDimension(width, height)
    }

}

fun <G : ViewGroup> G.addShadowLine(
        info: ShadowLineViewInfo = ShadowLineViewInfo.DEFAULT,
        fromSide: Side = ShadowLineView.DEFAULT_FROM_SIDE,
        isVisibleProducer: Producer<Boolean> = true.toProducer(),
        viewConfigurator: (ShadowLineView.() -> Unit)? = null
) =
        addChild(
                ShadowLineView(context, fromSide, info, isVisibleProducer),
                viewConfigurator
        )

fun <G : ViewGroup> G.addStartShadowLine(
        info: ShadowLineViewInfo = ShadowLineViewInfo.DEFAULT,
        isVisibleProducer: Producer<Boolean> = true.toProducer(),
        viewConfigurator: (ShadowLineView.() -> Unit)? = null
) =
        addShadowLine(
                info = info,
                fromSide = Side.START,
                isVisibleProducer = isVisibleProducer,
                viewConfigurator = viewConfigurator
        )

fun <G : ViewGroup> G.addTopShadowLine(
        info: ShadowLineViewInfo = ShadowLineViewInfo.DEFAULT,
        isVisibleProducer: Producer<Boolean> = true.toProducer(),
        viewConfigurator: (ShadowLineView.() -> Unit)? = null
) =
        addShadowLine(
                info = info,
                fromSide = Side.TOP,
                isVisibleProducer = isVisibleProducer,
                viewConfigurator = viewConfigurator
        )

fun <G : ViewGroup> G.addEndShadowLine(
        info: ShadowLineViewInfo = ShadowLineViewInfo.DEFAULT,
        isVisibleProducer: Producer<Boolean> = true.toProducer(),
        viewConfigurator: (ShadowLineView.() -> Unit)? = null
) =
        addShadowLine(
                info = info,
                fromSide = Side.END,
                isVisibleProducer = isVisibleProducer,
                viewConfigurator = viewConfigurator
        )

fun <G : ViewGroup> G.addBottomShadowLine(
        info: ShadowLineViewInfo = ShadowLineViewInfo.DEFAULT,
        isVisibleProducer: Producer<Boolean> = true.toProducer(),
        viewConfigurator: (ShadowLineView.() -> Unit)? = null
) =
        addShadowLine(
                info = info,
                fromSide = Side.BOTTOM,
                isVisibleProducer = isVisibleProducer,
                viewConfigurator = viewConfigurator
        )