package ru.hnau.androidutils.ui.view.list.base

import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import ru.hnau.jutils.helpers.Box


class BaseListViewHolder<T>(
        vertical: Boolean,
        val viewWrapper: BaseListViewWrapper<T>,
        private val startDragging: (BaseListViewHolder<T>) -> Unit,
        private val startSwiping: (BaseListViewHolder<T>) -> Unit
) : RecyclerView.ViewHolder(viewWrapper.view) {

    companion object {
        private const val WRAP_CONTENT = RecyclerView.LayoutParams.WRAP_CONTENT
        private const val MATCH_PARENT = RecyclerView.LayoutParams.MATCH_PARENT
    }

    lateinit var content: Box<T>
        private set

    init {
        viewWrapper.setViewHolder(this)
        viewWrapper.view.layoutParams = RecyclerView.LayoutParams(
                if (vertical) MATCH_PARENT else WRAP_CONTENT,
                if (vertical) WRAP_CONTENT else MATCH_PARENT
        )
    }

    fun setContent(content: T, position: Int) {
        this.content = Box(content)
        viewWrapper.setContent(content, position)
    }

    fun startDragging() = startDragging.invoke(this)
    fun startSwiping() = startSwiping.invoke(this)

    fun handleDrawContentViewForSwiping(c: Canvas, dX: Float, dY: Float) =
            viewWrapper.handleDrawContentViewForSwiping(c, dX, dY)

    fun handleDrawContentViewForDragging(c: Canvas, dX: Float, dY: Float) =
            viewWrapper.handleDrawContentViewForDragging(c, dX, dY)

    fun onSelectedForSwiping() = viewWrapper.onSelectedForSwiping()
    fun onSelectedForDragging() = viewWrapper.onSelectedForDragging()
    fun onUnselected() = viewWrapper.onUnselected()

}