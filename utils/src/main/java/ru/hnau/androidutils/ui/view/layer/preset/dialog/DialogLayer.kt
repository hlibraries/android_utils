package ru.hnau.androidutils.ui.view.layer.preset.dialog

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogViewBuilder
import ru.hnau.androidutils.ui.view.layer.layer.Layer
import ru.hnau.androidutils.ui.view.layer.layer.LayerState
import ru.hnau.androidutils.ui.view.layer.layer.ManagerConnector
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogViewDecoration
import ru.hnau.androidutils.ui.view.utils.*


class DialogLayer(context: Context) : ViewGroup(context), Layer {

    companion object {

        fun <V : DialogView, I> create(
                context: Context,
                dialogViewBuilder: DialogViewBuilder<V, I>
        ) =
                DialogLayer(context).apply {
                    this.dialogViewBuilder = dialogViewBuilder
                }

    }

    override val view = this

    override val transparent = true

    @ManagerConnector
    private lateinit var managerConnector: LayerManagerConnector

    @LayerState
    private lateinit var dialogViewBuilder: DialogViewBuilder<*, *>

    private val dialogView: DialogView by lazy { dialogViewBuilder.build(managerConnector) }

    private val contentView: View by lazy {
        dialogView.build().apply {
            setOnClickListener { }
        }
    }

    private val dialogViewDecoration: DialogViewDecoration
        get() = dialogView.decoration

    private val dialogViewDecoratorView = DialogLayerDecorationView(context) { dialogViewDecoration }

    init {
        addView(dialogViewDecoratorView)
        setOnClickListener { managerConnector.handleGoBack() }
    }

    override fun afterCreate() {
        super.afterCreate()
        addView(contentView)
    }

    override fun handleGoBack() =
            dialogView.handleGoBack()

    override fun beforeDestroy() {
        super.beforeDestroy()
        dialogView.onClosed()
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        dialogViewDecoration.layoutDialogView(width, height, contentView)
        dialogViewDecoratorView.layout(0, 0, width, height)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val width = getMaxMeasurement(widthMeasureSpec, 0)
        val height = getMaxMeasurement(heightMeasureSpec, 0)
        setMeasuredDimension(width, height)

        dialogViewDecoratorView.measure(
                makeExactlyMeasureSpec(width),
                makeExactlyMeasureSpec(height)
        )

        dialogViewDecoration.measureDialogView(width, height, contentView)

    }

}