package ru.hnau.androidutils.ui.utils.h_gravity


enum class HGravityOrientationValue {
    START,
    CENTER,
    END;

    val opposite: HGravityOrientationValue by lazy {
        when (this) {
            START -> END
            CENTER -> CENTER
            END -> START
        }
    }

}

fun HGravityOrientationValue?.isCenter() =
        this == null || this == HGravityOrientationValue.CENTER