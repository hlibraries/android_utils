package ru.hnau.androidutils.ui.view.utils.apply

import android.support.design.widget.CoordinatorLayout
import android.support.v4.widget.NestedScrollView
import android.view.View
import android.view.ViewGroup
import android.widget.*


fun <G : ViewGroup, V : View> G.addChild(child: V?, viewConfigurator: (V.() -> Unit)? = null) =
        child?.let { view ->
            apply {
                viewConfigurator?.invoke(view)
                addView(view)
            }
        } ?: this

@Deprecated("Use addChild")
fun <G : ViewGroup, V : View> G.addView(view: V, viewConfigurator: (V.() -> Unit)? = null) =
        addChild(view, viewConfigurator)

fun <G : ViewGroup> G.addView(viewConfigurator: (View.() -> Unit)? = null) =
        addChild(View(context), viewConfigurator)

fun <G : ViewGroup> G.addImageView(viewConfigurator: (ImageView.() -> Unit)? = null) =
        addChild(ImageView(context), viewConfigurator)

fun <G : ViewGroup> G.addEditText(viewConfigurator: (EditText.() -> Unit)? = null) =
        addChild(EditText(context), viewConfigurator)

fun <G : ViewGroup> G.addLinearLayout(orientation: Int, viewConfigurator: (LinearLayout.() -> Unit)? = null) =
        addChild(LinearLayout(context).apply { this.orientation = orientation }, viewConfigurator)

fun <G : ViewGroup> G.addVerticalLayout(viewConfigurator: (LinearLayout.() -> Unit)? = null) =
        addLinearLayout(LinearLayout.VERTICAL, viewConfigurator)

fun <G : ViewGroup> G.addHorizontalLayout(viewConfigurator: (LinearLayout.() -> Unit)? = null) =
        addLinearLayout(LinearLayout.HORIZONTAL, viewConfigurator)

fun <G : ViewGroup> G.addFrameLayout(viewConfigurator: (FrameLayout.() -> Unit)? = null) =
        addChild(FrameLayout(context), viewConfigurator)

fun <G : ViewGroup> G.addRelativeLayout(viewConfigurator: (RelativeLayout.() -> Unit)? = null) =
        addChild(RelativeLayout(context), viewConfigurator)

fun <G : ViewGroup> G.addScrollView(viewConfigurator: (ScrollView.() -> Unit)? = null) =
        addChild(ScrollView(context), viewConfigurator)

fun <G : ViewGroup> G.addHorizontalScrollView(viewConfigurator: (HorizontalScrollView.() -> Unit)? = null) =
        addChild(HorizontalScrollView(context), viewConfigurator)

fun <G : ViewGroup> G.addNestedScrollView(viewConfigurator: (NestedScrollView.() -> Unit)? = null) =
        addChild(NestedScrollView(context), viewConfigurator)

fun <G : ViewGroup> G.addCoordinatorLayout(viewConfigurator: (CoordinatorLayout.() -> Unit)? = null) =
        addChild(CoordinatorLayout(context), viewConfigurator)