package ru.hnau.androidutils.ui.view.utils.apply

import android.widget.LinearLayout
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


fun <L: LinearLayout> L.applyGravity(gravity: HGravity) =
        apply { this.gravity = gravity.resolveAndroidGravity() }

fun <L: LinearLayout> L.applyCenterGravity() =
        applyGravity(HGravity.CENTER)

fun <L: LinearLayout> L.applyStartGravity() =
        applyGravity(HGravity.START_CENTER_VERTICAL)

fun <L: LinearLayout> L.applyTopGravity() =
        applyGravity(HGravity.TOP_CENTER_HORIZONTAL)

fun <L: LinearLayout> L.applyEndGravity() =
        applyGravity(HGravity.END_CENTER_VERTICAL)

fun <L: LinearLayout> L.applyBottomGravity() =
        applyGravity(HGravity.BOTTOM_CENTER_HORIZONTAL)

fun <L: LinearLayout> L.applyStartTopGravity() =
        applyGravity(HGravity.START_TOP)

fun <L: LinearLayout> L.applyStartBottomGravity() =
        applyGravity(HGravity.START_BOTTOM)

fun <L: LinearLayout> L.applyEndTopGravity() =
        applyGravity(HGravity.END_TOP)

fun <L: LinearLayout> L.applyEndBottomGravity() =
        applyGravity(HGravity.END_BOTTOM)