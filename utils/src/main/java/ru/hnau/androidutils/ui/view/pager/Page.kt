package ru.hnau.androidutils.ui.view.pager

import android.view.View
import ru.hnau.androidutils.go_back_handler.GoBackHandler
import ru.hnau.androidutils.utils.savable.Savable


interface Page<S : Savable> : PageInfoContainer<S>, GoBackHandler {

    val state: S

    val view: View

    override fun handleGoBack(): Boolean = false

    fun actualizeState()

    override val pageInfo: PageInfo<S>
        get() = PageInfo(
                pageClass = this::class.java,
                state = state
        )

}