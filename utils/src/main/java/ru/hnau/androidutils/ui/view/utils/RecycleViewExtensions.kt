package ru.hnau.androidutils.ui.view.utils

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import java.lang.Exception


val RecyclerView.verticalLinearLayoutManager: LinearLayoutManager?
    get() = linearLayoutManager.takeIf { it.orientation == LinearLayoutManager.VERTICAL }

val RecyclerView.horizontalLinearLayoutManager: LinearLayoutManager?
    get() = linearLayoutManager.takeIf { it.orientation == LinearLayoutManager.HORIZONTAL }

val RecyclerView.linearLayoutManager: LinearLayoutManager
    get() = this.layoutManager as? LinearLayoutManager
            ?: throw Exception("layoutManager is not LinearLayoutManager")

val LinearLayoutManager.isHorizontal: Boolean
    get() = orientation == LinearLayoutManager.HORIZONTAL

val LinearLayoutManager.isVertical: Boolean
    get() = orientation == LinearLayoutManager.VERTICAL