package ru.hnau.androidutils.ui.view.decorated_container

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import ru.hnau.androidutils.go_back_handler.GoBackHandler
import ru.hnau.androidutils.ui.drawer.Insets
import ru.hnau.androidutils.ui.drawer.max
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.utils.h_gravity.toHGravity
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyFrameParams


class DecoratedContainer(
        context: Context,
        private val decorations: Iterable<DecoratedContainerDecoration>
) : FrameLayout(
        context
), GoBackHandler {

    companion object {

        fun create(
                context: Context,
                decorations: Iterable<DecoratedContainerDecoration>,
                contentBuilder: (Insets) -> View?
        ) = DecoratedContainer(
                context = context,
                decorations = decorations
        ).apply {
            content = contentBuilder(insets)
        }

    }

    val insets = decorations
            .fold(Insets.EMPTY) { insets, decoration ->
                val decorationInsets = when (decoration.side) {
                    Side.START -> Insets(start = decoration.preferredSize)
                    Side.TOP -> Insets(top = decoration.preferredSize)
                    Side.END -> Insets(end = decoration.preferredSize)
                    Side.BOTTOM -> Insets(bottom = decoration.preferredSize)
                }
                max(insets, decorationInsets)
            }

    private val contentContainer = FrameLayout(context)

    var content: View? = null
        set(value) {
            if (field != value) {
                field = value
                contentContainer.removeAllViews()
                contentContainer.addChild(value)
            }
        }

    init {
        addChild(contentContainer)
        decorations.forEach { decoration ->
            addChild(
                    decoration.view
                            .applyFrameParams {
                                if (decoration.side.horizontal) {
                                    setMatchParentHeight()
                                } else {
                                    setMatchParentWidth()
                                }
                                setGravity(decoration.side.toHGravity())
                            }
            )
        }
    }

    override fun handleGoBack(): Boolean {

        val content = this.content
        if (content is GoBackHandler && content.handleGoBack()) {
            return true
        }

        decorations.forEach {
            if (it.handleGoBack()) {
                return true
            }
        }

        return false
    }


}

fun <G : ViewGroup> G.addDecoratedContainer(
        decorations: Iterable<DecoratedContainerDecoration>,
        contentBuilder: ((Insets) -> View)? = null,
        viewConfigurator: (DecoratedContainer.() -> Unit)? = null
) =
        addChild(DecoratedContainer(context, decorations)) {
            contentBuilder?.let {
                content = contentBuilder(insets)
            }
            viewConfigurator?.invoke(this)
        }