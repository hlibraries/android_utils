package ru.hnau.androidutils.ui.view.utils

import android.graphics.Rect
import android.view.View


fun View.layout(bounds: Rect) = layout(bounds.left, bounds.top, bounds.right, bounds.bottom)

fun View.getBounds(result: Rect) = result.set(left, top, right, bottom)

fun View.getBounds() = Rect().apply { getBounds(this) }