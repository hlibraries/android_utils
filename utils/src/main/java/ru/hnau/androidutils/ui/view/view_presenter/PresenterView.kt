package ru.hnau.androidutils.ui.view.view_presenter

import android.content.Context
import android.graphics.Rect
import android.view.ViewGroup
import ru.hnau.androidutils.animations.AnimationHeaviness
import ru.hnau.androidutils.animations.Animator
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.extensions.observeWhen


open class PresenterView(
        context: Context,
        presentingViewProducer: Producer<PresentingViewInfo>,
        private val presenterViewInfo: PresenterViewInfo = PresenterViewInfo()
) : ViewGroup(
        context
) {

    private val isVisibleToUserProducer =
            createIsVisibleToUserProducer()

    private var isVisibleToUser = false

    private var lastView: PresentingViewWrapper? = null
    private var currentView: PresentingViewWrapper? = null
    private var nextView: PresentingViewInfo? = null

    private var isAnimating = false

    private var animatingPercentage = 0f
        set(value) {
            field = value
            requestLayout()
        }


    private val layoutBounds = Rect()

    init {
        clipToPadding = false
        presentingViewProducer
                .observeWhen(isVisibleToUserProducer, this::onNewViewToPresent)

        isVisibleToUserProducer.attach { isVisibleToUser = it }
    }

    private fun onNewViewToPresent(presentingViewInfo: PresentingViewInfo) {

        if (presentingViewInfo.view == currentView?.view) {
            nextView = null
            return
        }

        nextView = presentingViewInfo
        startAnimationIfNeed()
    }

    private fun startAnimationIfNeed() {
        if (isAnimating) {
            return
        }

        val nextView = nextView ?: return
        val animatingTime = if (isVisibleToUser) nextView.properties.animatingTime else TimeValue.ZERO

        Animator.doAnimation(
                duration = animatingTime,
                heaviness = AnimationHeaviness.HARD,
                onBegin = {
                    isAnimating = true
                    lastView = currentView
                    currentView = PresentingViewWrapper(this, nextView)
                    this.nextView = null
                },
                onProgress = { animatingPercentage ->
                    this.animatingPercentage = animatingPercentage
                },
                onEnd = {
                    lastView?.removeFromParent(this)
                    lastView = null
                    isAnimating = false
                    startAnimationIfNeed()
                }
        )


    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        layoutBounds.set(
                paddingLeft,
                paddingTop,
                width - paddingRight,
                height - paddingBottom
        )
        lastView?.layout(layoutBounds, false, animatingPercentage)
        currentView?.layout(layoutBounds, true, animatingPercentage)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val childrenWidthMeasureSpec = resizeMeasureSpec(widthMeasureSpec, -horizontalPaddingSum)
        val childrenHeightMeasureSpec = resizeMeasureSpec(heightMeasureSpec, -verticalPaddingSum)

        lastView?.measure(childrenWidthMeasureSpec, childrenHeightMeasureSpec)
        currentView?.measure(childrenWidthMeasureSpec, childrenHeightMeasureSpec)

        val width = presenterViewInfo.horizontalSizeInterpolator.interpolate(
                from = lastView.measuredWidth,
                to = currentView.measuredWidth,
                percentage = animatingPercentage
        )

        val height = presenterViewInfo.verticalSizeInterpolator.interpolate(
                from = lastView.measuredHeight,
                to = currentView.measuredHeight,
                percentage = animatingPercentage
        )

        setMeasuredDimension(
                width + horizontalPaddingSum,
                height + verticalPaddingSum
        )
    }

}


fun <G: ViewGroup> G.addPresenter(
        presentingViewProducer: Producer<PresentingViewInfo>,
        presenterViewInfo: PresenterViewInfo = PresenterViewInfo(),
        viewConfigurator: (PresenterView.() -> Unit)? = null
) = addChild(
        PresenterView(context, presentingViewProducer, presenterViewInfo),
        viewConfigurator
)