package ru.hnau.androidutils.ui.view.utils.scroll.recycle_view

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import ru.hnau.androidutils.ui.view.utils.horizontalLinearLayoutManager
import ru.hnau.androidutils.ui.view.utils.isLTR
import ru.hnau.androidutils.ui.view.utils.verticalLinearLayoutManager
import ru.hnau.jutils.takeIfPositive
import java.lang.Exception

private val RecyclerView.isRecycleViewScrolledToLeft: Boolean
    get() {
        val itemsCount = itemsCount.takeIfPositive() ?: return false
        val layoutManager = horizontalLinearLayoutManager ?: return false
        val firstItemChild = layoutManager.getFirstChildIfItIsForFirstItem(itemsCount - 1)
                ?: return true
        return layoutManager.getDecoratedLeft(firstItemChild) < 0
    }

val RecyclerView.isRecycleViewScrolledToTop: Boolean
    get() {
        val itemsCount = itemsCount.takeIfPositive() ?: return false
        val layoutManager = verticalLinearLayoutManager ?: return false
        val firstItemChild = layoutManager.getFirstChildIfItIsForFirstItem(itemsCount - 1)
                ?: return true
        return layoutManager.getDecoratedTop(firstItemChild) < 0
    }

private val RecyclerView.isRecycleViewScrolledToRight: Boolean
    get() {
        val itemsCount = itemsCount.takeIfPositive() ?: return false
        val layoutManager = horizontalLinearLayoutManager ?: return false
        val lastItemChild = layoutManager.getLastChildIfItIsForLastItem(itemsCount - 1)
                ?: return true
        return layoutManager.getDecoratedRight(lastItemChild) > width
    }

val RecyclerView.isRecycleViewScrolledToBottom: Boolean
    get() {
        val itemsCount = itemsCount.takeIfPositive() ?: return false
        val layoutManager = verticalLinearLayoutManager ?: return false
        val lastItemChild = layoutManager.getLastChildIfItIsForLastItem(itemsCount - 1)
                ?: return true
        return layoutManager.getDecoratedBottom(lastItemChild) > height
    }

val RecyclerView.isRecycleViewScrolledToStart: Boolean
    get() = if (!isLTR) isRecycleViewScrolledToRight else isRecycleViewScrolledToLeft

val RecyclerView.isRecycleViewScrolledToEnd: Boolean
    get() = if (!isLTR) isRecycleViewScrolledToLeft else isRecycleViewScrolledToRight

private fun LinearLayoutManager.getFirstChildIfItIsForFirstItem(lastItemPosition: Int) =
        if (reverseLayout) getRightOrBottomChildIfItIsForLastItem(lastItemPosition) else getLeftOtTopChildIfItIsForFirstItem()

private fun LinearLayoutManager.getLastChildIfItIsForLastItem(lastItemPosition: Int) =
        if (reverseLayout) getLeftOtTopChildIfItIsForFirstItem() else getRightOrBottomChildIfItIsForLastItem(lastItemPosition)

private fun LinearLayoutManager.getLeftOtTopChildIfItIsForFirstItem() =
        if (findFirstVisibleItemPosition() > 0) null else findViewByPosition(0)

private fun LinearLayoutManager.getRightOrBottomChildIfItIsForLastItem(lastItemPosition: Int) =
        if (findLastVisibleItemPosition() < lastItemPosition) null else findViewByPosition(lastItemPosition)

private val RecyclerView.itemsCount: Int
    get() = adapter?.itemCount ?: 0