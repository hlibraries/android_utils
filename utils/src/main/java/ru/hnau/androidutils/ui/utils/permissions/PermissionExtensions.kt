package ru.hnau.androidutils.ui.utils.permissions

import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import ru.hnau.androidutils.utils.ContextConnector


fun checkHasPermission(permissionName: String) =
        ContextCompat.checkSelfPermission(ContextConnector.context, permissionName) == PackageManager.PERMISSION_GRANTED