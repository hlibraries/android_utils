package ru.hnau.androidutils.ui.view.decorated_container.decorations.tabs

import ru.hnau.androidutils.animations.AnimationsUtils
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp12
import ru.hnau.androidutils.context_getters.dp_px.dp24
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.jutils.TimeValue


data class TabsViewInfo(
        val rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        val inactiveInfo: TabsViewStateInfo = TabsViewStateInfo(
                color = ColorGetter.DKGRAY,
                titleTextSize = dp8
        ),
        val activeInfo: TabsViewStateInfo = TabsViewStateInfo(
                color = ColorGetter.BLACK,
                titleTextSize = dp12
        ),
        val animatingTime: TimeValue = AnimationsUtils.DEFAULT_ANIMATION_TIME,
        val preferredHeight: DpPxGetter = dp(44),
        val iconSize: DpPxGetter = dp24
)