package ru.hnau.androidutils.ui.utils

import android.graphics.Point


enum class Side(
        private val direction: Point,
        val horizontal: Boolean
) {

    START(
            direction = Point(-1, 0),
            horizontal = true
    ),

    TOP(
            direction = Point(0, -1),
            horizontal = false
    ),

    END(
            direction = Point(1, 0),
            horizontal = true
    ),

    BOTTOM(
            direction = Point(0, 1),
            horizontal = false
    );

    private val rtlDirection = Point(-direction.x, direction.y)

    fun getDirection(rtl: Boolean) = if (rtl) rtlDirection else direction

    fun getOpposite() = when (this) {
        START -> END
        TOP -> BOTTOM
        END -> START
        BOTTOM -> TOP
    }

}