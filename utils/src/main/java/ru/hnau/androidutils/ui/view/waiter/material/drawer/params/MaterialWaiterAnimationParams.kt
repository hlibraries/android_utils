package ru.hnau.androidutils.ui.view.waiter.material.drawer.params

import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.view.animation.Interpolator
import ru.hnau.jutils.TimeValue


data class MaterialWaiterAnimationParams(
        val period: TimeValue = TimeValue.SECOND * 1.7,
        val degreesOffsetFactor: Double = 6.0,
        val fazesDistancePercentage: Double = 0.4,
        val lineSidesInterpolator: Interpolator = FastOutSlowInInterpolator()
) {

    companion object {

        val DEFAULT = MaterialWaiterAnimationParams()

    }

}