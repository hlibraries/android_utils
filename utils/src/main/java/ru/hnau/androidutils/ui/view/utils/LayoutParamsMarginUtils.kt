package ru.hnau.androidutils.ui.view.utils

import android.content.Context
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setMargins(margins: Int) =
        setMargins(margins, margins, margins, margins)

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setMargins(margins: Float) =
        setMargins(margins.toInt())

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setMargins(context: Context, margins: DpPxGetter) =
        setMargins(margins.getPx(context))

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setMargins(left: Float, top: Float, right: Float, bottom: Float) =
        setMargins(left.toInt(), top.toInt(), right.toInt(), bottom.toInt())

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setMargins(context: Context, left: DpPxGetter, top: DpPxGetter, right: DpPxGetter, bottom: DpPxGetter) {
    setMargins(left.getPx(context), top.getPx(context), right.getPx(context), bottom.getPx(context))
}

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setMargins(horizontal: Int, vertical: Int) =
        setMargins(horizontal, vertical, horizontal, vertical)

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setMargins(horizontal: Float, vertical: Float) =
        setMargins(horizontal.toInt(), vertical.toInt())

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setMargins(context: Context, horizontal: DpPxGetter, vertical: DpPxGetter) {
    setMargins(horizontal.getPx(context), vertical.getPx(context))
}

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setHorizontalMargins(horizontalMargins: Int) =
        setMargins(horizontalMargins, topMargin, horizontalMargins, bottomMargin)

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setHorizontalMargins(horizontalMargins: Float) =
        setHorizontalMargins(horizontalMargins.toInt())

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setHorizontalMargins(context: Context, horizontalMargins: DpPxGetter) =
        setHorizontalMargins(horizontalMargins.getPx(context))

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setVerticalMargins(verticalMargins: Int) =
        setMargins(leftMargin, verticalMargins, rightMargin, verticalMargins)

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setVerticalMargins(verticalMargins: Float) =
        setVerticalMargins(verticalMargins.toInt())

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setVerticalMargins(context: Context, verticalMargins: DpPxGetter) =
        setVerticalMargins(verticalMargins.getPx(context))

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setLeftMargin(leftMargin: Int) {
    this.leftMargin = leftMargin
}

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setLeftMargin(leftMargin: Float) =
        setLeftMargin(leftMargin.toInt())

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setLeftMargin(context: Context, leftMargin: DpPxGetter) =
        setLeftMargin(leftMargin.getPx(context))

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setTopMargin(topMargin: Int) {
    this.topMargin = topMargin
}

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setTopMargin(topMargin: Float) =
        setTopMargin(topMargin.toInt())

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setTopMargin(context: Context, topMargin: DpPxGetter) =
        setTopMargin(topMargin.getPx(context))

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setRightMargin(rightMargin: Int) {
    this.rightMargin = rightMargin
}

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setRightMargin(rightMargin: Float) =
        setRightMargin(rightMargin.toInt())

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setRightMargin(context: Context, rightMargin: DpPxGetter) =
        setRightMargin(rightMargin.getPx(context))

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setBottomMargin(bottomMargin: Int) {
    this.bottomMargin = bottomMargin
}

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setBottomMargin(bottomMargin: Float) =
        setBottomMargin(bottomMargin.toInt())

@Deprecated("Use ViewGroup.MarginLayoutParams.setGravity")
fun ViewGroup.MarginLayoutParams.setBottomMargin(context: Context, bottomMargin: DpPxGetter) =
        setBottomMargin(bottomMargin.getPx(context))