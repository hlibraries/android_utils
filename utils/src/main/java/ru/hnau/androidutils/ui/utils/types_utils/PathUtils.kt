package ru.hnau.androidutils.ui.utils.types_utils

import android.graphics.Path
import android.graphics.RectF
import kotlin.math.min


fun Path.initAsRoundSideRect(
        rect: RectF,
        rectFMemSaveValue: RectF = RectF()
) =
        initAsRoundSideRect(
                rect.left, rect.top, rect.right, rect.bottom,
                rectFMemSaveValue
        )

fun Path.initAsRoundSideRect(
        rectLeft: Float,
        rectTop: Float,
        rectRight: Float,
        rectBottom: Float,
        rectFMemSaveValue: RectF = RectF()
) =
        initAsRoundCornerRect(
                rectLeft, rectTop, rectRight, rectBottom,
                min(rectRight - rectLeft, rectBottom - rectTop) / 2,
                rectFMemSaveValue
        )

fun Path.initAsRoundCornerRect(
        rect: RectF,
        cornerRadius: Float,
        rectFMemSaveValue: RectF = RectF()
) =
        initAsRoundCornerRect(
                rect.left, rect.top, rect.right, rect.bottom,
                cornerRadius, rectFMemSaveValue
        )

fun Path.initAsRoundCornerRect(
        rectLeft: Float,
        rectTop: Float,
        rectRight: Float,
        rectBottom: Float,
        cornerRadius: Float,
        rectFMemSaveValue: RectF = RectF()
) {

    val circleLeft = rectLeft + cornerRadius * 2
    val circleTop = rectTop + cornerRadius * 2
    val circleBottom = rectBottom - cornerRadius * 2
    val circleRight = rectRight - cornerRadius * 2

    reset()

    rectFMemSaveValue.set(rectLeft, rectTop, circleLeft, circleTop)
    arcTo(rectFMemSaveValue, 180f, 90f)

    rectFMemSaveValue.set(circleRight, rectTop, rectRight, circleTop)
    arcTo(rectFMemSaveValue, 270f, 90f)

    rectFMemSaveValue.set(circleRight, circleBottom, rectRight, rectBottom)
    arcTo(rectFMemSaveValue, 0f, 90f)

    rectFMemSaveValue.set(rectLeft, circleBottom, circleLeft, rectBottom)
    arcTo(rectFMemSaveValue, 90f, 90f)

    lineTo(rectLeft, rectTop + cornerRadius)

}

fun Path.initAsCircle(
        cx: Float,
        cy: Float,
        radius: Float,
        rectFMemSaveValue: RectF = RectF()
) =
        initAsOval(
                cx - radius,
                cy - radius,
                cx + radius,
                cy + radius,
                rectFMemSaveValue
        )

fun Path.initAsOval(
        left: Float,
        top: Float,
        right: Float,
        bottom: Float,
        rectFMemSaveValue: RectF = RectF()
) {
    rectFMemSaveValue.set(left, top, right, bottom)
    initAsOval(rectFMemSaveValue)
}

fun Path.initAsOval(
        rect: RectF
) {
    reset()
    addArc(rect, 0f, 360f)
}

fun Path.initAsCircle(
        rect: RectF,
        rectFMemSaveValue: RectF = RectF()
) {
    val cx = rect.centerX()
    val cy = rect.centerY()
    val radius = min(rect.width(), rect.height()) / 2
    initAsCircle(cx, cy, radius, rectFMemSaveValue)
}