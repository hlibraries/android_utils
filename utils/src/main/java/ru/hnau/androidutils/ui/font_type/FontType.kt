package ru.hnau.androidutils.ui.font_type

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface


data class FontType(
        val typeface: Typeface
) {

    companion object {

        val DEFAULT = FontType(Typeface.DEFAULT)
        val DEFAULT_BOLD = FontType(Typeface.DEFAULT_BOLD)
        val MONOSPACE = FontType(Typeface.MONOSPACE)
        val SANS_SERIF = FontType(Typeface.SANS_SERIF)
        val SERIF = FontType(Typeface.SERIF)

    }

    constructor(
            context: Context,
            assertFontName: String
    ) : this(
            Typeface.createFromAsset(context.assets, assertFontName)!!
    )

    fun applyToPaint(paint: Paint) {
        paint.typeface = typeface
    }

}

fun FontType?.createPaint() = Paint(Paint.ANTI_ALIAS_FLAG).apply {
    setFontType(this@createPaint)
}

fun Paint.setFontType(fontType: FontType?) {
    typeface = fontType?.typeface
}