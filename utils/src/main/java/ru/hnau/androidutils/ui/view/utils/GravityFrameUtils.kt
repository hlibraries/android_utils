package ru.hnau.androidutils.ui.view.utils

import android.widget.FrameLayout
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


@Deprecated("")
fun FrameLayout.setCenterForegroundGravity() {
    foregroundGravity = HGravity.CENTER.resolveAndroidGravity()
}

@Deprecated("")
fun FrameLayout.setStartTopForegroundGravity() {
    foregroundGravity = HGravity.START_TOP.resolveAndroidGravity()
}

@Deprecated("")
fun FrameLayout.setEndTopForegroundGravity() {
    foregroundGravity = HGravity.END_TOP.resolveAndroidGravity()
}

@Deprecated("")
fun FrameLayout.setEndBottomForegroundGravity() {
    foregroundGravity = HGravity.END_BOTTOM.resolveAndroidGravity()
}

@Deprecated("")
fun FrameLayout.setStartBottomForegroundGravity() {
    foregroundGravity = HGravity.START_BOTTOM.resolveAndroidGravity()
}

@Deprecated("")
fun FrameLayout.setTopCenterHorizontalForegroundGravity() {
    foregroundGravity = HGravity.TOP_CENTER_HORIZONTAL.resolveAndroidGravity()
}

@Deprecated("")
fun FrameLayout.setBottomCenterHorizontalForegroundGravity() {
    foregroundGravity = HGravity.BOTTOM_CENTER_HORIZONTAL.resolveAndroidGravity()
}

@Deprecated("")
fun FrameLayout.setStartCenterVerticalForegroundGravity() {
    foregroundGravity = HGravity.START_CENTER_VERTICAL.resolveAndroidGravity()
}

@Deprecated("")
fun FrameLayout.setEndCenterVerticalForegroundGravity() {
    foregroundGravity = HGravity.END_CENTER_VERTICAL.resolveAndroidGravity()
}