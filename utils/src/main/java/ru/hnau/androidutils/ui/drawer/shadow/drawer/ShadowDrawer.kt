package ru.hnau.androidutils.ui.drawer.shadow.drawer

import android.content.Context
import android.graphics.BlurMaskFilter
import android.graphics.Canvas
import android.graphics.Paint
import ru.hnau.androidutils.ui.canvas_shape.CanvasShape
import ru.hnau.androidutils.ui.drawer.CanvasShapeDrawer
import ru.hnau.androidutils.ui.utils.types_utils.doInState
import ru.hnau.androidutils.ui.drawer.shadow.info.ShadowInfo
import ru.hnau.jutils.takeIfPositive


class ShadowDrawer(
        private val context: Context,
        private val shadowInfo: ShadowInfo,
        canvasShape: CanvasShape
): CanvasShapeDrawer(
        canvasShape
) {

    val insets = shadowInfo.insets

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = shadowInfo.color.get(context)
        alpha = (shadowInfo.alpha * 255).toInt()
        maskFilter = shadowInfo.blur.getPx(context).takeIfPositive()?.let { BlurMaskFilter(it, BlurMaskFilter.Blur.NORMAL) }
    }

    fun draw(canvas: Canvas) = canvas.doInState {
        translate(0f, shadowInfo.offset.getPx(context))
        canvasShape.draw(canvas, paint)
    }

}