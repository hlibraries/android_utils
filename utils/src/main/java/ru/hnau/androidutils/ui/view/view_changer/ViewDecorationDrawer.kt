package ru.hnau.androidutils.ui.view.view_changer

import android.graphics.Canvas
import android.view.View


interface ViewDecorationDrawer {

    fun draw(
            canvas: Canvas,
            view: View,
            showing: Boolean,
            showingHidingPercentage: Float,
            scrollFactor: Float,
            onTop: Boolean
    )

}