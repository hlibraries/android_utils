package ru.hnau.androidutils.ui.view.list.base

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView


data class BaseListCalculateDiffInfo<T>(
        private val itemsComparator: (T, T) -> Boolean,
        private val itemsContentComparator: (T, T) -> Boolean,
        private val detectItemsMovesAfterUpdate: Boolean = true
) : DiffUtil.Callback() {

    companion object {

        fun <T, ID, CONTENT> create(
                itemIdExtractor: (T) -> ID,
                itemContentExtractor: (T) -> CONTENT,
                detectItemsMovesAfterUpdate: Boolean = true
        ) = BaseListCalculateDiffInfo<T>(
                itemsComparator = { item1, item2 -> itemIdExtractor.invoke(item1) == itemIdExtractor.invoke(item2) },
                itemsContentComparator = { item1, item2 -> itemContentExtractor.invoke(item1) == itemContentExtractor.invoke(item2) },
                detectItemsMovesAfterUpdate = detectItemsMovesAfterUpdate
        )

    }

    private var oldList = emptyList<T>()
    private var newList = emptyList<T>()

    fun onItemsChanged(
            oldList: List<T>,
            newList: List<T>,
            adapter: RecyclerView.Adapter<BaseListViewHolder<T>>
    ) {
        this.oldList = oldList
        this.newList = newList

        val diffUtilResult = DiffUtil.calculateDiff(this, detectItemsMovesAfterUpdate)
        diffUtilResult.dispatchUpdatesTo(adapter)

        this.oldList = emptyList()
        this.newList = emptyList()
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            itemsComparator(oldList[oldItemPosition], newList[newItemPosition])

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            itemsContentComparator(oldList[oldItemPosition], newList[newItemPosition])

}