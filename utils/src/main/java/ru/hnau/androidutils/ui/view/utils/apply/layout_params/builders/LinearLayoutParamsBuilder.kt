package ru.hnau.androidutils.ui.view.utils.apply.layout_params.builders

import android.view.View
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


class LinearLayoutParamsBuilder(
        view: View
) : MarginGravityLayoutParamsBuilder(
        view
) {

    var weight: Float = 0f
        private set

    fun setWeight(weight: Float) {
        this.weight = weight
    }

    fun setSingleWeight() =
            setWeight(1f)

    fun setZeroWidth() =
            setWidth(0)

    fun setZeroHeight() =
            setHeight(0)

    fun setStretchedWidth(weight: Float = 1f) {
        setZeroWidth()
        setWeight(weight)
    }

    fun setStretchedHeight(weight: Float = 1f) {
        setZeroHeight()
        setWeight(weight)
    }

    fun setSize(width: Int, height: Int, weight: Float) {
        setSize(width, height)
        setWeight(weight)
    }

    fun setSize(width: DpPxGetter, height: DpPxGetter, weight: Float) =
            setSize(width.getPxInt(context), height.getPxInt(context), weight)

    fun setSize(width: Float, height: Float, weight: Float) =
            setSize(width.toInt(), height.toInt(), weight)

    fun setSize(width: Double, height: Double, weight: Float) =
            setSize(width.toInt(), height.toInt(), weight)

    fun setSize(width: Short, height: Short, weight: Float) =
            setSize(width.toInt(), height.toInt(), weight)

    fun setSize(width: Long, height: Long, weight: Float) =
            setSize(width.toInt(), height.toInt(), weight)

    fun setSize(width: Byte, height: Byte, weight: Float) =
            setSize(width.toInt(), height.toInt(), weight)

    override fun build() =
            LinearLayout.LayoutParams(width, height, weight).apply {
                applyMargins()
                gravity = resolveGravity()
            }

}