package ru.hnau.androidutils.ui.view.buttons.circle

import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp56


class CircleButtonSize(
        val diameter: DpPxGetter
) {

    companion object {

        val DP_56 = CircleButtonSize(dp56)

        val DEFAULT = DP_56

    }

}