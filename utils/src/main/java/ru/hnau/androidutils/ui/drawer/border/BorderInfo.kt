package ru.hnau.androidutils.ui.drawer.border

import android.graphics.Paint
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.drawer.Insets


data class BorderInfo(
        val width: DpPxGetter,
        val color: ColorGetter,
        val alpha: Float = 1f,
        val lineJoin: Paint.Join = Paint.Join.MITER,
        val lineCap: Paint.Cap = Paint.Cap.SQUARE
) {

    val insets = Insets(width / 2)

}