package ru.hnau.androidutils.ui.view.utils

import android.graphics.drawable.Drawable
import android.os.Build
import android.support.v4.text.TextUtilsCompat
import android.support.v4.view.ViewCompat
import android.view.View
import android.view.View.LAYOUT_DIRECTION_RTL
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.dpToPx
import ru.hnau.androidutils.context_getters.dp_px.dpToPxInt
import ru.hnau.androidutils.context_getters.dp_px.pxToDp
import ru.hnau.androidutils.context_getters.dp_px.pxToDpInt
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.producer.Producer
import java.util.*

fun <V : View> V.setSoftwareRendering() =
        apply { setLayerType(View.LAYER_TYPE_SOFTWARE, null) }

fun <V : View> V.postDelayed(pause: TimeValue, action: () -> Unit) =
        apply { postDelayed(action, pause.milliseconds) }

fun <V : View> V.applyPost(action: () -> Unit) =
        apply { post(action) }

@Deprecated("Use View.applyBackground")
fun View.setBackgroundColor(color: ColorGetter) =
        setBackgroundColor(color.get(context))

@Deprecated("Use Producer.observeWhen(whenSign: Producer<Boolean>, listener: (T) -> Unit)")
fun <T> View.observeWhenVisibleToUser(producer: Producer<T>, handler: (T) -> Unit) =
        addIsVisibleToUserHandler { isVisibleToUser ->
            if (isVisibleToUser) {
                producer.attach(handler)
            } else {
                producer.detach(handler)
            }
        }

@Deprecated("Use View.createIsVisibleToUserProducer(): Producer<Boolean>")
fun View.addIsVisibleToUserHandler(handler: (isVisibleToUser: Boolean) -> Unit) {
    ViewIsVisibleToUserProducer(this).attach(handler)
}

@Deprecated("Use View.createIsVisibleToUserProducer(): Producer<Boolean>")
fun View.addShownToUserHandler(handler: () -> Unit) =
        addIsVisibleToUserHandler { it.ifTrue(handler) }

@Deprecated("Use View.createIsVisibleToUserProducer(): Producer<Boolean>")
fun View.addHiddenFromUserHandler(handler: () -> Unit) =
        addIsVisibleToUserHandler { it.ifFalse(handler) }

fun <V : View> V.setFitKeyboard() = apply {
    fitsSystemWindows = true
    ViewCompat.setOnApplyWindowInsetsListener(this) { view, insets ->
        insets.replaceSystemWindowInsets(0, 0, 0, insets.systemWindowInsetBottom).apply {
            ViewCompat.onApplyWindowInsets(view, this)
        }
    }
}

fun <T> View.invalidate(param: T) = invalidate()

fun <T> View.requestLayout(param: T) = requestLayout()

fun View.dpToPx(dp: Int) = dpToPx(context, dp)
fun View.dpToPx(dp: Float) = dpToPx(context, dp)
fun View.dpToPx(dp: Double) = dpToPx(context, dp)
fun View.dpToPxInt(dp: Int) = dpToPxInt(context, dp)
fun View.dpToPxInt(dp: Float) = dpToPxInt(context, dp)
fun View.dpToPxInt(dp: Double) = dpToPxInt(context, dp)

fun View.pxToDp(px: Int) = pxToDp(context, px)
fun View.pxToDp(px: Float) = pxToDp(context, px)
fun View.pxToDp(px: Double) = pxToDp(context, px)
fun View.pxToDpInt(px: Int) = pxToDpInt(context, px)
fun View.pxToDpInt(px: Float) = pxToDpInt(context, px)
fun View.pxToDpInt(px: Double) = pxToDpInt(context, px)

fun <V : View> V.applyVisibility(visibility: Int) = apply { setVisibility(visibility) }
fun <V : View> V.setVisible() = applyVisibility(View.VISIBLE)
fun <V : View> V.setInvisible() = applyVisibility(View.INVISIBLE)
fun <V : View> V.setGone() = applyVisibility(View.GONE)

fun <V : View> V.setVisibleOrInvisible(visible: Boolean) =
        apply { if (visible) setVisible() else setInvisible() }

fun <V : View> V.setVisibleOrGone(visible: Boolean) =
        apply { if (visible) setVisible() else setGone() }

val View.paddingStart: Int
    get() = if (!isLTR) paddingRight else paddingLeft

val View.paddingEnd: Int
    get() = if (!isLTR) paddingLeft else paddingRight
