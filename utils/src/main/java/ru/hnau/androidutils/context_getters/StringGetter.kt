package ru.hnau.androidutils.context_getters

import android.content.Context


class StringGetter(
        getter: (context: Context) -> String
) : ContextGetter<String>(
        getter = getter,
        dependencies = listOf(ContextGetterDependency.LANGUAGE)
) {

    constructor(existenceString: String) : this({ existenceString })

    constructor(resId: Int, vararg params: Any) : this({ it.getString(resId, *params) })

    constructor(vararg stringGetters: StringGetter) : this({ context ->
        stringGetters.joinToString("") { it.get(context) }
    })

    companion object {

        val EMPTY = StringGetter("")

    }

    operator fun plus(other: StringGetter) = StringGetter(this, other)

    operator fun plus(string: String) = plus(string.toGetter())

    operator fun plus(int: Int) = plus(int.toString())

    operator fun plus(float: Float) = plus(float.toString())

    operator fun plus(long: Long) = plus(long.toString())

    operator fun plus(double: Double) = plus(double.toString())

    operator fun plus(char: Char) = plus(char.toString())

    fun map(converter: (context: Context, string: String) -> String) =
            StringGetter { context ->
                val string = this.get(context)
                converter.invoke(context, string)
            }

    fun toUpperCase() = map { _, string -> string.toUpperCase() }

    fun toLowerCase() = map { _, string -> string.toLowerCase() }

}

fun String.toGetter() = StringGetter(this)