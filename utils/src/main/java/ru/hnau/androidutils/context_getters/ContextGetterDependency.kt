package ru.hnau.androidutils.context_getters


enum class ContextGetterDependency {

    SCALE,
    THEME,
    LANGUAGE

}