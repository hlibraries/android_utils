package ru.hnau.androidutils.context_getters.dp_px

import android.content.Context
import ru.hnau.androidutils.context_getters.ContextGetter
import ru.hnau.androidutils.context_getters.ContextGetterDependency
import kotlin.math.max
import kotlin.math.min


class DpPxGetter internal constructor(
        getter: (context: Context) -> DpPx
) : ContextGetter<DpPxGetter.DpPx>(
        getter = getter,
        dependencies = listOf(ContextGetterDependency.SCALE)
) {

    data class DpPx(
            val dp: Float,
            val px: Float
    ) {

        companion object {
            val ZERO = DpPx(0f, 0f)
        }

    }

    companion object {

        val ZERO = DpPxGetter { DpPx.ZERO }

        fun dp(getter: (Context) -> Float) = DpPxGetter { context ->
            val dp = getter.invoke(context)
            DpPx(dp, dpToPx(context, dp))
        }

        fun dpInt(getter: (Context) -> Int) = DpPxGetter { context ->
            val dp = getter.invoke(context).toFloat()
            DpPx(dp, dpToPx(context, dp))
        }

        fun px(getter: (Context) -> Float) = DpPxGetter { context ->
            val px = getter.invoke(context)
            DpPx(pxToDp(context, px), px)
        }

        fun pxInt(getter: (Context) -> Int) = DpPxGetter { context ->
            val px = getter.invoke(context).toFloat()
            DpPx(pxToDp(context, px), px)
        }

        fun dp(dp: Float) = dp { dp }
        fun dp(dp: Int) = dpInt { dp }
        fun px(px: Float) = px { px }
        fun px(px: Int) = pxInt { px }

    }

    fun getDp(context: Context) = get(context).dp
    fun getDpInt(context: Context) = get(context).dp.toInt()

    fun getPx(context: Context) = get(context).px
    fun getPxInt(context: Context) = get(context).px.toInt()

    private fun map(converter: (context: Context, dp: Float, px: Float) -> DpPx) =
            DpPxGetter { context ->
                converter.invoke(context, getDp(context), getPx(context))
            }

    operator fun plus(other: DpPxGetter) = map { context, dp, px ->
        DpPx(dp + other.get(context).dp, px + other.get(context).px)
    }

    operator fun minus(other: DpPxGetter) = map { context, dp, px ->
        DpPx(dp - other.get(context).dp, px - other.get(context).px)
    }

    operator fun times(value: Float) = map { _, dp, px ->
        DpPx(dp * value, px * value)
    }

    operator fun div(value: Float) = map { _, dp, px ->
        DpPx(dp / value, px / value)
    }

    operator fun times(value: Int) = times(value.toFloat())
    operator fun times(value: Double) = times(value.toFloat())
    operator fun times(value: Long) = times(value.toFloat())
    operator fun div(value: Int) = div(value.toFloat())
    operator fun div(value: Double) = div(value.toFloat())
    operator fun div(value: Long) = div(value.toFloat())

    fun coerceAtLeast(minimumValue: DpPxGetter) = map { context, dp, px ->
        DpPx(
                dp = dp.coerceAtLeast(minimumValue.getDp(context)),
                px = px.coerceAtLeast(minimumValue.getPx(context))
        )
    }

    fun coerceAtMost(maximumValue: DpPxGetter) = map { context, dp, px ->
        DpPx(
                dp = dp.coerceAtMost(maximumValue.getDp(context)),
                px = px.coerceAtMost(maximumValue.getPx(context))
        )
    }

    fun coerceIn(minimumValue: DpPxGetter, maximumValue: DpPxGetter) = map { context, dp, px ->
        DpPx(
                dp = dp.coerceIn(minimumValue.getDp(context), maximumValue.getDp(context)),
                px = px.coerceIn(minimumValue.getPx(context), maximumValue.getPx(context))
        )
    }

}

fun max(first: DpPxGetter, second: DpPxGetter) = DpPxGetter { context ->
    DpPxGetter.DpPx(
            dp = max(first.getDp(context), second.getDp(context)),
            px = max(first.getPx(context), second.getPx(context))
    )
}

fun min(first: DpPxGetter, second: DpPxGetter) = DpPxGetter { context ->
    DpPxGetter.DpPx(
            dp = min(first.getDp(context), second.getDp(context)),
            px = min(first.getPx(context), second.getPx(context))
    )
}