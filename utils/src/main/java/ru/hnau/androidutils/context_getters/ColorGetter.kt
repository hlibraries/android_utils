package ru.hnau.androidutils.context_getters

import android.content.Context
import android.graphics.Color
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.FloatRange
import android.support.annotation.IntRange
import android.support.v4.content.ContextCompat
import ru.hnau.androidutils.ui.utils.ThemeManager
import ru.hnau.androidutils.ui.utils.types_utils.ColorUtils


open class ColorGetter(
        getter: (context: Context) -> Int
) : ContextGetter<Int>(
        getter = getter,
        dependencies = listOf(ContextGetterDependency.THEME)
) {

    companion object {

        private const val MAX_COLOR_CHANNEL_INT_VALUE = 255

        val TRANSPARENT = byColor(Color.TRANSPARENT)
        val BLACK = byColor(Color.BLACK)
        val WHITE = byColor(Color.WHITE)
        val GRAY = byColor(Color.GRAY)
        val LTGRAY = byColor(Color.LTGRAY)
        val DKGRAY = byColor(Color.DKGRAY)
        val RED = byColor(Color.RED)
        val GREEN = byColor(Color.GREEN)
        val BLUE = byColor(Color.BLUE)
        val YELLOW = byColor(Color.YELLOW)
        val CYAN = byColor(Color.CYAN)
        val MAGENTA = byColor(Color.MAGENTA)

        fun choose(getter: (context: Context) -> ColorGetter) =
                ColorGetter { getter.invoke(it).get(it) }

        fun byResId(@ColorRes resId: Int) =
                ColorGetter { ContextCompat.getColor(it, resId) }

        fun byColor(@ColorInt color: Int) =
                ColorGetter { color }

        fun hex(hexString: String) = byColor(Color.parseColor(hexString))

        fun argb(
                @IntRange(from = 0, to = 255) a: Int,
                @IntRange(from = 0, to = 255) r: Int,
                @IntRange(from = 0, to = 255) g: Int,
                @IntRange(from = 0, to = 255) b: Int
        ) = byColor(Color.argb(a, r, g, b))

        fun argb(
                @FloatRange(from = 0.0, to = 1.0) a: Float,
                @FloatRange(from = 0.0, to = 1.0) r: Float,
                @FloatRange(from = 0.0, to = 1.0) g: Float,
                @FloatRange(from = 0.0, to = 1.0) b: Float
        ) = argb(
                (a * MAX_COLOR_CHANNEL_INT_VALUE).toInt(),
                (r * MAX_COLOR_CHANNEL_INT_VALUE).toInt(),
                (g * MAX_COLOR_CHANNEL_INT_VALUE).toInt(),
                (b * MAX_COLOR_CHANNEL_INT_VALUE).toInt()
        )

        fun rgb(
                @IntRange(from = 0, to = 255) r: Int,
                @IntRange(from = 0, to = 255) g: Int,
                @IntRange(from = 0, to = 255) b: Int
        ) = byColor(Color.rgb(r, g, b))

        fun rgb(
                @FloatRange(from = 0.0, to = 1.0) r: Float,
                @FloatRange(from = 0.0, to = 1.0) g: Float,
                @FloatRange(from = 0.0, to = 1.0) b: Float
        ) = rgb(
                (r * MAX_COLOR_CHANNEL_INT_VALUE).toInt(),
                (g * MAX_COLOR_CHANNEL_INT_VALUE).toInt(),
                (b * MAX_COLOR_CHANNEL_INT_VALUE).toInt()
        )

        fun grey(@FloatRange(from = 0.0, to = 1.0) lightness: Float) =
                rgb(lightness, lightness, lightness)

        fun grey(@IntRange(from = 0, to = 255) lightness: Int) =
                rgb(lightness, lightness, lightness)

        fun hsl(
                @FloatRange(from = 0.0, to = 1.0) hue: Float,
                @FloatRange(from = 0.0, to = 1.0) saturation: Float,
                @FloatRange(from = 0.0, to = 1.0) light: Float
        ) = ColorGetter { ColorUtils.hsl(hue, saturation, light) }

        fun hsl(
                @IntRange(from = 0, to = 255) hue: Int,
                @IntRange(from = 0, to = 255) saturation: Int,
                @IntRange(from = 0, to = 255) light: Int
        ) = ColorGetter { ColorUtils.hsl(hue, saturation, light) }

    }

    constructor(colorGetter: ColorGetter) : this({ colorGetter.get(it) })

    fun map(converter: (context: Context, color: Int) -> Int) =
            ColorGetter { context ->
                val color = this.get(context)
                converter.invoke(context, color)
            }

    fun mapWithAlpha(alpha: Float) =
            map { _, color -> ColorUtils.colorWithAlpha(color, alpha) }

    fun mapInterThisAndOther(other: ColorGetter, pos: Float) =
            map { context, color -> ColorUtils.colorInterColors(color, other.get(context), pos) }

}

fun Int.toColorGetter() = ColorGetter.byColor(this)